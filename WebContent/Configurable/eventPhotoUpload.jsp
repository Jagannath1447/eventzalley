<!DOCTYPE html>

<%@page import="com.uthkrushta.mybos.configuration.bean.GlobalUrlSettingBean"%>
<%@page import="java.util.Date"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeMasterBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventMasterBean"%>
<%@page import="java.awt.image.DataBuffer"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.uthkrushta.basis.bean.MessageBean"%>
<%@page import="com.uthkrushta.basis.controller.GenericController"%>

<%@page import="org.ajaxanywhere.AAUtils"%>
<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.mybos.code.bean.PaymentModeCodeBean"%>

<%@page import="com.uthkrushta.mybos.master.bean.AreaMasterBean"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<html>
<%
	String strContextPath = "../";
	strContextPath = WebUtil.parseString(request, IUMCConstants.CONTEXT_PATH);
%>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>Eventzalley</title>
<!-- Favicon-->
<!-- <link rel="icon" href="favicon.ico" type="image/x-icon"> -->

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" type="text/css">
<!-- Bootstrap Select Css -->
<link href="../plugins/bootstrap-select/css/bootstrap-select.css"
	rel="stylesheet" />

<link href="../Style/default.css"
	rel="stylesheet" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<link href="../css/themes/all-themes.css" rel="stylesheet" /> 
<script src="js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="js/jquery.fileupload.js"></script>
<script type="text/javascript">


function changeResult(){
	
	$("#result").hide();
}

$(function() {

   

    $('#fileupload').fileupload({
    	dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('<p/>').text(file.name).appendTo(document.body);
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css(
                'width',
                progress + '%'
            );
        }
    });
    
  });

function scrollWin() {
    window.scrollBy(0, 100); // Scroll down by 100px
}

    </script>

</head>

<%

Long memberID = (Long) session.getAttribute("LOGIN_ID");

long userID=0;


if(null != memberID ){
	userID = memberID.longValue();
}

GlobalUrlSettingBean objGISB = (GlobalUrlSettingBean) DBUtil.getRecord(IUMCConstants.BN_GLOBAL_IMAGE, IUMCConstants.GET_ACTIVE_ROWS, "");
if(objGISB == null){
objGISB = new GlobalUrlSettingBean();
}

List lstEventNames =  DBUtil.getRecords(ILabelConstants.BN_EVENT_MASTER, IUMCConstants.GET_ACTIVE_ROWS+" AND dtStartDate <= '"+WebUtil.formatDate(new Date(), IUMCConstants.DB_DATE_FORMAT)+"' ", "dtStartDate DESC");
	
%>



<body class="theme-blue" style="background-color: #e9e9e9">

	<!-- Page Loader -->




	<jsp:include page="../MyIncludes/incPageHeader.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
		 
	</jsp:include>
	<!-- #Top Bar -->

	<style type="text/css">
		.bar {
		    height: 18px;
		    background: #1f91f3;
		}
	</style>

	<jsp:include page="../MyIncludes/incLeftPanel.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>

	




	<section class="content">
					
		<div class="container-fluid">
		
			<div class="row clearfix ">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="card" style="margin-bottom: 0px;">
						<div class="header"  >
							<h2 >Photo Upload</h2>
						</div>
						
						<div id="ActionPanel" class="row no-margin-bottom default-margin "
							style="background-color: #006699;border:none; height: 45px; margin-right: 0px; margin-left: 0px; width: 100%">
							
							
							<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2 floatRight"
								style="float: right;">
								<input type="button" value="Close"
									class="btn btn-danger rightDefaultMargin alignRight"
									style="float: right; background-color: blue;"
									onClick="onCancel('../myHome.jsp')" />
							</div>
						</div>
						 <!-- method="post" action="/EventzAlley/UploadAttachment" enctype='multipart/form-data'  -->
						<form method="post">
						<div class="body" style=" margin-bottom: 25px;">
						
							<div class="row clearfix">
							
								<div class="col-sm-6" style=" padding-top: 20px;margin-top: 25px;">
								 <input type="hidden" value="<%=userID%>" id="userID">
								<select class="form-control show-tick"
									id="<%=ILabelConstants.selEventName%>"
									name="<%=ILabelConstants.selEventName%>" onChange="changeResult()"> <!-- onChange="findEventType()" -->
									<option value="0">-- Select Event Name*--</option>
									<%
										if (null != lstEventNames) {
											for (int i = 0; i < lstEventNames.size(); i++) {
												EventMasterBean objEMB = (EventMasterBean) lstEventNames.get(i);
									%>

									<option value="<%=objEMB.getLngID()%>"><%=objEMB.getStrName()%></option>
								
									<%
										}

										}
									%>


								</select>
							</div>
							
							<div class="col-sm-6 col-lg-4" style=" padding-top: 20px; margin-top: 30px;">	
								<input id="hdnET" type="hidden" name="hdnET" value="0">
								<input id="fileupload" type="file" name="image" disabled multiple style="width: 99px;" >
								
								
								<div id="progress" style="background-color:lightgrey"> <div class="bar" style="text-align:center;color:white;width:0%"></div>
								<!-- <button type="submit">Upload</button> -->
		               
		               				<div id="progress"> <div class="bar" style="width: 0%;"></div>
									</div>
							
								</div>

							
							
						
							
							</div >
							<div class="col-sm-12 col-lg-12" style="text-align:center; font-size:16px; color:#00920c;" id="result">
							
							
							
							</div>

							
				
						
						
						
						
						
						
						</div>
						</form>
						</div>
						
						
					<!-- <div class="card">
							<div id="ActionPanel" class="row no-margin-bottom default-margin "
							style="background-color: #006699;border:none; height: 45px; margin-right: 0px; margin-left: 0px; width: 100%">
							
							<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2 floatRight"
								style="float: right;">
								<input type="button" value="Close"
									class="btn btn-danger rightDefaultMargin alignRight"
									style="float: right; background-color: blue;"
									onClick="onCancel('../myHome.jsp')" />
							</div>
						</div>
						</div> -->
						
						
						</div>
						
					</div>
				</div>
			
			<input type="hidden" id="globalSetting" value="<%=WebUtil.processString(objGISB.getStrName())%>">
	</section>





	<jsp:include page="../MyIncludes/incBottomPageReferences.jsp"
		flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>
	<!-- Jquery Core Js -->
	<!-- <script src="../plugins/jquery/jquery.min.js"></script> -->

	<!-- Bootstrap Core Js -->
	<script src="../plugins/bootstrap/js/bootstrap.js"></script>

	<!-- Select Plugin Js -->
	<script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

	<!-- Slimscroll Plugin Js -->
	 <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script> 

	<!-- Waves Effect Plugin Js -->
	<script src="../plugins/node-waves/waves.js"></script>

	<!-- Jquery CountTo Plugin Js -->
	 <script src="../plugins/jquery-countto/jquery.countTo.js"></script> 

	<!-- Morris Plugin Js -->
	<script src="../plugins/raphael/raphael.min.js"></script>
	<script src="../plugins/morrisjs/morris.js"></script>

	 <!-- Bootstrap Material Datetime Picker Plugin Js -->
	<!-- <script
		src="../plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>  -->

	<!--  Bootstrap Datepicker Plugin Js -->
	<!-- <script
		src="../plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>  --> 

	 <!-- dialogs Sweet Alert  -->
     <script src="../js/pages/ui/dialogs.js"></script>

	<!-- Sparkline Chart Plugin Js -->
	 <script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script> 

	<!-- Custom Js -->
	<script src="../js/admin.js"></script>
	<!-- <script src="../js/pages/index.js"></script> -->

	<!-- Demo Js -->
	<!-- <script src="../js/demo.js"></script> -->
	
	<!--  multiple file upload  --> 
	
    <script src="../js/vendor/jquery.ui.widget.js"></script>
	<script src="../js/jquery.iframe-transport.js"></script>
	<script src="../js/jquery.fileupload.js"></script>
	
	 <script>
	 var userID = $("#userID").val();
	    
    $(function () {
        'use strict';
        
        var urlVal = $("#globalSetting").val();
        var strUrl = urlVal+"files-upload/event/";
        //var strUrl = "http://localhost:8089/files-upload/event/";
        
    
        $("#fileupload").prop("disabled", true);
        
        
         $('select').on('change', function() {
        	 $("#hdnET").val($("#<%=ILabelConstants.selEventName%>  option:selected").val());
        	
        	if(this.value >0){	        	
	      	  	$("#fileupload").prop("disabled", false);
        	}else{
        		$("#fileupload").prop("disabled", true);
        	} 
        	
        	
        	
        	
      	}); 
        
            
            
         $('#fileupload').fileupload({
             dataType: 'json',
             add: function (e, data) {
            	 data.url = strUrl+$("#hdnET").val()+"/"+userID;
            	 console.log('data.url: ', data.url);                 
                 data.submit();
             },
             done: function (e, data) {
                 $.each(data.result.files, function (index, file) {
                     $('<p/>').text(file.name).appendTo(document.body);
                 });
             },
             progressall: function (e, data) {
                 var progress = parseInt(data.loaded / data.total * 100, 10);
                 $('#progress .bar').css(
                     'width',
                     progress + '%',
                    
                 );
                 
                /*  $("#result").text('Uploaded Successfully'),
                 console.log("SUCCESS : ", data),
                 $("#result").show(), */
                 
             }
             
             
           /*   success: function (e,data) {

	                //$("#result").text(data);
	                console.log("SUCCESS : ", data);
	                
	              

	            },
	            error: function (e) {

	                //$("#result").text(e.responseText);
	                
	                console.log("ERROR : ", e);

	               
	            } */
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             
             
         });
            
    });

    function downloadAll(){
        var images = document.getElementsByTagName('img');
        var srcList = [];
        var i = 0;

        setInterval(function(){
            if(images.length > i){
                srcList.push(images[i].src);
                var link = document.createElement("a");
                link.id=i;
                link.download = images[i].src;
                link.href = images[i].src;
                link.click();
                i++;
            }
        },1500);
    }
    
    </script>

</body>

</html>
