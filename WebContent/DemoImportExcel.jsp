<!DOCTYPE html>

<%@page import="java.io.File"%>
<%@page import="com.uthkrushta.mybos.code.bean.MyBosGender"%>
<%@page import="com.uthkrushta.mybos.configuration.bean.AgeCategoryConfigurationBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeLapMasterBean"%>
<%@page import="com.uthkrushta.mybos.code.bean.TimeCodeBean"%>
<%@page import="com.uthkrushta.mybos.master.controller.EventCreationController"%>
<%@page import="com.uthkrushta.mybos.code.bean.CertificateTypeBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeTimeMasterBean"%>
<%@page import="com.uthkrushta.mybos.configuration.bean.EventPhotoUploadBean"%>
<%@page import="com.uthkrushta.mybos.configuration.controller.EventPhotoUploadController"%>
<%@page import="com.uthkrushta.basis.constants.ISessionAttributes"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeMasterBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventMasterBean"%>
<%@page import="java.awt.image.DataBuffer"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.uthkrushta.basis.bean.MessageBean"%>
<%@page import="com.uthkrushta.basis.controller.GenericController"%>

<%@page import="org.ajaxanywhere.AAUtils"%>
<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Advanced Form Elements | Bootstrap Based Admin Template - Material Design</title>

    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <!-- <link href="plugins/animate-css/animate.css" rel="stylesheet" /> -->

    <!-- Colorpicker Css -->
    <!-- <link href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" />
 -->
    <!-- Dropzone Css -->
    <!-- <link href="plugins/dropzone/dropzone.css" rel="stylesheet"> -->

    <!-- Multi Select Css -->
   <!--  <link href="plugins/multi-select/css/multi-select.css" rel="stylesheet"> -->

    <!-- Bootstrap Spinner Css -->
   <!--  <link href="plugins/jquery-spinner/css/bootstrap-spinner.css" rel="stylesheet"> -->

    <!-- Bootstrap Tagsinput Css -->
   <!--  <link href="plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet"> -->

    <!-- Bootstrap Select Css -->
     <link href="plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />  

    <!-- noUISlider Css -->
   <!--  <link href="plugins/nouislider/nouislider.min.css" rel="stylesheet" /> -->
	
	 <!-- Light Gallery Plugin Css -->
    <link href="plugins/light-gallery/css/lightgallery.css" rel="stylesheet">
	
    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="css/themes/all-themes.css" rel="stylesheet" />
</head>

<%


String strUserAction = "GET";





AAUtils.addZonesToRefresh(request, "List");

%>






<body class="theme-red">
<form method="post">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <section class="content">
   
   
    
        <div class="container-fluid">
            <div class="block-header">
                <h2>Import Excel</h2>
            </div>
            
            
            
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div>
   <input type="button" value="Save"  class="btn btn-primary "
									 data-type="success" id="saveID" onClick="onSave()"/> 
   
   </div>
                    <aa:zone name="List" skipIfNotIncluded="true" > 
                    <div class="card">


<div class="col-sm-6 col-lg-4" style=" padding-top: 20px; margin-top: 30px;">	
							
							<input id="fileupload" type="file" name="files[]" data-url="/EventzAlley/importMemberData"  >
							<div id="progress" style="background-color:lightgrey"> <div class="bar" style="text-align:center;color:white;width:0%"></div>
							<!-- <button type="submit">Upload</button> -->
	               <!-- <button>Upload</button> -->
	               <div id="progress"> <div class="bar" style="width: 0%;"></div>
					</div>
						
							</div>

							
							
						
							
							</div>



<div class="col-sm-6 col-lg-4" style=" padding-top: 20px; margin-top: 30px;">	
<input type="file" id="i_file" value="file2"> 
<input type="button" id="i_submit" value="Submit">
<br>
<img src="" width="200" style="display:none;" />
<br>
<div id="disp_tmp_path"></div>
</div>










					</div>
                    </aa:zone>
                </div>
            </div>
            <!-- #END# Advanced Select -->
            <jsp:include page="MyIncludes/incPageVariables.jsp" flush="true">
	                   <jsp:param name="USER_ACTION" value="<%=strUserAction%>" />
   		               <jsp:param name="IS_AJAX_SUBMIT" value="true" />
   	                   </jsp:include>
            </div>
    



</section>

    <!-- Jquery Core Js -->
   <!--  <script src="plugins/jquery/jquery.min.js"></script> -->

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
     <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script> 

    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Bootstrap Colorpicker Js -->
    <!-- <script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> -->

    <!-- Dropzone Plugin Js -->
   <!--  <script src="plugins/dropzone/dropzone.js"></script> -->

    <!-- Input Mask Plugin Js -->
   <!--  <script src="plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script> -->

    <!-- Multi Select Plugin Js -->
   <!--  <script src="plugins/multi-select/js/jquery.multi-select.js"></script> -->

    <!-- Jquery Spinner Plugin Js -->
    <!-- <script src="plugins/jquery-spinner/js/jquery.spinner.js"></script> -->

    <!-- Bootstrap Tags Input Plugin Js -->
  <!--   <script src="plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script> -->

    <!-- noUISlider Plugin Js -->
   <!--  <script src="plugins/nouislider/nouislider.js"></script> -->
	
	 <!-- Light Gallery Plugin Js -->
    <script src="plugins/light-gallery/js/lightgallery-all.js"></script>
	<script src="plugins/light-gallery/js/lg-share.js"></script>
	<script src="plugins/light-gallery/js/lg-hash.js"></script>

    <!-- Custom Js -->
    <script src="js/pages/medias/image-gallery.js"></script>
	
	
    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

	
	 <script src="js/applicationValidation.js"></script>
    <script src="js/commonActions.js"></script>
    <script src="js/aa.js"></script> 

<!--  multiple file upload  --> 
	
    <script src="../js/vendor/jquery.ui.widget.js"></script>
	<script src="../js/jquery.iframe-transport.js"></script>
	<script src="../js/jquery.fileupload.js"></script>



    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <!-- <script src="js/pages/forms/advanced-form-elements.js"></script> -->

    <!-- Demo Js -->
    <script src="js/demo.js"></script>
    <script type="text/javascript">
    
    
    $(function() {

        

        $('#fileupload').fileupload({
        	dataType: 'json',
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    $('<p/>').text(file.name).appendTo(document.body);
                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .bar').css(
                    'width',
                    progress + '%'
                );
            }
        }); 
        
      });
    
    
    
    
    
    
    
    
    $('#i_file').change( function(event) {
        var tmppath = URL.createObjectURL(event.target.files[0]);
        $("img").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));

        $("#disp_tmp_path").html("Temporary Path(Copy it and try pasting it in browser address bar) --> <strong>["+tmppath+"]</strong>");
    });
    
    </script>
 
    
    </form>
</body>

</html>
