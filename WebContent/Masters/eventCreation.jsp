<!DOCTYPE html>

<%@page import="com.uthkrushta.mybos.code.bean.MyBosGender"%>
<%@page import="com.uthkrushta.mybos.configuration.bean.AgeCategoryConfigurationBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeLapMasterBean"%>
<%@page import="com.uthkrushta.mybos.code.bean.TimeCodeBean"%>
<%@page import="com.uthkrushta.mybos.master.controller.EventCreationController"%>
<%@page import="com.uthkrushta.mybos.code.bean.CertificateTypeBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeTimeMasterBean"%>
<%@page import="com.uthkrushta.mybos.configuration.bean.EventPhotoUploadBean"%>
<%@page import="com.uthkrushta.mybos.configuration.controller.EventPhotoUploadController"%>
<%@page import="com.uthkrushta.basis.constants.ISessionAttributes"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeMasterBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventMasterBean"%>
<%@page import="java.awt.image.DataBuffer"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.uthkrushta.basis.bean.MessageBean"%>
<%@page import="com.uthkrushta.basis.controller.GenericController"%>

<%@page import="org.ajaxanywhere.AAUtils"%>
<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<html>
<%
	String strContextPath = "../";
	strContextPath = WebUtil.parseString(request, IUMCConstants.CONTEXT_PATH);
%>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>Eventzalley</title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" type="text/css">

 <!-- Bootstrap DatePicker Css -->
    <link href="../plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
<link href="../Style/default.css"
	rel="stylesheet" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<!-- Bootstrap Material Datetime Picker Css -->
    <link href="../plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" /> 

<!-- Bootstrap Select Css -->
     <link href="../plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />  

  <link href= "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="../css/themes/all-themes.css" rel="stylesheet" /> 
<style type="text/css">
.handsOn{
cursor:pointer;
}

/* .table-responsive {
		  overflow-y: visible !important;
		} */
		/*  .table-responsive .dropdown-menu {
        position: static !important;}  */
       /*  .table-responsive {
        overflow: inherit;
    } */
    @media (max-width: 767px) {
    .table-responsive .dropdown-menu {
        position: static !important;
    }
}
@media (min-width: 768px) {
    .table-responsive {
        overflow: visible;
    }
}
    
</style>

<script type="text/javascript">
  
  
  
  
  
function onSubmitVal()
{
	var validateDate =0;
	
	var startDate = $("#<%=ILabelConstants.START_DATE%>").val();
	var endDate = $("#<%=ILabelConstants.END_DATE%>").val();
	var oneDay = 24*60*60*1000;
	
	startDate = startDate.split('-'); 
	endDate = endDate.split('-'); 
	startDate = new Date(startDate[2], startDate[1]-1, startDate[0]);
 	endDate = new Date(endDate[2], endDate[1]-1, endDate[0]);
	
 	if(Math.round(  (endDate.getTime() - startDate.getTime()) / (oneDay)  ) < 0)  
	  {
 		
      alert("End date must be greater than start date"); 	
      $('#<%=ILabelConstants.END_DATE%>').val('');
      return false; 
	  } 
 	
 	$( ".typeStartDate" ).each(function( index ) {
 		  console.log( index + ": " + $( this ).val() );
 		  if(validateTypeDate($(this).val()) == false){
 			 validateDate = 1
 		  }
 		  
 		});
	
	
 	if(validateDate == 1){
 		alert("Please Select Valid Competition Date");
 		return;
 	}
 	
	document.forms[0].hdnUserAction.value = "SUBMIT";
   /* alert(document.forms[0].hdnUserAction.value);*/
   if(ValidateForm())
    	{
   
		if(null!= document.forms[0].hdnAjaxSubmit && document.forms[0].hdnAjaxSubmit.value.toLowerCase()== 'true')	
		{
			
			ajaxAnywhere.submitAJAX();
		}
		else
		{
			document.forms[0].submit();
		}
}
}
  function validateTypeDate(date){
	  
	  var startDate = $("#<%=ILabelConstants.START_DATE%>").val();
		var endDate = $("#<%=ILabelConstants.END_DATE%>").val();
		
		var startDD = startDate.substring(0,2);
		var startMM = startDate.substring(3,5);
		var startYY = startDate.substring(6,10);
		
		var endDD = endDate.substring(0,2);
		var endMM = endDate.substring(3,5);
		var endYY = endDate.substring(6,10);
		
		  var valStartDate = startYY.concat(startMM.concat(startDD));
		  var valEndDate = endYY.concat(endMM.concat(endDD));
		  
		  var dateDD = date.substring(0,2);
		var dateMM = date.substring(3,5);
			var dateYY = date.substring(6,10);
			
			 var valDateDate = dateYY.concat(dateMM.concat(dateDD));
		
	 	if( valDateDate < valStartDate || valEndDate < valDateDate){
	 		return false;
	 	}
	 	else{
	 		return true; 
	 	}
  }
  
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 57)) {
        return false;
    }
    return true;
}


function addSplits(eventTypeId)
{
	$("#<%=ILabelConstants.hdnEventTypeID%>").val(eventTypeId);
	document.forms[0].hdnUserAction.value = "<%=ILabelConstants.SplitCreation%>";
	if(document.forms[0].hdnAjaxSubmit.value.toLowerCase() == "true")	
	{
		ajaxAnywhere.submitAJAX();
	}
	else
	{
		document.forms[0].submit();
	} 
	
	
	}

function onAddAgeCategory()
{
	
	document.forms[0].hdnUserAction.value = "<%=ILabelConstants.AgeCategoryCreation%>";
	if(document.forms[0].hdnAjaxSubmit.value.toLowerCase() == "true")	
	{
		ajaxAnywhere.submitAJAX();
	}
	else
	{
		document.forms[0].submit();
	} 
	}

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      //  document.getElementById("myBtn").style.display = "block";
    } else {
        // document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
    
function deleteRow(objTD){
	  
	  if(!confirm("Do you want to delete the selected Item ?")){
			return
		}
	  var Row = $(objTD).parent();
	  var objHdnRowCounter = $("#<%=ILabelConstants.hdnItemRowCounter%>");
	
	intIndex = Number($(objHdnRowCounter).val());

	var objHdnItem = $(Row).find("#<%=ILabelConstants.hdnInvItemID%>");
	intRecCount =Number($(objHdnRowCounter).val());
	
  intRecCount = intRecCount -1;
  
 	$("#hdnRowCounter").val(intRecCount);
	
	 if(Number($(objHdnItem).val())>0)
	{	
		var deleteId = $(".SelItemID").val();
		$(".SelItemID").val(deleteId+""+$(objHdnItem).val()+";");
		$("#hdnUserAction").val("<%=IUMCConstants.ACTION_DELETE_ITEM%>"); 
		$(Row).remove(); 
		
		ajaxAnywhere.submitAJAX();
		
	}else{
		$(Row).remove(); 
		
		ajaxAnywhere.submitAJAX();
		
	}
	
}
 function SaveSplit(){
	 document.forms[0].hdnUserAction.value = "SAVE_SPLIT";
		if(document.forms[0].hdnAjaxSubmit.value.toLowerCase() == "true")	
		{
			ajaxAnywhere.submitAJAX();
		}
		else
		{
			document.forms[0].submit();
		} 
 }  
   
 function onAddAgeCategoryRow()
 {

 	var categoryName =$("#<%=ILabelConstants.textAgeCategoryName%>").val();
 	var genderID=$("#<%=ILabelConstants.txtSelectgender%> option:selected").val();
 	/* alert("hiiii bye = "+genderID)
 	return; */
 	var minAge=$("#<%=ILabelConstants.txtMinAge%>").val();
 	var maxAge=$("#<%=ILabelConstants.txtMaxAge%>").val();
 	var eventTypeID=$("#<%=ILabelConstants.txtEventType%> option:selected").val();
 	
 	var intAddCount = 1;
  	
 	var objHdnRowCount = $("#<%=ILabelConstants.hdnItemRowCounterAge%>");	 // getting hdnItemRowCounter and storing in variable
 	intRowCounterAge =Number($(objHdnRowCount).val());						// variable to convert number and seeting it to intRowCounter
 	intRowCounterAge++;
  	
  	objItemTable = $("#tbodyAddIdAge");
 	objItemRow = $("#ItemRowAge");
 	
 	//getting the structure of tr and appending 
 	var objNewItem = objItemRow.html();
 	var objNewRow = $("<tr></tr>").append(objNewItem);
 	
 	var objRow1=objNewRow.find(".<%=ILabelConstants.textAgeCategoryName%>");
 	    	objRow1.attr('name',objRow1.attr('name')+intRowCounterAge);
 	    	objRow1.val(categoryName);
 	
 	    	var objRow2=objNewRow.find(".<%=ILabelConstants.txtSelectgender%>");
 	    	objRow2.attr('name',objRow2.attr('name')+intRowCounterAge);
 	    	objRow2.val(genderID);
 	    	
 	    	var objRow3=objNewRow.find(".<%=ILabelConstants.txtMinAge%>");
 	    	objRow3.attr('name',objRow3.attr('name')+intRowCounterAge);
 	    	objRow3.val(minAge);
 		
 	    	var objRow4=objNewRow.find(".<%=ILabelConstants.txtMaxAge%>");
 	    	objRow4.attr('name',objRow4.attr('name')+intRowCounterAge);
 	    	objRow4.val(maxAge);
 	    	
 	    	<%-- var objRow5=objNewRow.find(".<%=ILabelConstants.txtEventType%>");
 	    	objRow5.attr('name',objRow5.attr('name')+intRowCounterAge);
 	    	objRow5.val(eventTypeID); --%>
 	
 	//Setting intRowCounterSplit to
 	$("#<%=ILabelConstants.hdnItemRowCounterAge%>").val(intRowCounterAge);	    	
 	$("#<%=ILabelConstants.hdnRowCounterAge%>").val(intRowCounterAge);
 	
 	
 	// to append new row to body	    	
 	objItemTable.append(objNewRow);
 	
 	
  	
 	if(document.forms[0].hdnAddCount != null)
 	{
 		intAddCount = Number(document.forms[0].hdnAddCount.value);
 	}
 	if(document.forms[0].hdnRowCounterAge != null)
 	{
 		document.forms[0].hdnRowCounterAge.value = Number(document.forms[0].hdnRowCounterAge.value) + Number(intAddCount);
 		document.forms[0].hdnUserAction.value = "ADD_AGE";
 		if(document.forms[0].hdnAjaxSubmit.value.toLowerCase() == "true")	
 		{
 			ajaxAnywhere.submitAJAX();
 		}
 		else
 		{
 			document.forms[0].submit();
 		}
 		
 	}
 	else
 	{
 		alert("HTML Error: hdnRowCounter is missing")
 		
 	}
 }
    
 
   
function onAddSplitRow()
{

	var splitOrder =$("#<%=ILabelConstants.SPLIT_ORDER%>").val();
	var splitName=$("#<%=ILabelConstants.SPLIT_NAME%>").val();
	var splitLabel=$("#<%=ILabelConstants.SPLIT_LABEL_NAME%>").val();
	
	var intAddCount = 1;
 	
	var objHdnRowCount = $("#<%=ILabelConstants.hdnItemRowCounterSplit%>");	 // getting hdnItemRowCounter and storing in variable
	intRowCounterSplit =Number($(objHdnRowCount).val());						// variable to convert number and seeting it to intRowCounter
	intRowCounterSplit++;
 	
 	objItemTable = $("#tbodyAddIdSplit");
	objItemRow = $("#ItemRowSplit");
	
	//getting the structure of tr and appending 
	var objNewItem = objItemRow.html();
	var objNewRow = $("<tr></tr>").append(objNewItem);
	
	var objRow1=objNewRow.find(".<%=ILabelConstants.SPLIT_ORDER%>");
	    	objRow1.attr('name',objRow1.attr('name')+intRowCounterSplit);
	    	objRow1.val(splitOrder);
	
	    	var objRow2=objNewRow.find(".<%=ILabelConstants.SPLIT_NAME%>");
	    	objRow2.attr('name',objRow2.attr('name')+intRowCounterSplit);
	    	objRow2.val(splitName);
	    	
	    	var objRow3=objNewRow.find(".<%=ILabelConstants.SPLIT_LABEL_NAME%>");
	    	objRow3.attr('name',objRow3.attr('name')+intRowCounterSplit);
	    	objRow3.val(splitLabel);
	
	
	//Setting intRowCounterSplit to
	$("#<%=ILabelConstants.hdnItemRowCounterSplit%>").val(intRowCounterSplit);	    	
	$("#<%=ILabelConstants.hdnRowCounterSplit%>").val(intRowCounterSplit);
	
	// to append new row to body	    	
	objItemTable.append(objNewRow);
	
	if(document.forms[0].hdnAddCount != null)
	{
		intAddCount = Number(document.forms[0].hdnAddCount.value);
	}
	if(document.forms[0].hdnRowCounterSplit != null)
	{
		document.forms[0].hdnRowCounterSplit.value = Number(document.forms[0].hdnRowCounterSplit.value) + Number(intAddCount);
		document.forms[0].hdnUserAction.value = "ADD_SPLIT";
		if(document.forms[0].hdnAjaxSubmit.value.toLowerCase() == "true")	
		{
			ajaxAnywhere.submitAJAX();
		}
		else
		{
			document.forms[0].submit();
		}
	}
	else
	{
		alert("HTML Error: hdnRowCounter is missing")
	}
}
    
    
function deleteSplitRow(objTD){
	  
	  if(!confirm("Do you want to delete the selected Item ?")){
			return
		}
	  var Row = $(objTD).parent();
	  var objHdnRowCounter = $("#<%=ILabelConstants.hdnItemRowCounterSplit%>");
	  
	  intIndex= Number($(objHdnRowCounter).val());
	
	  var objHdnItem = $(Row).find("#<%=ILabelConstants.hdnInvItemSplitID%>");
	  intRecCount =Number($(objHdnRowCounter).val());
	intRecCount = intRecCount -1;
 <%--  $('#<%=ILabelConstants.hdnItemRowCounterSplit%>').val(intRecCount); --%>
      	
	/* $("#hdnItemRowCounterSplit").val(intRecCount); */
	
	if(Number($(objHdnItem).val())>0)
	{	
		var deleteId = $(".SelItemIDSplit").val();
		$(".SelItemIDSplit").val(deleteId+""+$(objHdnItem).val()+";");
		$("#hdnUserAction").val("<%=ILabelConstants.ACTION_DELETE_SPLIT_ITEM%>"); 
		$(Row).remove(); 	
		
		ajaxAnywhere.submitAJAX();
		
		
	}else{
		
		$(Row).remove(); 
		
	}
	
}
function deleteAgeRow(objTD){
	  
	  if(!confirm("Do you want to delete the selected Item ?")){
			return
		}
	  var Row = $(objTD).parent();
	  var objHdnRowCounter = $("#<%=ILabelConstants.hdnItemRowCounterAge%>");
	  
	  intIndex= Number($(objHdnRowCounter).val());
	
	  var objHdnItem = $(Row).find("#<%=ILabelConstants.hdnInvItemAgeID%>");
	  intRecCount =Number($(objHdnRowCounter).val());
	intRecCount = intRecCount -1;
$('#<%=ILabelConstants.hdnItemRowCounterAge%>').val(intRecCount);
    	

	$("#hdnItemRowCounterAge").val(intRecCount);
	
	//alert("Number($(objHdnItem).val())--- = "+Number($(objHdnItem).val()))
	if(Number($(objHdnItem).val())>0)
	{	
		var deleteId = $(".SelItemIDAGE").val();
		$(".SelItemIDAge").val(deleteId+""+$(objHdnItem).val()+";");
		$("#hdnUserAction").val("<%=ILabelConstants.ACTION_DELETE_AGE_ITEM%>"); 
		$(Row).remove(); 	
		
		ajaxAnywhere.submitAJAX();
		
		
	}else{
		
		$(Row).remove(); 
		//alert('alert')
		/* ajaxAnywhere.submitAJAX(); */
		
		
	}
	
} 




    </script>

</head>

<%
	int Index = 1;
	String strUserAction = "GET";
	String strStatusText = "";
	String strStatusStyle = "";
	String strPageType = "";
	String strSelectedItemID = ";";
	String strSplitSelectedItemID = ";";
	int intRowCounter = 0;
	int intRowCounterSplit = 0;
	int intRowCounterAge = 0;
	long hdnlngEventTypeId = 0;
	String strMultipleSelect = "";
	
	
	ArrayList<MessageBean> arrMsg = null; 
	MessageBean objMsg;
	
	EventMasterBean objEMB = new EventMasterBean();
	EventTypeMasterBean objETMB = new EventTypeMasterBean();
	EventTypeTimeMasterBean objETTMB = new EventTypeTimeMasterBean();
	CertificateTypeBean objCTB = new CertificateTypeBean();
	EventCreationController objGC = new EventCreationController();
	EventTypeMasterBean objETMB2 = new EventTypeMasterBean();
	EventTypeLapMasterBean objETLMB2 = new EventTypeLapMasterBean();
	AgeCategoryConfigurationBean objACCB = new AgeCategoryConfigurationBean();
	List lstEventLap2 = null;
	List lstAgeCategory = null;
	List lstEventTypeResult = null;
	List lstGender = DBUtil.getRecords(ILabelConstants.CD_GENDER_BN, IUMCConstants.GET_ACTIVE_ROWS, "lngID");
	MyBosGender objGender = new MyBosGender();
	EventTypeMasterBean objETMB3 = new EventTypeMasterBean();
	List lstEventType = null;
	long eventID = 0;
	List lstCertificateType = (List) DBUtil.getRecords(ILabelConstants.CD_CERTIFICATE_TYPE_BN, IUMCConstants.GET_ACTIVE_ROWS, "strName");
	List lstTime = (List) DBUtil.getRecords(ILabelConstants.CD_TIME_BN, IUMCConstants.GET_ACTIVE_ROWS, "lngID");
	List lstEventTypeTime = null;
	
	strPageType = WebUtil.parseString(request, ILabelConstants.PAGE_TYPE);
	if (strPageType.equalsIgnoreCase("Edit")) {
		eventID = WebUtil.parseLong(request, ILabelConstants.ID);
		objEMB = (EventMasterBean) DBUtil.getRecord(ILabelConstants.BN_EVENT_MASTER,
				"lngID=" + eventID + " and " + IUMCConstants.GET_ACTIVE_ROWS, "");
		if (null == objEMB) {
			objEMB = new EventMasterBean();
		}
		
	}
	
	
	if(eventID > 0){
		
		lstEventType = (List) DBUtil.getRecords(ILabelConstants.BN_EVENT_TYPE_MASTER, 
				IUMCConstants.GET_ACTIVE_ROWS+" AND lngEventID = "+eventID, "lngID");
		if(lstEventType != null){
			intRowCounter = lstEventType.size();
		}
		
	}
	
			
	if (AAUtils.isAjaxRequest(request)) {
		strUserAction = WebUtil.processString(request.getParameter(ILabelConstants.hdnUserAction));
		intRowCounter = WebUtil.parseInt(request.getParameter(ILabelConstants.hdnRowCounter));
	
		
		objGC.doAction(request, response);
		if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_SUBMIT))  {
			objEMB = (EventMasterBean) objGC.get(null);
			if(objEMB == null){
				objEMB = new EventMasterBean();
			}
			lstEventType = (List) DBUtil.getRecords(ILabelConstants.BN_EVENT_TYPE_MASTER, IUMCConstants.GET_ACTIVE_ROWS+" AND lngEventID = "+objEMB.getLngID(), "lngID");
			if(lstEventType != null && !strUserAction.equalsIgnoreCase(IUMCConstants.ADD) ){
				intRowCounter = lstEventType.size();
			}else{
				intRowCounter = 0;
			}
		}
		
		else if(strUserAction.equalsIgnoreCase(ILabelConstants.SplitCreation))  {
			objEMB = (EventMasterBean) objGC.get(request);
			if(objEMB == null){
				objEMB = new EventMasterBean();
						}
			
			hdnlngEventTypeId = WebUtil.parseLong(request, ILabelConstants.hdnEventTypeID);
			if(hdnlngEventTypeId > 0){
				objETMB2 = (EventTypeMasterBean) DBUtil.getRecord(ILabelConstants.BN_EVENT_TYPE_MASTER, IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+hdnlngEventTypeId, "");
				if(objETMB2 == null){
					objETMB2 = new EventTypeMasterBean();
				}else{
					lstEventLap2 = (List)DBUtil.getRecords(ILabelConstants.BN_EVENT_TYPE_LAP, IUMCConstants.GET_ACTIVE_ROWS+" AND lngEventTypeID = "+objETMB2.getLngID(), "lngLapOrder");
					if(lstEventLap2 != null && lstEventLap2.size() > 0){
						intRowCounterSplit = lstEventLap2.size();
					}
					
				}
				
				
			}
			AAUtils.addZonesToRefresh(request, "PastData");
			
			
		}
		
		else if(strUserAction.equalsIgnoreCase(ILabelConstants.AgeCategoryCreation))  {
			 
			lstAgeCategory = (List) DBUtil.getRecords(ILabelConstants.BN_AGE_CATEGORY_CONFIG, IUMCConstants.GET_ACTIVE_ROWS+" AND lngEventID ="+eventID, "lngEventTypeID");
			if(lstAgeCategory != null && lstAgeCategory.size()>0){
			intRowCounterAge= lstAgeCategory.size();
			}
			
			 AAUtils.addZonesToRefresh(request, "AgeCategoryData"); 
			
			
		}
		
		
		else{
			
			objEMB = (EventMasterBean) objGC.get(request);
			if(objEMB == null){
				objEMB = new EventMasterBean();
						}
			lstEventType = (List) DBUtil.getRecords(ILabelConstants.BN_EVENT_TYPE_MASTER, IUMCConstants.GET_ACTIVE_ROWS+" AND lngEventID = "+objEMB.getLngID(), "lngID");
			if(lstEventType != null && !strUserAction.equalsIgnoreCase(IUMCConstants.ADD) ){
				intRowCounter = lstEventType.size();
			}else if(!strUserAction.equalsIgnoreCase(IUMCConstants.ADD)){
				intRowCounter = 0;
			}
		
		}
		
		
		lstEventType = (List) DBUtil.getRecords(ILabelConstants.BN_EVENT_TYPE_MASTER, IUMCConstants.GET_ACTIVE_ROWS+" AND lngEventID = "+objEMB.getLngID(), "lngID");
		if(lstEventType != null && !strUserAction.equalsIgnoreCase(IUMCConstants.ADD) ){
			intRowCounter = lstEventType.size();
		}
		
		
		
		
	}
	
	 arrMsg = objGC.getArrMsg();		//Getting the messages to display
		if(null!= arrMsg && arrMsg.size() > 0)
		{
			objMsg = MessageBean.getLeadingMessage(arrMsg);
			if(objMsg != null)
			{
		strStatusText = objMsg.getStrMsgText();
		strStatusStyle = objMsg.getStrMsgType();
			}
		}
	AAUtils.addZonesToRefresh(request, "List,SplitList,AgeCategoryList,Status");
	
	
	
	
	
%>



<body class="theme-blue" style="background-color: #e9e9e9;">
<form method="post">
	<!-- Page Loader -->




	<jsp:include page="../MyIncludes/incPageHeader.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
		 
	</jsp:include>
	<!-- #Top Bar -->

<style type="text/css">
		.bar {
		    height: 18px;
		    background: green;
		}
	</style>

	<jsp:include page="../MyIncludes/incLeftPanel.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>

	




	<section class="content">
	
	
		
			
	 <jsp:include page="../MyIncludes/incPageHeaderInfo.jsp" flush="true">
					 
					    <jsp:param name="STATUS_STYLE" value="<%=strStatusStyle%>" />
					    <jsp:param name="STATUS_TEXT" value="<%=strStatusText%>" />
					</jsp:include> 
					
		<div class="container-fluid">
		
		<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header" >
							<h2 >Event Creation</h2>
						</div>
						<div id="ActionPanel" class="row no-margin-bottom default-margin "
							style="background-color: #006699;border:none; height: 50px; margin-right: 0px; margin-left: 0px; width: 100%">
							
							<div class="col-xs-10 col-md-10 col-sm-10 col-lg-10 floatleft" style="margin-top:4px">
								<input type="button" value="Add New"  class="btn btn-primary "
									  onClick="gotoThisPage('eventCreation.jsp')"/> 
								 <input type="button" value="Save"  class="btn btn-primary "
									 data-type="success" id="saveID" onClick="onSubmitVal(),topFunction()"/> 
									  
									
							</div>
							<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2 floatRight"
								style="float: right; margin-top:4px">
								<input type="button" value="Close"
									class="btn btn-danger rightDefaultMargin alignRight"
									style="float: right; background-color: blue;"
									onClick="onCancel('eventsList.jsp')" />
							</div>
						</div>
						<aa:zone name="header" skipIfNotIncluded="true" >  
                        <div class="body">
                            <div class="row clearfix">
                               
                                  
                                 <div class="col-xs-12 col-md-4 col-sm-12 col-lg-4 " style="margin-bottom: 0px;">

										<div class="form-group form-float" style="margin-top: 25px;">

											<div class="form-line focused">
											<input type="hidden" name="<%=ILabelConstants.hdnID %>" value="<%=objEMB.getLngID()%>" >  
											 <input type="hidden" name="<%=ILabelConstants.hdnCreatedBy%>" value="<%=objEMB.getLngCreatedBy()%>">
                        					<input  type="hidden" name="<%=ILabelConstants.hdnCreatedon%>" value="<%=objEMB.getDtCreatedOn()%>" >
												<input type="hidden" name="<%=ILabelConstants.hdnUpdatedBy%>" value="<%=objEMB.getLngUpdatedBy()%>">
                        					<input  type="hidden" name="<%=ILabelConstants.hdnUpdatedOn%>" value="<%=objEMB.getDtUpdatedOn()%>" >
												<label class="form-label" >Event
													Name*</label>
												<input type="text"   class="form-control"
													name="<%=ILabelConstants.EventName%>"
													id="<%=ILabelConstants.EventName%>"
													FieldType="mandatory" FieldName="Event Name"
													value="<%=WebUtil.processString(objEMB.getStrName())%>"
													style="background-color: white" > 
													

											</div>
										</div>
								</div>
								 
                                 
								<div class="col-xs-12 col-md-4 col-sm-12 col-lg-4 " style="margin-bottom: 15px;  margin-top: 10px;">

									<div class="form-group form-float" style="margin-bottom: 0px;  margin-top: 15px;">
                                         <div class="form-line" id="bs_datepicker_container">
                                            <input type="text" name="<%=ILabelConstants.START_DATE %>" id="<%=ILabelConstants.START_DATE %>" FieldType="mandatory" FieldName="Start Date" class="datepicker form-control" placeholder="Start Date*"
                                            value="<%=WebUtil.formatDate(objEMB.getDtStartDate(), IUMCConstants.DATE_FORMAT)%>">
                                        </div> 
                                        <!-- <div class="form-line" id="bs_datepicker_container">
                                            <input type="text" class="form-control" placeholder="Please choose a date...">
                                        </div> -->
                                    </div>
								</div>
								<div class="col-xs-12 col-md-4 col-sm-12 col-lg-4 " style="margin-bottom: 15px;  margin-top: 10px;">

									<div class="form-group form-float" style="margin-bottom: 0px;  margin-top: 15px;">
                                        <div class="form-line" id="bs_datepicker_container">
                                            <input type="text" name="<%=ILabelConstants.END_DATE%>" id="<%=ILabelConstants.END_DATE%>" FieldType="mandatory" FieldName="End Date" class="datepicker form-control" placeholder="End Date*"
                                            value="<%=WebUtil.formatDate(objEMB.getDtEndDate(), IUMCConstants.DATE_FORMAT)%>">
                                        </div>
                                    </div>
								</div>  
                                   
                                   <div class="col-xs-12 col-md-4 col-sm-12 col-lg-4 " style="margin-bottom: 0px;">

										<div class="form-group form-float" style="margin-top: 25px;">

											<div class="form-line focused">
												<input type="text" class="form-control" FieldType="mandatory" FieldName="Event Place"
													name="<%=ILabelConstants.EventPlaceName%>"
													id="<%=ILabelConstants.EventPlaceName%>"
													value="<%=WebUtil.processString(objEMB.getStrPlace()) %>"
													style="background-color: white"> <label
													class="form-label" >Event
													Place*</label>

											</div>
										</div>
								</div> 
								
								<div class="col-xs-12 col-md-4 col-sm-12 col-lg-4 " style="margin-bottom: 0px;">
									
										<div class="form-group form-float" style="margin-top: 25px;">
								<div class="col-xs-10 col-md-7 col-sm-10 col-lg-7 "><span>Is Event Completed?</span></div>
								<div class="col-xs-2 col-md-5 col-sm-2 col-lg-5 "> <input type="checkbox" id="md_checkbox_29" class="filled-in chk-col-teal" name="<%=ILabelConstants.IsEventCompleted%>" <%if(objEMB.getIntIsCompleted()== 1){%>checked<%} %> value="1" />
                                <label for="md_checkbox_29"></label></div>
                                    </div>
                                    </div>
                                   <div class="col-xs-12 col-md-4 col-sm-12 col-lg-4 " >

									<div class="form-group form-float" style="margin-bottom: 0px;  margin-top: 15px;">
									<div class="col-xs-6 col-md-6 col-sm-6 col-lg-6 " >
                                        	 <input type="button" class="btn btn-primary"  value="Add Age Category" data-toggle="modal"  <%if(objEMB.getLngID() == 0){ %>disabled<%} %> onClick="onAddAgeCategory()" data-target="#largeModalForAge"> 
                                      </div>    <div class="col-xs-6 col-md-6 col-sm-6 col-lg-6 " > 
                                            <input type="button" class="btn btn-primary" style="background-color:#1f91f3" value="Add Competition" <%if(objEMB.getLngID() == 0){ %>disabled<%} %> onclick="onAddRow()" >
                                        </div>
                                    </div>
								</div>   
                                    
                                    

                                
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                </div>
                            
                        </div>
                        </aa:zone>
                    </div>
                </div>
            </div>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
			
		</div>
			
			
	</section>
	  
	<section class="content details" style="margin-top: 0px; padding-bottom: 30px;">
        <div class="container-fluid">
            <!-- Example Tab -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                       
                      <aa:zone name="List" skipIfNotIncluded="true" >  
                   	<div class="body " style="margin-bottom: 0px; "> <!-- table-responsive  -->
                   	<div class="row clearfix">
                   <!-- <div class="body table-responsive" style="padding-top: 0px;padding-left: 0px;padding-right: 0px;"> -->
                    <table id="dataTable" class="table table-hover">
                      <thead>
                        <tr>
                         
                        <%-- <th style="padding-bottom: 0px;">  <!-- style="width:15px" align="center" --> 
                         
                         <div align="center" class="">
                         	 <input type="checkbox" id="md_checkbox_29" class="filled-in chk-col-teal" name="<%=ILabelConstants.chkMain%>" value="1" onclick="SelectAll('chkChild_',this,hdnRowCounter,hdnSelectedID)" />
                                <label for="md_checkbox_29"></label>
                          </div>
                         
                         </th> --%>
                          <th style=" padding-bottom: 15px;">Competition Name*</th>
                          <th style=" padding-bottom: 15px;">Total Distance(km)</th>
                          <th style=" padding-bottom: 15px;">Start Time</th>
                          <th style=" padding-bottom: 15px;">Start Date*</th>
                          <th style=" padding-bottom: 15px;">Certificate*</th>
                          <th style=" padding-bottom: 15px;">Is Timed</th>
                          <th style=" padding-bottom: 15px;"></th> <!-- Splits -->
                          <th style=" padding-bottom: 15px;">Excel</th>
                          
                         <!-- In future may be  -->
                          <!-- <th style=" padding-bottom: 12px;"> <button type="button" class="btn btn-danger  waves-effect waves-circle waves-float" style="padding-top: 0px; padding-bottom: 0px;  padding-left: 0px;  padding-right: 0px;">
                          <a onclick="onDelete()"><i class="material-icons" style="color:white">delete</i></a>
                          </button>
                          </th> -->
                    		
                        	
                        	<th style=" padding-bottom: 15px;">Delete</th>
                           </tr>
                      </thead> 
            
                      <tbody >
                      
                  <%
	                 			 for(Index =1 ; Index <= intRowCounter; Index++){
                  	    					if(!strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_ADD)){
                  	    						if(null != lstEventType){
                  	    						objETMB = (EventTypeMasterBean)	lstEventType.get(Index-1);
                  	    						objETTMB = (EventTypeTimeMasterBean) DBUtil.getRecord(ILabelConstants.BN_EVENT_TYPE_TIME, IUMCConstants.GET_ACTIVE_ROWS+" AND lngEventTypeID = "+objETMB.getLngID(), "");
                  	    						 	if(objETTMB == null){
                  	    						 		objETTMB = new EventTypeTimeMasterBean();
                  	    						 	}
                  	    							
                  	    						}
                  	    						if(objETMB.getLngID() > 0){
                  	    						lstEventTypeResult = DBUtil.getRecords(ILabelConstants.BN_EVENT_RESULT, IUMCConstants.GET_ACTIVE_ROWS+" AND lngEventTypeID = "+objETMB.getLngID() , "");
                  	    						}
                  	    						
                  	    						if(null == lstEventType){
                  	    							objETMB = new EventTypeMasterBean();
                  	    							
                  	    						}
                  	    					}	else{
                  	    						objETMB = new EventTypeMasterBean();
                  	    						objETMB.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnID_ + Index));
                  	    						objETMB.setStrName(WebUtil.parseString(request, ILabelConstants.txtEventTypeName_+ Index));
                  	    						objETMB.setLngCertificateTypeID(WebUtil.parseLong(request, ILabelConstants.txtCertificateType_+Index));
                  	    						objETMB.setLngIsTimed(WebUtil.parseLong(request, ILabelConstants.chkIsTimed_+Index));
                  	    						objETMB.setLngTotalDistance(WebUtil.parseLong(request, ILabelConstants.TotalDistance_+Index));
                  	    						
                  	    						
                  	    						objETTMB = new EventTypeTimeMasterBean();
                  	    						objETTMB.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnTimeID_ + Index));
                  	    						objETTMB.setLngStartTimeID(WebUtil.parseLong(request, ILabelConstants.START_TIME_+ Index));
                  	    						objETTMB.setDtStartDate(WebUtil.parseDate(request, ILabelConstants.START_DATE_+ Index, IUMCConstants.DATE_FORMAT));
                  	    						System.out.println("start date : "+WebUtil.parseDate(request, ILabelConstants.START_DATE_+ Index, IUMCConstants.DATE_FORMAT));
                  	    					}
                  	    							
                  	    							
                  	    						
                  %>
                      
                      
                        <tr id="tableRow" >
                         
                          <td> 
                          <div class="form-line">
                          <input type="hidden" name="<%=ILabelConstants.hdnCreatedBy_+ Index%>" value="<%=objETMB.getLngCreatedBy()%>">
                        <input  type="hidden" name="<%=ILabelConstants.hdnCreatedon_+ Index%>" value="<%=objETMB.getDtCreatedOn()%>" >
                          <input FieldType="mandatory" FieldName="Competition Name"  class="validate[required] form-control"  type="text"  name="<%= ILabelConstants.txtEventTypeName_+Index %>"  value="<%=WebUtil.processString(objETMB.getStrName() )%>"  >
                         </div>
                         
                         <input type="hidden"
									name="<%=ILabelConstants.hdnItemRowCounter%>" size="2"
									value="<%=intRowCounter%>"
									id="<%=ILabelConstants.hdnItemRowCounter%>">
									<input type="hidden"
												name="<%=ILabelConstants.hdnInvItemID_%>"
												ID="<%=ILabelConstants.hdnInvItemID%>"
												value="<%=objETMB.getLngID()%>" size="2">
									<input
									type="hidden" class="SelItemID"
									name="<%=ILabelConstants.hdnSelectedItemID%>"
									value="<%=strSelectedItemID%>" size="20">
                         		
                           	<input type="hidden" name="<%=ILabelConstants.hdnID_ + Index %>" value="<%=objETMB.getLngID() %>" SIZE="2" >   
                          		
                          	
                         
                          </td>
                          <input type="hidden" name="<%=ILabelConstants.hdnTimeID_+ Index %>" value="<%=objETTMB.getLngID() %>" SIZE="2" > 
                        <td>
                        <input type="text" class="validate[required] form-control" name="<%=ILabelConstants.TotalDistance_+ Index %>" value="<%=objETMB.getLngTotalDistance()%>" onkeypress="return isNumber(event)">
                        
                        </td>
						
						<td style="width: 111px;">  
                          <select   class="form-control show-tick "  name="<%=ILabelConstants.START_TIME_+Index%>"  > <!-- FieldType="mandatory" FieldName="Select Time" -->
                         <option selected value="0">--- Select ---</option>
							 <%
                          if(lstTime != null){
                        	for(int i=0;i<lstTime.size();i++)
                        	{
                        		TimeCodeBean objTCB = (TimeCodeBean) lstTime.get(i);
                        	
                        		
                        	
                        	  %> 
                        	  <option  value="<%=objTCB.getLngID()%>"<% if(objTCB.getLngID() == objETTMB.getLngStartTimeID() ) {%> selected <%} %> ><%= objTCB.getStrName() %></option>
                        	  
                        	  
                        	  
                        <% }
                          }
                          %> 
                          </select>
                         
                          </td> 
                           <td>
                           
                           <div class="form-line" id="bs_datepicker_container"> 
                           <input type="text" name="<%=ILabelConstants.START_DATE_+Index %>" id="<%=ILabelConstants.START_DATE_+Index %>"  FieldType = "mandatory" FieldName="Competition Start Date"    class="datepicker form-control typeStartDate" placeholder="Competition Start Date"
                           <%if(objETTMB.getLngID() > 0){ %>  value="<%=WebUtil.formatDate(objETTMB.getDtStartDate(), IUMCConstants.DATE_FORMAT) %>"<%}else{%> value="<%=WebUtil.formatDate(objEMB.getDtStartDate(), IUMCConstants.DATE_FORMAT) %>" <%} %>
                             style="width: 100px;" >
                          </div>
                          </td>
                          <td style="width:220px"> 
                          <select   class="form-control show-tick" FieldType="mandatory" FieldName="Certificate Type" name="<%=ILabelConstants.txtCertificateType_+Index%>"  >
                         <option selected value="0">--- Select ---</option>
			 			<%
			 			 	
		                  	if(null != lstCertificateType){
		                  		
			                	for(int i=0;i<lstCertificateType.size();i++){
			                		CertificateTypeBean	objCTB2 = (CertificateTypeBean) lstCertificateType.get(i);
			 			 %> 
		                    		<option  value="<%=objCTB2.getLngID()%>"<% if(objCTB2.getLngID() == objETMB.getLngCertificateTypeID()) {%> selected <%} %> ><%= objCTB2.getStrName() %></option>
		                    
		                   <% 	
		                    	}
		                 	} %>
                          </select>
                         
                          </td> 
                          <!-- In future may be -->
                          <%-- <td align="center" >
                         
                        	<input type="checkbox" id="md_checkbox_29<%= +Index %>" class="filled-in chk-col-teal" name="<%=ILabelConstants.chkChild_+Index %>" value="<%=objETMB.getLngID() %>" onclick="CheckSelectSingle(this,chkMain,hdnRowCounter,hdnSelectedID)" />
                                <label for="md_checkbox_29<%= +Index %>"></label>
                          </td> --%>
                          <td>
                          <input type="checkbox" id="md_checkbox_29<%= +Index %>" DBvalue ="<%=objETMB.getLngIsTimed()%>"  class="filled-in chk-col-teal" name="<%=ILabelConstants.chkIsTimed_+Index %>" <%if(objETMB.getLngIsTimed()==1){%>checked<%} %> value="1" />
                                <label for="md_checkbox_29<%= +Index %>"></label>
                          </td>
                          <td >
                          <%if(objETMB.getLngID() > 0 && objETMB.getLngIsTimed() == 1) {%>
                          
                          <input type="button" class="btn bg-teal waves-effect waves-effect m-r-20" value="Add Splits"  onClick="addSplits(<%=objETMB.getLngID() %>)" data-toggle="modal" data-target="#largeModal"> 
                          
                          <%} %></td>
                          <td>
                          <%if(objETMB.getLngID() > 0) {%>
                          
                          <!-- <input type="button" class="btn bg-teal waves-effect waves-effect m-r-20" value="Download" >  -->
                          <%-- <button onClick="onExportToExcel(<%=objETMB.getLngID()%>)"  style=" background-color: white; border-color: white;"><div class="demo-google-material-icon" > <i class="material-icons" style="color:#009688">file_download</i></div></button> --%>
                          <a><i class="material-icons handsOn" style="color:#009688" onClick="onExportToExcel(<%=objETMB.getLngID()%>)" >file_download</i></a>
                          
                          <%} %></td>
                          
                          
                          <%if(lstEventTypeResult == null || lstEventTypeResult.size() == 0) {%>
                          <td class="glyphicon  glyphicon-remove clickable "
												height="20" onclick="deleteRow(this)" style="color: red;" align="center"></td>
                        
                          
                           <%} %>
                           
                           </tr>
               		 <%                         
	    				}
                      %> 
                      <tr><th>
                     
                      </th>
                      </tr>
                      </tbody>
                       <jsp:include page="../MyIncludes/incPageVariables.jsp" flush="true">
	                   <jsp:param name="USER_ACTION" value="<%=strUserAction%>" />
   		               <jsp:param name="ROW_COUNTER" value="<%=intRowCounter%>" />
   		               <jsp:param name="IS_AJAX_SUBMIT" value="true" />
   	                   </jsp:include>
                    </table>
                   <!--  </div> -->
                    </div>
                    </div>  <!-- table-responsive -->
            <%-- <input type="hidden" name="<%=ILabelConstants.selCompanyID %>" size="10" value="<%= lngCompanyId %>"> --%>        
    		 </aa:zone>                         
                       
                 <input type="hidden" name="<%=ILabelConstants.hdnEventTypeID %>" id="<%=ILabelConstants.hdnEventTypeID%>" size="10" value="<%=hdnlngEventTypeId %>">      
                       
                       
                      
                        <div id="ActionPanel" class="row no-margin-bottom default-margin "
							style="background-color: #006699;border:none; height: 50px; margin-right: 0px; margin-left: 0px; width: 100%">
							<div class="col-xs-10 col-md-10 col-sm-10 col-lg-10 floatleft" style="margin-top:4px">
									<input type="button" value="Add New"  class="btn btn-primary "
									  onClick="gotoThisPage('eventCreation.jsp')"/> 
								 <input type="button" value="Save"  class="btn btn-primary "
									 data-type="success" id="saveID" onClick="onSubmitVal(),topFunction()"/> 
									<!-- <input type="button" value="Delete" class="btn btn-danger "
									 onClick="onDelete()" /> -->
							</div>
							<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2 floatRight"
								style="float: right; margin-top:4px">
								<input type="button" value="Close"
									class="btn btn-danger rightDefaultMargin alignRight"
									style="float: right; background-color: blue;"
									onClick="onCancel('eventsList.jsp')" />
							</div>
						</div>
                        
                    </div>
                </div>
            </div>
          </div>
        </section> 





<div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <aa:zone name="PastData" skipIfNotIncluded="true"> 
						 
						 <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-nav-right" role="tablist" >
                                <li role="presentation" class="active" style="padding-left: 3px;"><a href="#home" data-toggle="tab" >Event Type Split Creation</a></li>
                                <!-- <li role="presentation" ><a href="#profile" data-toggle="tab">Certificate</a></li>
                                <li role="presentation" ><a href="#messages" data-toggle="tab">Photos</a></li> -->
                               
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="home">
                                    <div class="row">
                                    <p>
                                    
                                    <div class="col-lg-6" style="padding-left: 30px;">Event Name : <%=WebUtil.processString(objEMB.getStrName()) %></div>  
                                    <div class="col-lg-6" style="padding-left: 30px;">Competition Name : <%=WebUtil.processString(objETMB2.getStrName()) %> </div> 
                                    </p>
                                    </div>
                                     <div class="body table-responsive"> 
                                     <div class="row">
                           <aa:zone name="SplitList" skipIfNotIncluded="true"> 
                            <table class="table table-hover " style="margin-left:30px; width:96%" >
                                <thead>
                                    <tr>
                                       
                                      <th>Split Order</th>
                                      <th>Split Name</th>
                                        <th>Distance(km)</th>
                                        <th><input type="button" class="btn waves-effect waves-effect m-r-20" style="background-color:#1f91f3" value="Add Split" onclick="onAddSplitRow()" ></th>
                                    	
                                    </tr>
                                </thead>
                                <tbody id="tbodyAddIdSplit">
                                <%if(lstEventLap2 != null){ 
                                for(int i = 1; i<=lstEventLap2.size(); i++){
                                	objETLMB2 = (EventTypeLapMasterBean) lstEventLap2.get(i-1); 
                                
                                %>
                                
                                    <tr>
                                        <td>
                                         <input
												type="hidden" name="<%=ILabelConstants.lngSplitID_ + i%>"
												value="<%=objETLMB2.getLngID()%>" size="2">
                                       <%--  <input type="hidden" 
												name="<%=ILabelConstants.hdnSplitRowItemID_%>"
												ID="<%=ILabelConstants.hdnSplitRowItemID_%>" value="0"> --%>
                                        
                                        
                                        
                                        <input type="text" name="<%=ILabelConstants.SPLIT_ORDER_ +i%>" value="<%=objETLMB2.getLngLapOrder()%>" onkeypress="return isNumber(event)" size="14"></th>
                                        <td><input type="text" name="<%=ILabelConstants.SPLIT_NAME_ +i%>" value="<%=WebUtil.processString(objETLMB2.getStrName())%>" size="14"></td>
                                        <td>
                                        <input type="hidden"
												name="<%=ILabelConstants.hdnInvItemSplitID_%>"
												ID="<%=ILabelConstants.hdnInvItemSplitID%>"
												value="<%=objETLMB2.getLngID()%>" size="2">
												
												<input
									type="hidden" class="SelItemSplitID"
									name="<%=ILabelConstants.hdnSelectedSplitItemID%>"
									value="<%=strSplitSelectedItemID%>" size="20">
												
                                        <input type="text" name="<%=ILabelConstants.SPLIT_LABEL_NAME_ +i%>" value="<%=WebUtil.processString(objETLMB2.getStrdistance())%>" onkeypress="return isNumber(event)" size="14"></td>
                                        <td width="7%" class="glyphicon  glyphicon-remove clickable"
												height="20" onclick="deleteSplitRow(this)" align="center"></td>
                                    </tr>
                                    <%} }%>
                                    <tr id="ItemRowSplit" class="hide">
                                    <td><input type="text" name="<%=ILabelConstants.SPLIT_ORDER_%>" id="<%=ILabelConstants.SPLIT_ORDER%>" class="<%=ILabelConstants.SPLIT_ORDER%>" value="" onkeypress="return isNumber(event)" size="14"></td>
                                    <td><input type="text" name="<%=ILabelConstants.SPLIT_NAME_%>" id="<%=ILabelConstants.SPLIT_NAME%>" class="<%=ILabelConstants.SPLIT_NAME%>" value="" size="14"></td>
                                    <td><input type="text" name="<%=ILabelConstants.SPLIT_LABEL_NAME_%>" id="<%=ILabelConstants.SPLIT_LABEL_NAME%>" class="<%=ILabelConstants.SPLIT_LABEL_NAME%>" value="" onkeypress="return isNumber(event)" size="14"></td>
                                     <td width="7%" class="glyphicon  glyphicon-remove clickable"
												height="20" onclick="deleteSplitRow(this)" align="center"></td>
                                    </tr>
                                    
                                </tbody> 
                                
                                
                            </table>
                            </aa:zone>
                            <input
									type="hidden" class="SelItemID"
									name="<%=ILabelConstants.hdnSelectedItemID%>"
									value="<%=strSelectedItemID%>" size="20">
                           <input type="hidden" name="<%=ILabelConstants.hdnRowCounterSplit %>" size="2" class="flattextbox" value="<%=intRowCounterSplit%>" id="<%=ILabelConstants.hdnRowCounterSplit%>">
                           <input type="hidden"
									name="<%=ILabelConstants.hdnItemRowCounterSplit%>" size="2"
									value="<%=intRowCounterSplit%>"
									id="<%=ILabelConstants.hdnItemRowCounterSplit%>"> <input
									type="hidden" class="SelItemIDSplit"
									name="<%=ILabelConstants.hdnSelectedItemIDSplit%>"
									value="<%=strSelectedItemID%>" size="20">
                    
                    			</div>
                        		</div> 
                                </div>
                                <!-- <div role="tabpanel" class="tab-pane fade" id="profile">
                                    <div class="row">
                                
                                
                            </div>
                                </div> -->
                                
                                
                                
                                
                                 
                                
                                
                                
                            </div>
                        </div>
                        </aa:zone>
                     
                        <div class="modal-footer">
                            <!-- <button type="button" class="btn btn-link waves-effect">SAVE CHANGES</button> -->
                            <input type="button" class="btn bg-teal waves-effect" data-dismiss="modal" value="SAVE" onClick="SaveSplit()">
                            <input type="button" class="btn bg-teal waves-effect" data-dismiss="modal" value="CLOSE">
                            
                            </div>
                    </div>
                </div>
            </div>







<div class="modal fade" id="largeModalForAge" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <aa:zone name="AgeCategoryData" skipIfNotIncluded="true"> 
						 
						 <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-nav-right" role="tablist" >
                                <li role="presentation" class="active" style="padding-left: 3px;"><a href="#home" data-toggle="tab" >Event Type Age Category Creation</a></li>
                                <!-- <li role="presentation" ><a href="#profile" data-toggle="tab">Certificate</a></li>
                                <li role="presentation" ><a href="#messages" data-toggle="tab">Photos</a></li> -->
                               
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="home">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-left:0px;">
                                    <p>
                                    
                                    <div class="col-lg-4">Event Name : <%=WebUtil.processString(objEMB.getStrName()) %></div>  
                                    
                                    </p>
                                    </div>
                                    <!-- <div class="body table-responsive"> -->
                           <aa:zone name="AgeCategoryList" skipIfNotIncluded="true"> 
                           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px;padding-right: 0px;">
                           <div class="body table-responsive" style="margin-bottom: 0px;">
                           
                            <table class="table table-hover"  >
                                <thead>
                                    <tr>
                                       
                                      <th>Name</th>
                                      <th>Gender</th>
                                        <th>Min. Age</th>
                                        <th>Max. Age</th>
                                       <!--  <th>Event Type</th> -->
                                        <th><input type="button" class="btn bg-teal waves-effect" style="background-color:#1f91f3" value="New" onClick="onAddAgeCategoryRow()" ></th>
                                    
                                    
                                    </tr>
                                </thead>
                                <tbody id="tbodyAddIdAge">
                                <%if(lstAgeCategory != null){ 
                                for(int i = 1; i<=lstAgeCategory.size(); i++){
                                	objACCB = (AgeCategoryConfigurationBean) lstAgeCategory.get(i-1); 
                                	
                                	
                                	%>
                                
                                    <tr>
                                        <td>
                                         <input
												type="hidden" name="<%=ILabelConstants.lngAgeID_ + i%>"
												value="<%=objACCB.getLngID()%>" size="2">
                                        <input type="hidden" 
												name="<%=ILabelConstants.hdnAgeRowItemID_%>"
												ID="<%=ILabelConstants.hdnAgeRowItemID_%>" value="0">
                                        
                                        
                                        
                                        <input type="text" name="<%=ILabelConstants.textAgeCategoryName_ +i%>" value="<%=objACCB.getStrName()%>"></th>
                                        <td>
                                        
                                        <select   class="form-control show-tick" FieldType="mandatory" FieldName="Select Gender" name="<%=ILabelConstants.txtSelectgender_ +i%>" >
                        				 <option selected value="0">--- Select ---</option>
			 							<%
			 			 	
		                  					if(null != lstGender){
		                  		
			                	for(int j=0;j<lstGender.size();j++){
			                			objGender = (MyBosGender) lstGender.get(j);
			 			 %> 
		                    		<option  value="<%=objGender.getLngID()%>"<% if(objGender.getLngID() == objACCB.getLngGenderID()) {%> selected <%} %> ><%= objGender.getStrName() %></option>
		                    
		                   <% 	
		                    	}
		                 	} %>
                          </select>
                                        
                                      </td>
                                      <td>  
                                        <input type="text" name="<%=ILabelConstants.txtMinAge_ +i%>" value="<%=objACCB.getLngMinAge()%>" style="width: 82px;"></td>
                                        
                                        <td>  
                                        <input type="text" name="<%=ILabelConstants.txtMaxAge_ +i%>" value="<%=objACCB.getLngMaxAge()%>" style="width: 82px;"></td>
                                         <%-- <td>  
                                         
                                        <select   class="form-control show-tick" FieldType="mandatory" FieldName="Select Competition" name="<%=ILabelConstants.txtEventType_ +i%>" multiple>
                        				 
			 							<%
		                  					if(null != lstEventType){
		                  		
			                	for(int j=0;j<lstEventType.size();j++){
			                		objETMB3 = (EventTypeMasterBean) lstEventType.get(j);
			 			 %> 
		                    		<option  value="<%=objETMB3.getLngID()%>"<% if(objETMB3.getLngID() == objACCB.getLngEventTypeID()) {%> selected <%} %> ><%= objETMB3.getStrName() %></option>
		                    
		                   <% 	
		                    	}
		                 	} %>
                          </select>
                                        
                                    <input type="hidden"
												name="<%=ILabelConstants.hdnInvItemAgeID_%>"
												ID="<%=ILabelConstants.hdnInvItemAgeID%>"
												value="<%=objACCB.getLngID()%>" size="2">
												
												<input
									type="hidden" class="SelItemSplitID"
									name="<%=ILabelConstants.hdnSelectedSplitItemID%>"
									value="<%=strSplitSelectedItemID%>" size="20">    
                                    			
                                        </td> --%>
                                        <td width="7%" class="glyphicon  glyphicon-remove clickable"
												height="20" onclick="deleteAgeRow(this)" align="center"></td>
                                    </tr>
                                    <%} }%>
                                    <tr id="ItemRowAge" class="hide">
                                    <td><input type="text" name="<%=ILabelConstants.textAgeCategoryName_%>" id="<%=ILabelConstants.textAgeCategoryName%>" class="<%=ILabelConstants.textAgeCategoryName%>" value=""></td>
                                    <td>
                                   <% if(null != lstGender){%> 
                                    <select   class="<%=ILabelConstants.txtSelectgender%>"  name="<%=ILabelConstants.txtSelectgender_%>" id="<%=ILabelConstants.txtSelectgender%>">
                        				  <option selected value="0">--- Select ---</option> 
			 							<%
			 			 	
		                  					
		                  		
			                	for(int j=0;j<lstGender.size();j++){
			                			objGender = (MyBosGender) lstGender.get(j);
			 			 %> 
		                    		<option  value="<%=objGender.getLngID()%>"><%= objGender.getStrName() %></option>
		                    
		                   <% 	
		                 	} %>
                          </select>
                           <% 	
		                    	}
		                 	 %>
                                    </td>
                                    
                                    
                                    <td><input type="text" name="<%=ILabelConstants.txtMinAge_%>" id="<%=ILabelConstants.txtMinAge%>" class="<%=ILabelConstants.txtMinAge%>" value="" style="width: 82px;"></td>
                                    
                                    
                                     <td><input type="text" name="<%=ILabelConstants.txtMaxAge_%>" id="<%=ILabelConstants.txtMaxAge%>" class="<%=ILabelConstants.txtMaxAge%>" value="" style="width: 82px;"></td>
                                     <%-- <td>
                                       <select   class="form-control show-tick txtEventType"  name="<%=ILabelConstants.txtEventType_%>"  id="<%=ILabelConstants.txtEventType%>" multiple>
                        				
			 							<%
		                  					if(null != lstEventType){
		                  		
			                	for(int j=0;j<lstEventType.size();j++){
			                		objETMB3 = (EventTypeMasterBean) lstEventType.get(j);
			 			 %> 
		                    		<option  value="<%=objETMB3.getLngID()%>" ><%= objETMB3.getStrName() %></option>
		                    
		                   <% 	
		                    	}
		                 	} %>
                          </select>  
                          
                          
                            </td> --%>
                                     
                                     
                                     <td width="7%" class="glyphicon  glyphicon-remove clickable"
												height="20" onclick="deleteAgeRow(this)" align="center"></td>
                                    </tr> 
                                    
                                </tbody>  
                                
                                     
                                
                            </table>
                            </div>
                            </div>
                            </aa:zone>
                             <input
									type="hidden" class="SelItemID"
									name="<%=ILabelConstants.hdnSelectedItemID%>"
									value="<%=strSelectedItemID%>" size="20">
                           <input type="hidden" name="<%=ILabelConstants.hdnRowCounterAge %>" size="2" class="flattextbox" value="<%=intRowCounterAge%>" id="<%=ILabelConstants.hdnRowCounterAge%>">
                           <input type="hidden"
									name="<%=ILabelConstants.hdnItemRowCounterAge%>" size="2"
									value="<%=intRowCounterAge%>"
									id="<%=ILabelConstants.hdnItemRowCounterAge%>"> <input
									type="hidden" class="SelItemIDAge"
									name="<%=ILabelConstants.hdnSelectedItemIDAge%>"
									value="<%=strSelectedItemID%>" size="20">
                    
                     
                       <!--  </div> -->
                                </div>
                                <!-- <div role="tabpanel" class="tab-pane fade" id="profile">
                                    <div class="row">
                                
                                
                            </div>
                                </div> -->
                                
                                
                                
                                
                                 
                                
                                
                                
                            </div>
                        </div>
                        </aa:zone>
                     
                     
                     
                        <div class="modal-footer">
                        	<input type="button" class="btn bg-teal waves-effect" style="background-color:#1f91f3" value="New" onClick="onAddAgeCategoryRow()" >
                            <!-- <button type="button" class="btn btn-link waves-effect">SAVE CHANGES</button> -->
                            <input type="button" class="btn bg-teal waves-effect" data-dismiss="modal" value="SAVE" onClick="SaveSplit()">
                            <input type="button" class="btn bg-teal waves-effect" data-dismiss="modal" value="CLOSE">
                            
                            </div>
                    </div>
                </div>
           
           
           
            </div>








	<jsp:include page="../MyIncludes/incBottomPageReferences.jsp"
		flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>

	<!-- Bootstrap Core Js -->
	<script src="../plugins/bootstrap/js/bootstrap.js"></script>

	<!-- Select Plugin Js -->
	<script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

	<!-- Slimscroll Plugin Js -->
	 <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script> 

	<!-- Waves Effect Plugin Js -->
	<script src="../plugins/node-waves/waves.js"></script>

	<!-- Jquery CountTo Plugin Js -->
	 <script src="../plugins/jquery-countto/jquery.countTo.js"></script> 

		<!-- Autosize Plugin Js -->
    <script src="../plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="../plugins/momentjs/moment.js"></script>
    
	 <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="../plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
	 <script src="../js/pages/forms/basic-form-elements.js"></script>

	 <!-- Multi Select Plugin Js -->
    <script src="../plugins/multi-select/js/jquery.multi-select.js"></script>

	
	 <!-- dialogs Sweet Alert  -->
     <script src="../js/pages/ui/dialogs.js"></script>

	<!-- Sparkline Chart Plugin Js -->
	 <script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script> 

	

	<!-- Custom Js -->
	<script src="../js/admin.js"></script>

	<!-- Demo Js -->
	<script src="../js/demo.js"></script>
	
	<script>
	function afterAjaxReload(){
		
		$('#timeOut').delay(3000).hide(500);
		
		$('.datepicker').bootstrapMaterialDatePicker({
	    	format: 'DD-MM-YYYY',										// format: 'dddd DD MMMM YYYY',
	        clearButton: true,
	        weekStart: 1,
	        time: false
	    });
		
		
		
		$('select').selectpicker();   // select drop down
	 }
	
	function onExportToExcel(typeID){
		
		
		
		
			 strURL = "../Excel/SplitExcelTemplate.jsp?TYPEID="+typeID;
			objWin = document.open(strURL,"GEN_REP_EXCEL","left=50");
			objWin.focus(); 
		}
	
	
	
	 /* $(function() {
	        $('.multiselect-ui').multiselect({
	            includeSelectAllOption: true
	        });
	    }); */
	
	 $(document).ready(function() {

		   $(".table-drop .dropdown-toggle").click(function () {
		        $(this).parents(".table-responsive").toggleClass("res-drop");
		   });

		 });
	 
	 
	 
	 
	 
	 
	 
	</script>
	
	
  
	</form>
</body>

</html>
