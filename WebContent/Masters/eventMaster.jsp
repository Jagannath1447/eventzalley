<!DOCTYPE html>

<%@page import="com.uthkrushta.mybos.code.bean.CertificateTypeBean"%>
<%@page import="com.uthkrushta.mybos.configuration.bean.GlobalUrlSettingBean"%>
<%@page import="com.uthkrushta.mybos.transaction.bean.EventLapTimeResultBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeLapMasterBean"%>
<%@page import="com.uthkrushta.mybos.transaction.bean.view.MemberRecentEventView"%>
<%@page import="com.uthkrushta.mybos.transaction.bean.view.MemberUpcomingEventView"%>
<%@page import="com.uthkrushta.mybos.code.bean.TimeCodeBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeTimeMasterBean"%>
<%@page import="java.util.Date"%>
<%@page import="com.uthkrushta.mybos.configuration.bean.EventPhotoUploadBean"%>
<%@page import="com.uthkrushta.mybos.configuration.controller.EventPhotoUploadController"%>
<%@page import="com.uthkrushta.basis.constants.ISessionAttributes"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeMasterBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventMasterBean"%>
<%@page import="java.awt.image.DataBuffer"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.uthkrushta.basis.bean.MessageBean"%>
<%@page import="com.uthkrushta.basis.controller.GenericController"%>

<%@page import="org.ajaxanywhere.AAUtils"%>
<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<html>
<%
	String strContextPath = "../";
	strContextPath = WebUtil.parseString(request, IUMCConstants.CONTEXT_PATH);
%>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>Eventzalley</title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- Facebook icon -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- A jQuery plugin that adds cross-browser mouse wheel support. (Optional) -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" type="text/css">
<!-- Bootstrap Select Css -->
<link href="../plugins/bootstrap-select/css/bootstrap-select.css"
	rel="stylesheet" />
  <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />
 <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

<link href="../dist/css/lightgallery.css" rel="stylesheet">

    <!-- Light Gallery Plugin Css -->
    <!-- <link href="../plugins/light-gallery/css/lightgallery.css" rel="stylesheet"> -->
		 <link href="../css/themes/all-themes.css" rel="stylesheet" />
<link href="../Style/default.css"
	rel="stylesheet" />

<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->

<!-- <link href="../css/themes/all-themes.css" rel="stylesheet" />  -->
<style type="text/css">
            body{
                background-color: #152836
            }
            .demo-gallery > ul {
              margin-bottom: 0;
            }
            .demo-gallery > ul > li {
                float: left;
                margin-bottom: 15px;
                margin-right: 20px;
                width: 200px;
            }
            .demo-gallery > ul > li a {
              border: 3px solid #FFF;
              border-radius: 3px;
              display: block;
              overflow: hidden;
              position: relative;
              float: left;
            }
            .demo-gallery > ul > li a > img {
              -webkit-transition: -webkit-transform 0.15s ease 0s;
              -moz-transition: -moz-transform 0.15s ease 0s;
              -o-transition: -o-transform 0.15s ease 0s;
              transition: transform 0.15s ease 0s;
              -webkit-transform: scale3d(1, 1, 1);
              transform: scale3d(1, 1, 1);
              height: 100%;
              width: 100%;
            }
            .demo-gallery > ul > li a:hover > img {
              -webkit-transform: scale3d(1.1, 1.1, 1.1);
              transform: scale3d(1.1, 1.1, 1.1);
            }
            .demo-gallery > ul > li a:hover .demo-gallery-poster > img {
              opacity: 1;
            }
            .demo-gallery > ul > li a .demo-gallery-poster {
              background-color: rgba(0, 0, 0, 0.1);
              bottom: 0;
              left: 0;
              position: absolute;
              right: 0;
              top: 0;
              -webkit-transition: background-color 0.15s ease 0s;
              -o-transition: background-color 0.15s ease 0s;
              transition: background-color 0.15s ease 0s;
            }
            .demo-gallery > ul > li a .demo-gallery-poster > img {
              left: 50%;
              margin-left: -10px;
              margin-top: -10px;
              opacity: 0;
              position: absolute;
              top: 50%;
              -webkit-transition: opacity 0.3s ease 0s;
              -o-transition: opacity 0.3s ease 0s;
              transition: opacity 0.3s ease 0s;
            }
            .demo-gallery > ul > li a:hover .demo-gallery-poster {
              background-color: rgba(0, 0, 0, 0.5);
            }
            .demo-gallery .justified-gallery > a > img {
              -webkit-transition: -webkit-transform 0.15s ease 0s;
              -moz-transition: -moz-transform 0.15s ease 0s;
              -o-transition: -o-transform 0.15s ease 0s;
              transition: transform 0.15s ease 0s;
              -webkit-transform: scale3d(1, 1, 1);
              transform: scale3d(1, 1, 1);
              height: 100%;
              width: 100%;
            }
            .demo-gallery .justified-gallery > a:hover > img {
              -webkit-transform: scale3d(1.1, 1.1, 1.1);
              transform: scale3d(1.1, 1.1, 1.1);
            }
            .demo-gallery .justified-gallery > a:hover .demo-gallery-poster > img {
              opacity: 1;
            }
            .demo-gallery .justified-gallery > a .demo-gallery-poster {
              background-color: rgba(0, 0, 0, 0.1);
              bottom: 0;
              left: 0;
              position: absolute;
              right: 0;
              top: 0;
              -webkit-transition: background-color 0.15s ease 0s;
              -o-transition: background-color 0.15s ease 0s;
              transition: background-color 0.15s ease 0s;
            }
            .demo-gallery .justified-gallery > a .demo-gallery-poster > img {
              left: 50%;
              margin-left: -10px;
              margin-top: -10px;
              opacity: 0;
              position: absolute;
              top: 50%;
              -webkit-transition: opacity 0.3s ease 0s;
              -o-transition: opacity 0.3s ease 0s;
              transition: opacity 0.3s ease 0s;
            }
            .demo-gallery .justified-gallery > a:hover .demo-gallery-poster {
              background-color: rgba(0, 0, 0, 0.5);
            }
            .demo-gallery .video .demo-gallery-poster img {
              height: 48px;
              margin-left: -24px;
              margin-top: -24px;
              opacity: 0.8;
              width: 48px;
            }
            .demo-gallery.dark > ul > li a {
              border: 3px solid #04070a;
            }
            .home .demo-gallery {
              padding-bottom: 80px;
            }
        </style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

</head>

<%
	int Index = 1;
	String strUserAction = "GET";
	String strStatusText = "";
	String strStatusStyle = "";
	
	/* int intRowCounter = 1; */
	long pastID = 0;
	
	String strImageContextPath = "/EventzAlley/servlet1/";
	//To exclude the time from date
	
	
	//Ends
	GlobalUrlSettingBean objGISB = (GlobalUrlSettingBean) DBUtil.getRecord(IUMCConstants.BN_GLOBAL_IMAGE, IUMCConstants.GET_ACTIVE_ROWS, "");
if(objGISB == null){
objGISB = new GlobalUrlSettingBean();
}
	long userID = ((Long) session.getAttribute("LOGIN_ID")).longValue();
	List lstUpComingEventType = (List) DBUtil.getRecords(ILabelConstants.BN_MEMBER_UPCOMING_EVENT , "lngUserID = "+userID, "dtStartDate"); 
	List lstPastEvents = (List) DBUtil.getRecords(ILabelConstants.TR_MEMBER_RECENT_VIEW_BN , "lngUserID = "+userID, "dtEventStartDate DESC");
	MemberRecentEventView objMREV2 = new MemberRecentEventView();
	MemberUpcomingEventView objMUEV = new MemberUpcomingEventView();
	TimeCodeBean objTCB = new TimeCodeBean();
	EventPhotoUploadBean objEPUB2 = new EventPhotoUploadBean();
	EventTypeLapMasterBean objETLMB = new EventTypeLapMasterBean();
	EventLapTimeResultBean objELTRB = new EventLapTimeResultBean();
	
	CertificateTypeBean objCTB = new CertificateTypeBean();
	
	MemberRecentEventView objMREV = new MemberRecentEventView();
	objMREV = (MemberRecentEventView) DBUtil.getRecord( ILabelConstants.TR_MEMBER_RECENT_VIEW_BN, " lngUserID = "+userID, " dtEventStartDate DESC");
	if(objMREV == null){
		objMREV = new MemberRecentEventView();
	}
	
	long lngCertificateType = 1;
	String strCertificationRpt = "";		/* String strCertificationRpt = "Rpt/eventCompletionCertificate.rpt"; */
	List lstLastEventPhotos = (List) DBUtil.getRecords(ILabelConstants.BN_EVENT_PHOTO_UPLOAD , IUMCConstants.GET_ACTIVE_ROWS+" AND intPublish = 1 AND lngEventID = "+objMREV.getLngEventID()+" AND FIND_IN_SET( '" + objMREV.getStrBIBNO() + "', strBibNoEntry) > 0",
			"");
	
	EventMasterBean objEMB = new EventMasterBean();
	EventTypeMasterBean objETMB = new EventTypeMasterBean(); 
	List lstEventTypeLap = null;
	
	
	
	
	if (AAUtils.isAjaxRequest(request)) {
		strUserAction = WebUtil.processString(request.getParameter(ILabelConstants.hdnUserAction));
		
		if(strUserAction.equalsIgnoreCase(ILabelConstants.DETAILS))
		{
			pastID= WebUtil.parseLong(request, ILabelConstants.hdnDetailID);	
			System.out.println(" pastID = "+pastID);
			objMREV = (MemberRecentEventView) DBUtil.getRecord(ILabelConstants.TR_MEMBER_RECENT_VIEW_BN, "lngID = "+pastID, "");
			if(objMREV == null){
				
				objMREV = new MemberRecentEventView();
			}else
			 {
			 	lstEventTypeLap = (List) DBUtil.getRecords(ILabelConstants.BN_EVENT_TYPE_LAP, IUMCConstants.GET_ACTIVE_ROWS+" AND lngEventTypeID = "+objMREV.getLngEventTypeID(), "lngID");
			 
			 
			 	 
			 		
			 		lstEventTypeLap = (List) DBUtil.getRecords(ILabelConstants.BN_EVENT_TYPE_LAP, IUMCConstants.GET_ACTIVE_ROWS+" AND lngEventTypeID = "+objMREV.getLngEventTypeID(), "lngID");
			 		lngCertificateType = objMREV.getLngCertificateID();
			 		if (lngCertificateType > 0) {
						objCTB = (CertificateTypeBean) DBUtil.getRecord(ILabelConstants.BN_CERTIFICATE_TYPE, IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+lngCertificateType, ""); 
						if(objCTB == null){
							objCTB = new CertificateTypeBean();
						}else{
							strCertificationRpt = "Rpt/"+objCTB.getStrCertificatePath();
						}
						
					}  else {
						strCertificationRpt = "";
					} 
			 }

			AAUtils.addZonesToRefresh(request, "PastData");
		}
	}
		
	 
	
	
	
	
	
%>



<body class="theme-blue" style="background-color: #e9e9e9;">
  
	<!-- Page Loader -->

	<jsp:include page="../MyIncludes/incPageHeader.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
		 
	</jsp:include>
	<!-- #Top Bar -->

	<jsp:include page="../MyIncludes/incLeftPanel.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>

<form  method="post" >
	<section class="content">
	
	 <jsp:include page="../MyIncludes/incPageHeaderInfo.jsp" flush="true">
					 
					    <jsp:param name="STATUS_STYLE" value="<%=strStatusStyle%>" />
					    <jsp:param name="STATUS_TEXT" value="<%=strStatusText%>" />
					</jsp:include> 
			
		<div class="container-fluid">
		 		
		<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 0px;">
                    <div class="card">
                        <div class="header" >
							<h2 >Event Master</h2>
						</div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group full-body" id="accordion_19" role="tablist" aria-multiselectable="true">
                                        
                                        
                                        <div class="panel panel-col-myBlue">
                                            <div class="panel-heading" role="tab" id="headingTwo_19">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" href="#collapseTwo_19" aria-expanded="false" aria-controls="collapseTwo_19">
                                                        <i class="material-icons">folder_shared</i>Recent Events
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo_19" class="panel-collapse" role="tabpanel" aria-labelledby="headingTwo_19">
                                              <div class="body table-responsive">
					                            
					                            <table class="table table-hover" >
					                                <thead>
					                                    <tr>
					                                        <th>#</th>
					                                        <th>Date</th>
					                                        <th>Event Name</th>
					                                        <th>Competition Name</th>
					                                        <th>Place</th>
					                                        <th>Details</th>
					                                    </tr>
					                                </thead>
					                                <% 	if(null!=lstPastEvents){
					               			 				for(int index =0; index<lstPastEvents.size(); index++)
					               			 				{
					               			 				objMREV2 = (MemberRecentEventView) lstPastEvents.get(index);
					               			 				
					               			 			
					                                    
					               			 				%>
					                                <tbody>
					                                    <tr>
					                                        <td scope="row"><%=index+1 %>
					                                        <input type ="hidden" name="<%=ILabelConstants.Past_Event_Hidden_ID%>" value="<%=objMREV2.getLngID() %>" >
					                                        
					                                        </td>
					                                        <td><%=WebUtil.formatDate(objMREV2.getDtEventStartDate(), IUMCConstants.DATE_FORMAT) %></td>
					                                        <td><%=WebUtil.processString(objMREV2.getStrEventName())%></td>
					                                       <td><%=WebUtil.processString(objMREV2.getStrEventTypeName()) %></td>
					                                        <td><%=WebUtil.processString(objMREV2.getStrPlace()) %></td>
					                                        <td><input type="button" class="btn bg-teal waves-effect waves-effect m-r-20" value="Details" data-toggle="modal" onClick="details(<%=objMREV2.getLngID() %>)" data-target="#largeModal"> </td>
					                                    </tr>
					                                    
					                                </tbody>
					                                 <%}}else{%>
					                                
					                                <td>No Records</td>
					                               <% }%> 
					                            </table>
					                            <input type="hidden" name="<%=ILabelConstants.hdnDetailID%>"  id="<%=ILabelConstants.hdnDetailID%>" >
					                        </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-col-myBlue" >
                                            <div class="panel-heading" role="tab" id="headingOne_19">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" href="#collapseOne_19" aria-expanded="false" aria-controls="collapseOne_19">
                                                        <i class="material-icons">perm_contact_calendar</i> Upcoming Events
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_19" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_19">
                                              <div class="body table-responsive">
					                            <table class="table table-hover" >
					                                <thead>
					                                    <tr>
					                                        <th>#</th>
															<th>Date</th>
															<th>Time</th>
					                                        <th>Event Name</th>
					                                        <th>Competition Name</th> 
					                                        <th>Place</th>
					                                        
					                                    </tr>
					                                </thead>
					                                
					                                <% 	if(null!=lstUpComingEventType){
					               			 				for(int index =0; index<lstUpComingEventType.size(); index++)
					               			 				{
					               			 				objMUEV = (MemberUpcomingEventView) lstUpComingEventType.get(index);
					               			 			if(objMUEV == null){
					               			 			objMUEV = new MemberUpcomingEventView();
					               			 			}
					               			 		objTCB = (TimeCodeBean) DBUtil.getRecord(ILabelConstants.CD_TIME_BN, IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+objMUEV.getLngEventTimeID(), "");
					               			 				if(objTCB == null){
					               			 				objTCB = new TimeCodeBean();
					               			 				}
					               			 				%>
					                                <tbody>
					                                    <tr>
					                                        <td scope="row"><%=index+1 %></td>
					                                        <td><%=WebUtil.formatDate(objMUEV.getDtStartDate(), IUMCConstants.DATE_FORMAT)%></td>
					                                        <td><%=WebUtil.processString(objTCB.getStrName()) %></td> 
					                                        <td><%=WebUtil.processString(objMUEV.getStrEventName()) %></td> 
					                                       	<td><%=WebUtil.processString(objMUEV.getStrEventTypeName()) %></td>
					                                        <td><%=WebUtil.processString(objMUEV.getStrPlace())%></td>
					                                    </tr>
					                                </tbody>
					                                
					                                <%}}else{%>
					                                
					                                <td>No Upcoming Events.</td>
					                               <% }%> 
					                            </table>
					                        </div>
				                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
			
		</div>
		
			
			
	
	</section>
	
	<!-- </form> -->
	

	
	
	
	<div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <aa:zone name="PastData" skipIfNotIncluded="true"> 
						 
						 <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-nav-right" role="tablist" >
                            <%if(objMREV.getLngIsTimed() ==1 ) { %>
                                <li role="presentation" class="active" style="padding-left: 3px;"><a href="#home" data-toggle="tab" >Time data</a></li>
                              <% } %>
                                <li role="presentation" ><a href="#profile" data-toggle="tab">Certificate</a></li>
                                <li role="presentation" ><a href="#messages" data-toggle="tab">Photos</a></li>
                               
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                            <%if(objMREV.getLngIsTimed() ==1 ) { %>
                                <div role="tabpanel" class="tab-pane fade in active" id="home">
                                    
                                    <p>
                                    
                                    <div class="col-lg-4">Event Name :  <%=WebUtil.processString(objMREV.getStrEventName()) %></div>  
                                    <div class="col-lg-4">Event Type Name : <%=WebUtil.processString(objMREV.getStrEventTypeName()) %></div> 
                                    <div class="col-lg-4">Rank :<%if(objMREV.getLngRank() != 0){ %> #<%=objMREV.getLngRank() %> <%} %> </div>   
                                    <br>
                                    <div class="col-lg-4">CHIP : <%=WebUtil.processString(objMREV.getStrChip()) %></div>  
                                   	<div class="col-lg-4">GUN : <%=WebUtil.processString(objMREV.getStrGun()) %></div>
                                   	<div class="col-lg-4">PACE : <%=WebUtil.processString(objMREV.getStrPace()) %></div>
                                    </p>
                                    
                                    <!-- <div class="body table-responsive"> -->
                           
                            <table class="table table-hover" style="margin-left:15px; width:96%" >
                                <thead>
                                    <tr>
                                        <th>#</th>
                                      <th>Lap</th>
                                        <th>Time</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                
                                <%
                                if(lstEventTypeLap != null){
                                	for(int i = 0; i<lstEventTypeLap.size(); i++ ){
                                		objETLMB = (EventTypeLapMasterBean) lstEventTypeLap.get(i);
                                		if(objETLMB == null){
                                			objETLMB = new EventTypeLapMasterBean();
                                		}
                                		objELTRB = (EventLapTimeResultBean) DBUtil.getRecord(ILabelConstants.BN_EVENT_LAP_TIME_RESULT,
                                				" lngEventlapID = "+objETLMB.getLngID()+" AND lngEventresultID = "+objMREV.getLngID()
                                				, "");
                                		if(objELTRB == null){
                                			objELTRB = new EventLapTimeResultBean();
                                		}
                                	%>	
                                		
                                		
                              
                                
                                
                                    <tr>
                                        <th scope="row"><%=i+1%></th>
                                        <td><%=WebUtil.processString(objETLMB.getStrName())%></td>
                                        <td><%=WebUtil.processString(objELTRB.getStrTime())%></td>
                                        
                                        
                                    </tr>
                                    <%  		
                                	}
                                }
                                
                                %>
                                   
                                </tbody>
                            </table>
                           
                       <!--  </div> -->
                                </div>
                                <%} %>
                                <div role="tabpanel"  <%if(objMREV.getLngIsTimed() ==0 ) { %>class="tab-pane fade in active" <%} else{%>class="tab-pane fade" <%} %> id="profile">
                                    <div class="row">
                                <div class="col-xs-6 col-md-3">
                                    <a href="javascript:void(0);" class="thumbnail">
                                        <img src="../Images/pdfImage.png" class="img-responsive">
                                    </a>
                                    <input type="button" class="btn bg-teal waves-effect waves-effect m-r-20"
                                     <%if(strCertificationRpt == ""){ %>disabled<%} %>
                                     value="Click here to download certificate" onClick="openCertificate('<%=objMREV.getLngUserID()%>','<%=objMREV.getLngEventID()%>','<%=objMREV.getLngEventTypeID()%>','<%=strCertificationRpt%>')" >
                                </div>
                                
                            </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="messages">
                               
                            <div class="body" >
                            <div class="row clearfix">
							 <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                            <%-- <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                                <% 
						if(lstLastEventPhotos != null){
							for(int i = 0; i<lstLastEventPhotos.size(); i++ ){
								objEPUB2 = (EventPhotoUploadBean) lstLastEventPhotos.get(i);
									%>	
                                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12" id="lg-share">
                                    <a href="<%=strImageContextPath%><%=objMREV.getLngEventID()%>/<%=objEPUB2.getStrImagePath()%>" data-sub-html="<%=objEPUB2.getStrImageDescription()%>" data-facebook-share-url="http://app.eventzalley.in/servlet1/1/IMG_0954.jpg" download>
                                        <img class="img-responsive thumbnail" src="<%=strImageContextPath%><%=objMREV.getLngEventID()%>/<%=objEPUB2.getStrImagePath()%> " style="
 										   width: 200px; height: 150px;margin-bottom: 5px;">
 										  
                                    </a>
                                    
                                  
                                </div>
                             
                                <%	}
								}
								%>
                           
	
                            </div> --%>
                       <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                       <div>
                       <% 
						if(lstLastEventPhotos != null){ %>
                        <input type="button" class="btn bg-teal waves-effect" data-dismiss="modal" value="Download All" onClick="downloadAll()" style="margin-left: 35px;">
						<% }%></div>
                       <% 
						if(lstLastEventPhotos != null){
							for(int i = 0; i<lstLastEventPhotos.size(); i++ ){
								objEPUB2 = (EventPhotoUploadBean) lstLastEventPhotos.get(i);
									%>	
            <div id="lightgallery" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="margin-left: 15px;margin-right: 15px;margin-top: 15px;">
          
                <div   
                >
                    <a  href="<%=objGISB.getStrName()%>event-image/<%=objMREV.getLngEventID()%>/<%=objEPUB2.getStrImagePath()%>" 
                    data-facebook-share-url="<%=objGISB.getStrName()%>event-image/thumbnail/<%=objMREV.getLngEventID()%>/<%=objEPUB2.getStrImagePath()%>" 
                    download
                    data-sub-html="<h4><%=objEPUB2.getStrImageDescription()%></h4>">
                        <img class="img-responsive imgDownload" alt="<%=objGISB.getStrName()%>event-image/<%=objMREV.getLngEventID()%>/<%=objEPUB2.getStrImagePath()%>" src="<%=objGISB.getStrName()%>event-image/thumbnail/<%=objMREV.getLngEventID()%>/<%=objEPUB2.getStrImagePath()%>"
                         style="width: 200px; height: 150px;margin-bottom: 5px;">
                    </a>
                </div>
               
            </div>
            <%	}
								}
								%>
                       
                  </div>     
                        </div>
                           </div> 
                            
                            
                            </div>
                                </div>
                                
                                
                                
                                 
                                
                                
                                
                            </div>
                        </div>
                        </aa:zone>
                     
                        <div class="modal-footer">
                            <!-- <button type="button" class="btn btn-link waves-effect">SAVE CHANGES</button> -->
                            <input type="button" class="btn bg-teal waves-effect" data-dismiss="modal" value="CLOSE">
                           
                            </div>
                    </div>
                </div>
            </div>

<jsp:include page="../MyIncludes/incPageVariables.jsp" flush="true">
					     <jsp:param name="USER_ACTION" value="<%=strUserAction%>" />
					     <jsp:param name="IS_AJAX_SUBMIT" value="true" />
				 	</jsp:include> 

</form>


				

<%-- --%>






	<jsp:include page="../MyIncludes/incBottomPageReferences.jsp"
		flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>
	<!-- Jquery Core Js -->
	<!-- <script src="../plugins/jquery/jquery.min.js"></script> -->

	<!-- Bootstrap Core Js -->
	<script src="../plugins/bootstrap/js/bootstrap.js"></script>

	<!-- Select Plugin Js -->
	<script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

	<!-- Slimscroll Plugin Js -->
	 <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script> 

	<!-- Waves Effect Plugin Js -->
	<script src="../plugins/node-waves/waves.js"></script>

	<!-- Jquery CountTo Plugin Js -->
	 <script src="../plugins/jquery-countto/jquery.countTo.js"></script> 

	<!-- Morris Plugin Js -->
	<script src="../plugins/raphael/raphael.min.js"></script>
	<script src="../plugins/morrisjs/morris.js"></script>

	 <!-- Bootstrap Material Datetime Picker Plugin Js -->
	<!-- <script
		src="../plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>  -->

	<!--  Bootstrap Datepicker Plugin Js -->
	<!-- <script
		src="../plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>  --> 


	<!-- lightgallery plugins -->
    <!-- <script src="../plugins/light-gallery/js/lg-thumbnail.min.js"></script>
    <script src="../plugins/light-gallery/js/lg-fullscreen.min.js"></script> -->

	<script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
      <script src="../dist/js/lightgallery-all.min.js"></script>


	 <!-- Light Gallery Plugin Js -->
    <!-- <script src="../plugins/light-gallery/js/lightgallery-all.js"></script>-->
	<script src="../plugins/light-gallery/js/lg-share.js"></script>
	<script src="../plugins/light-gallery/js/lg-hash.js"></script> 

    <!-- Custom Js -->
   <!--  <script src="../js/pages/medias/image-gallery.js"></script> -->


	 <!-- dialogs Sweet Alert  -->
     <script src="../js/pages/ui/dialogs.js"></script>

	<!-- Sparkline Chart Plugin Js -->
	 <script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script> 

	<!-- Custom Js -->
	<script src="../js/admin.js"></script>
	<!-- <script src="../js/pages/index.js"></script> -->

	<!-- Demo Js -->
	 <!-- <script src="../js/demo.js"></script>  -->
	
	
	
	 <script>
 	
	 function afterAjaxReload(){
		 $('#aniimated-thumbnials').lightGallery({
		        thumbnail: true,
		         selector: 'a' 
		    });
		
		  
	 }
	 
	 
     
	 $(document).ready(function(){
         $('#lightgallery').lightGallery();
       
        
        
     });
   
	 
	 
	 function details(value){
		
		 $('.details').show();	
		
		 
		 $("#<%=ILabelConstants.hdnDetailID%>").val(value);
	
		<%--  document.forms[0].hdnUserAction.value = "<%=ILabelConstants.DETAILS%>"; --%>
		$("#<%=ILabelConstants.hdnUserAction%>").val("<%=ILabelConstants.DETAILS%>");
		 
		
  			
  			ajaxAnywhere.submitAJAX();
  	
		 
		 
	 }
	 
	 $(function(){
		 $('.details').hide();
		 
	 });
	 
	 
	 function openCertificate(UserID,EvntID,TypeID,strCertificatePath){
			openPageInNewWin('../reportViewer/rptCertificateViewer.jsp?ID='+UserID+"&evnt="+EvntID+"&comp="+TypeID+"&RP="+strCertificatePath,'CERT_RPT');
		} 
	 
	
	 
	 
	 
	 $("form").submit(function() {

		   $('#in-between').remove();

		});
	 
	 
	 
	 
	 
	function downloadAll(){ 
	 
		var images = document.getElementsByClassName("imgDownload");
		var srcList = [];
		var i = 0;

	setInterval(function(){
	    if(images.length > i){
	        srcList.push(images[i].alt);
	        var link = document.createElement("a");
	        link.id=i;
	        link.download = images[i].alt;
	        link.href = images[i].alt;
	        link.click();
	        i++;
	    }
	},1500);
	 
	}
	 
	 
	 
    </script>
	
  
	<!--  </form>  -->
</body>

</html>
