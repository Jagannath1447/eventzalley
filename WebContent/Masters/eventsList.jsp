<!DOCTYPE html>

<%@page import="com.uthkrushta.mybos.master.controller.EventListController"%>
<%@page import="com.uthkrushta.mybos.configuration.bean.EventPhotoUploadBean"%>
<%@page import="com.uthkrushta.mybos.configuration.controller.EventPhotoUploadController"%>
<%@page import="com.uthkrushta.basis.constants.ISessionAttributes"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeMasterBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventMasterBean"%>
<%@page import="java.awt.image.DataBuffer"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.uthkrushta.basis.bean.MessageBean"%>
<%@page import="com.uthkrushta.basis.controller.GenericController"%>

<%@page import="org.ajaxanywhere.AAUtils"%>
<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<html>
<%
	String strContextPath = "../";
	strContextPath = WebUtil.parseString(request, IUMCConstants.CONTEXT_PATH);
%>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>Eventzalley</title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" type="text/css">
<!-- Bootstrap Select Css -->
<link href="../plugins/bootstrap-select/css/bootstrap-select.css"
	rel="stylesheet" />
  <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Light Gallery Plugin Css -->
    <link href="../plugins/light-gallery/css/lightgallery.css" rel="stylesheet">

<link href="../Style/default.css"
	rel="stylesheet" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


<link href="../css/themes/all-themes.css" rel="stylesheet" /> 
<script type = "text/javascript">
    
  

    </script>

</head>

<%

int intCurrentPage = 1;
String strUserAction = "GET"; 
int intRowCounter = 1;
String strSelectedID = ";";
String strStatusText = "";
String strStatusStyle = "";


ArrayList<MessageBean> arrMsg = null; 
MessageBean objMsg;

EventListController objGC = new EventListController();
EventMasterBean objEMB = new EventMasterBean();
intRowCounter=WebUtil.parseInt(request.getParameter(ILabelConstants.hdnRowCounter));	
String strWhereClause = IUMCConstants.GET_ACTIVE_ROWS;
List lstEvents = DBUtil.getRecords(ILabelConstants.BN_EVENT_MASTER, strWhereClause, "dtStartDate DESC");
if(lstEvents != null){
	intRowCounter = lstEvents.size();
}else{
	intRowCounter =0;
}

if (AAUtils.isAjaxRequest(request)) {
	strUserAction = WebUtil.processString(request.getParameter(ILabelConstants.hdnUserAction));
	intRowCounter = WebUtil.parseInt(request.getParameter(ILabelConstants.hdnRowCounter));	
	objGC.doAction(request, response);
	
	lstEvents = (List) objGC.getList(request);
	if(lstEvents != null){
		intRowCounter = lstEvents.size();
	}else{
		intRowCounter=0;
	}
	
	
	
	
}

arrMsg = objGC.getArrMsg();		//Getting the messages to display
if(null!= arrMsg && arrMsg.size() > 0)
{
	objMsg = MessageBean.getLeadingMessage(arrMsg);
	if(objMsg != null)
	{
strStatusText = objMsg.getStrMsgText();
strStatusStyle = objMsg.getStrMsgType();
	}
}
AAUtils.addZonesToRefresh(request, "List,Status");
%>



<body class="theme-blue" style="background-color: #e9e9e9;">
<form method="post">
	<!-- Page Loader -->




	<jsp:include page="../MyIncludes/incPageHeader.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
		 
	</jsp:include>
	<!-- #Top Bar -->

<style type="text/css">
		.bar {
		    height: 18px;
		    background: green;
		}
	</style>

	<jsp:include page="../MyIncludes/incLeftPanel.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>

	




	<section class="content">
	
	 <jsp:include page="../MyIncludes/incPageHeaderInfo.jsp" flush="true">
					 
					    <jsp:param name="STATUS_STYLE" value="<%=strStatusStyle%>" />
					    <jsp:param name="STATUS_TEXT" value="<%=strStatusText%>" />
					</jsp:include> 
		<div class="container-fluid">
		
		<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header" >
							<h2 >Events List</h2>
						</div>
						<div id="ActionPanel" class="row no-margin-bottom default-margin "
							style="background-color: #006699;border:none; height: 50px; margin-right: 0px; margin-left: 0px; width: 100%">
							<div class="col-xs-8 col-md-8 col-sm-6 col-lg-6 floatleft" style="margin-top:4px">
								 <input type="button" value="Add New"  class="btn btn-primary "
									  onClick="gotoThisPage('eventCreation.jsp')"/> 
									 
							
							</div>
							<div class="col-xs-4 col-md-4 col-sm-6 col-lg-6 floatRight"
								style="float: right; margin-top:4px">
								<input type="button" value="Close"
									class="btn btn-danger rightDefaultMargin alignRight"
									style="float: right; background-color: blue;margin-left: 10px;"
									onClick="onCancel('../myHome.jsp')" />
								
								<input type="button" value="Delete"
									class="btn btn-danger rightDefaultMargin alignRight"
									style="float: right;  background-color: blue;" 
									onClick="onDelete()"/>
									
									
								
									
							</div>
						</div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group full-body" id="accordion_19" role="tablist" aria-multiselectable="true">
                                       
                                            
                                            <div id="collapseOne_19" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_19">
                                               
                                                   <aa:zone name="List" skipIfNotIncluded="true" > 
                                                    <div class="body table-responsive">
                            <table class="table table-hover" >
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Place</th> 
                                        <th>Start Date</th>
                                        <th>End Eate</th>
                                        
                                       	<th style="padding-bottom: 0px;">
                         	 <input type="checkbox" id="md_checkbox_29" class="filled-in chk-col-teal" name="<%=ILabelConstants.chkMain%>" value="1" onclick="SelectAll('chkChild_',this,hdnRowCounter,hdnSelectedID)" />
                                <label for="md_checkbox_29" style=" margin-bottom: 0px;"></label>
                          </th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                <% 	if(null!=lstEvents){
               			 				for(int index =1; index<=lstEvents.size(); index++)
               			 				{
               			 				objEMB = (EventMasterBean) lstEvents.get(index-1);%>
                                    <tr>
                                        <td scope="row"><%=index%>
                                        
                                        <input type="hidden"
									name="<%=ILabelConstants.hdnItemRowCounter%>" size="2"
									value="<%=intRowCounter%>"
									id="<%=ILabelConstants.hdnItemRowCounter%>">
                                        </td>
                                        <td class="clickable" onClick="gotoThisPage('eventCreation.jsp?<%=ILabelConstants.PAGE_TYPE %>=Edit&<%=ILabelConstants.ID %>=<%=objEMB.getLngID() %>')"><%=WebUtil.processString(objEMB.getStrName()) %></td>
                                        <td><%=WebUtil.processString(objEMB.getStrPlace()) %></td>
                                        <td><%=WebUtil.formatDate(objEMB.getDtStartDate(), IUMCConstants.DATE_FORMAT) %></td>
                                        <td><%=WebUtil.formatDate(objEMB.getDtEndDate(), IUMCConstants.DATE_FORMAT)%></td> 
                                       
                                        <td style="padding-bottom: 1px;border-bottom-width: 1px;"><input type="checkbox" id="md_checkbox_29<%= +index %>" class="filled-in chk-col-teal" name="<%=ILabelConstants.chkChild_+index %>" value="<%=objEMB.getLngID() %>" onclick="CheckSelectSingle(this,chkMain,hdnRowCounter,hdnSelectedID)" />
                                		<label style=" margin-bottom: 0px;" for="md_checkbox_29<%= +index %>"></label></td> 
                                    </tr>
                                     	<% }}
                                     	else{%>
                                     	
                                     	<td style="border-bottom-width:0px; font-size:16px; align:center">No Events</td>
                                     	
                                     	
                                     	
                                     	<%}%>
                                    
                                </tbody>
                           <jsp:include page="../MyIncludes/incPageVariables.jsp" flush="true">
	                   <jsp:param name="USER_ACTION" value="<%=strUserAction%>" />
   		               <jsp:param name="ROW_COUNTER" value="<%=intRowCounter%>" />
   		               <jsp:param name="IS_AJAX_SUBMIT" value="true" />
   	                   </jsp:include>
                           
                            </table>
                           
                        </div>
                                    </aa:zone>            
                                            </div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="ActionPanel" class="row no-margin-bottom default-margin "
							style="background-color: #006699;border:none;height: 50px;margin-right: 0px;margin-left: 0px;width: 100%;margin-bottom: 25px;">
							<div class="col-xs-8 col-md-8 col-sm-6 col-lg-6 floatleft" style="margin-top:4px">
								 <input type="button" value="Add New"  class="btn btn-primary "
									  onClick="gotoThisPage('eventCreation.jsp')"/> 
									 
							
							</div>
							<div class="col-xs-4 col-md-4 col-sm-6 col-lg-6 floatRight"
								style="float: right; margin-top:4px">
								<input type="button" value="Close"
									class="btn btn-danger rightDefaultMargin alignRight"
									style="float: right; background-color: blue;margin-left: 10px;"
									onClick="onCancel('../myHome.jsp')" />
									
								<input type="button" value="Delete"
									class="btn btn-danger rightDefaultMargin alignRight"
									style="float: right;  background-color: blue;" 
									onClick="onDelete()" />
									
								
									
							</div>
						</div>
                    </div>
                </div>
            </div>
		
		
			
		</div>
			
			
	</section>
 


	<jsp:include page="../MyIncludes/incBottomPageReferences.jsp"
		flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>
	<!-- Jquery Core Js -->
	<!-- <script src="../plugins/jquery/jquery.min.js"></script> -->

	<!-- Bootstrap Core Js -->
	<script src="../plugins/bootstrap/js/bootstrap.js"></script>

	<!-- Select Plugin Js -->
	<script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

	<!-- Slimscroll Plugin Js -->
	 <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script> 

	<!-- Waves Effect Plugin Js -->
	<script src="../plugins/node-waves/waves.js"></script>

	<!-- Jquery CountTo Plugin Js -->
	 <script src="../plugins/jquery-countto/jquery.countTo.js"></script> 

	<!-- Morris Plugin Js -->
	<script src="../plugins/raphael/raphael.min.js"></script>
	<script src="../plugins/morrisjs/morris.js"></script>

	 <!-- Bootstrap Material Datetime Picker Plugin Js -->
	<!-- <script
		src="../plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>  -->

	<!--  Bootstrap Datepicker Plugin Js -->
	<!-- <script
		src="../plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>  --> 

	 <!-- Light Gallery Plugin Js -->
    <script src="../plugins/light-gallery/js/lightgallery-all.js"></script>

    <!-- Custom Js -->
    <script src="../js/pages/medias/image-gallery.js"></script>


	 <!-- dialogs Sweet Alert  -->
     <script src="../js/pages/ui/dialogs.js"></script>

	<!-- Sparkline Chart Plugin Js -->
	 <script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script> 

	<!-- Custom Js -->
	<script src="../js/admin.js"></script>
	<!-- <script src="../js/pages/index.js"></script> -->

	<!-- Demo Js -->
	<!-- <script src="../js/demo.js"></script> -->
	
	
	
	 <script>
 	
	
	 
    </script>
	
  
	</form>
</body>

</html>
