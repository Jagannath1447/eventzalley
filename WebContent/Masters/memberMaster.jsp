<!DOCTYPE html>
<%@page import="com.uthkrushta.mybos.master.bean.EventMasterBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.CountryBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.RoleBean"%>
<%@page import="com.uthkrushta.basis.constants.ISessionAttributes"%>
<%@page import="com.uthkrushta.mybos.master.controller.UserMasterController"%>
<%@page import="com.uthkrushta.mybos.code.bean.SecurityQuestionBean"%>
<%@page import="com.uthkrushta.mybos.code.bean.SecurityQuestionBean"%>
<%@page import="com.uthkrushta.mybos.code.bean.BloodGroupBean"%>
<%@page import="com.uthkrushta.mybos.code.bean.MyBosGender"%>
<%@page import="com.uthkrushta.mybos.master.bean.UserMasterBean"%>
<%@page import="java.awt.image.DataBuffer"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.uthkrushta.basis.bean.MessageBean"%>
<%@page import="com.uthkrushta.basis.controller.GenericController"%>


<%@page import="org.ajaxanywhere.AAUtils"%>
<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.mybos.code.bean.PaymentModeCodeBean"%>


<%@page import="com.uthkrushta.mybos.master.bean.AreaMasterBean"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<html>
<%
	String strContextPath = "../";
	strContextPath = WebUtil.parseString(request, IUMCConstants.CONTEXT_PATH);
%>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>Eventzalley</title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" type="text/css">
<!-- Bootstrap Select Css -->
<link href="../plugins/bootstrap-select/css/bootstrap-select.css"
	rel="stylesheet" />

<link href="../Style/default.css"
	rel="stylesheet" />
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->

<!-- Both for password eye -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
 

  <!-- Bootstrap Material Datetime Picker Css -->
    <link href="../plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" /> 


    <!-- Wait Me Css -->
    <link href="../plugins/waitme/waitMe.css" rel="stylesheet" />

 <link href= "https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<!-- Bootstrap Material Datetime Picker Css 	-->
  <!--   <link href="../plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" /> -->
 
 
 
 
   
<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="../css/themes/all-themes.css" rel="stylesheet" />
<style type="text/css">

 /* .wrapper {
  padding: 100px;
}

.image--cover2 {
  width: 150px;
  height: 180px;
  border-radius: 100%;
 

  object-fit: cover;
  object-position: center right;
}
    
    .center {
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: 100%;
    
}
 */

 
    


</style>

<script type="text/javascript">

function checkConfPass()
{
 var pass=document.forms[0].password.value;
 var rpass=document.forms[0].confirmPassword.value;

 if(pass.trim()=="")
	 {
	  alert('Please First Enter the password');
	  document.forms[0].confirmPassword.value="";
	  document.forms[0].password.focus(); 
	  return false;
	 }
 
 if(pass!=rpass)
	 {
	  alert("Your Password and Confirm password did not match");
	  return false;
	 }
 else{
	 return true;
 }
}
    
function onSave(){
	
	
	var gender = $("#<%=ILabelConstants.selGender%> option:selected").val();
	var primaryCntNo = $("#<%=ILabelConstants.primaryNumber%>").val();
	var emailID =$("#<%=ILabelConstants.emailID%>").val();
	var role = $("#<%=ILabelConstants.selRoleID%>").val();
	<%-- var password = $("#<%=ILabelConstants.password%>").val();
	var confirmPassword = $("#<%=ILabelConstants.confirmPassword%>").val(); --%>
	<%--  var zip =	$("#<%=ILabelConstants.ZipCode%> ").val();	 --%>
	var phoneno = /^[0]?[789]\d{9}$/;  
	var atposition=emailID.indexOf("@");  
	var dotposition=emailID.lastIndexOf("."); 
	var decimal= /\d{1,10}(\.\d{1,2})?/;  
		
	/* var zipno = /^\d{6}(-\d{5})?(?!-)$/ ; */
	
	
	 
	
	

	if(gender == 0)
	{
		alert("Please Select Gender");
		return;	
	}else if(role == 0){
		
		alert("Please Select Role");
		return;
	}
	
	
	
	else if (atposition<1 || dotposition<(atposition+2) || dotposition+2>=emailID.length){  
	 	 alert("Please enter a valid Primary e-mail address!!");  /* \n atpostion:"+atposition+"\n dotposition:"+dotposition */  
	 	document.forms["myForm"]["<%=ILabelConstants.emailID %>"].focus();
		  return ;
   }
	
	
	/* else if( !zip.match(zipno)){			//!zip.match(decimal)		!zip.match(zipno) &&
		
		alert("Please Enter a Valid Zip Code!!")
		return;
		} */
		
	
	else{
		
		document.forms[0].hdnUserAction.value = "SUBMIT"; 
        /* alert(document.forms[0].hdnUserAction.value);*/
        
     		
     	if(ValidateForm())
    	{
     		if(checkConfPass()){
     			
     		
     		 if(null!= document.forms[0].hdnAjaxSubmit && document.forms[0].hdnAjaxSubmit.value.toLowerCase()== 'true')	
     		{	
     			
     			ajaxAnywhere.submitAJAX();
     		}
     		else
     		{
     			document.forms[0].submit();
     		} 
     		 
     		}
    	}
		
	 } 
	
 		
}

function checkPass()
{
	var pass=document.forms[0].password.value;
	 var rpass=document.forms[0].confirmPassword.value;

	 if(!rpass.trim()=="")
		 {
            if(pass!=rpass)
                {
                   alert("Your Password and Confirm password did not match");
                   return false;
                }
		 }

	 
	 
}
/* function checkConfPass()
{
 var pass=document.forms[0].password.value;
 var rpass=document.forms[0].confirmPassword.value;

 if(pass.trim()=="")
	 {
	  alert('Please First Enter the password');
	  document.forms[0].confirmPassword.value="";
	  document.forms[0].password.focus(); 
	  return false;
	 }
 
 if(pass!=rpass)
	 {
	  alert("Your Password and Confirm password did not match");
	  return false;
	 }
} */
    
    




function afterAjaxReload(){
	$('#date').bootstrapMaterialDatePicker({ weekStart : 0, time: false });  
	
}

function photoChange(){
	
	window.location.reload();
	
	/* document.forms[0].hdnUserAction.value = "PhotoChange"; 
	ajaxAnywhere.submitAJAX(); */
	
}


    </script>

</head>

<%
	String strUserAction = "GET";
	String strStatusText = "";
	String strStatusStyle = "";
	String strContaxtImagePath = "/EventzAlley/servlet1"; 
	
	ArrayList<MessageBean> arrMsg = null; 
	MessageBean objMsg;
	
	
	UserMasterBean objUMB = new UserMasterBean();
	long userID = WebUtil.parseLong(request, ILabelConstants.ID);  //((Long) session.getAttribute(ISessionAttributes.LOGIN_ID)).longValue();
	System.out.println("userID =  "+userID);
	
	objUMB = (UserMasterBean) DBUtil.getRecord(ILabelConstants.BN_USER_MASTER_BEAN, IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+userID , "");
	
	if(objUMB == null){
		
		objUMB = new UserMasterBean();
	}
	
	
	UserMasterController objGC = new UserMasterController();
	
	
	List lstCountry = DBUtil.getRecords(ILabelConstants.BN_COUNTRY, IUMCConstants.GET_ACTIVE_ROWS, "strName");
	List lstGender = DBUtil.getRecords(ILabelConstants.CD_GENDER_BN, IUMCConstants.GET_ACTIVE_ROWS, "");
	List lstBloodGroup = DBUtil.getRecords(ILabelConstants.CD_BLOOD_GROUP_BN, IUMCConstants.GET_ACTIVE_ROWS, "");
	List lstSecQuestion = DBUtil.getRecords(ILabelConstants.CD_SECURITY_QUESTION_BN, IUMCConstants.GET_ACTIVE_ROWS, "");
	List lstRole = DBUtil.getRecords(ILabelConstants.BN_ROLE_MASTER, IUMCConstants.GET_ACTIVE_ROWS+" AND intRoleType ="+IUMCConstants.MEMBER_ROLE_TYPE  , "");
	
	
	
	
	
	if(AAUtils.isAjaxRequest(request)){
		strUserAction = WebUtil.processString(request.getParameter(ILabelConstants.hdnUserAction));
		
		objGC.doAction(request, response);		//Calling the controller to do actions
		/* if(strUserAction.equalsIgnoreCase("PhotoChange"))
		{
			AAUtils.addZonesToRefresh(request, "PhotoChange");
		} */
	}	
			//Getting the list to display  
		 
	
	
	
	 arrMsg = objGC.getArrMsg();		//Getting the messages to display
	if(null!= arrMsg && arrMsg.size() > 0)
	{
		objMsg = MessageBean.getLeadingMessage(arrMsg);
		if(objMsg != null)
		{
	strStatusText = objMsg.getStrMsgText();
	strStatusStyle = objMsg.getStrMsgType();
		}
	}
	
	
	AAUtils.addZonesToRefresh(request, "PhotoChange");
	 
%>



<body class="theme-blue" style="background-color: #e9e9e9;">
<form method="post" autocomplete="off">
	<!-- Page Loader -->




	<jsp:include page="../MyIncludes/incPageHeader.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
		 
	</jsp:include>
	<!-- #Top Bar -->



	<jsp:include page="../MyIncludes/incLeftPanel.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>

	




	<section class="content">
	
	
		
			
	 <jsp:include page="../MyIncludes/incPageHeaderInfo.jsp" flush="true">
					 
					    <jsp:param name="STATUS_STYLE" value="<%=strStatusStyle%>" />
					    <jsp:param name="STATUS_TEXT" value="<%=strStatusText%>" />
					</jsp:include> 
					
					
				
				
					
					
					
		<div class="container-fluid">
		
			<div class="row clearfix ">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="card" style="margin-bottom: 0px;">
						<div class="header" >
							<h2 style="color: #006699;">Member Master</h2>
						</div>
						<div id="ActionPanel" class="row no-margin-bottom default-margin "
							style="background-color: #006699;border:none; height: 50px; margin-right: 0px; margin-left: 0px; width: 100%">
							<div class="col-xs-10 col-md-10 col-sm-10 col-lg-10 floatleft" style="margin-top:4px">
								  <input type="button" value="Add New"  class="btn btn-primary "
									  onClick="gotoThisPage('memberMaster.jsp')"/> 
								 <input type="button" value="Save"  class="btn btn-primary "
									 data-type="success"  onClick="onSave()"/> 
									<!-- <input type="button" value="Delete" class="btn btn-danger "
									 onClick="onDelete()" /> -->
							</div>
							<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2 floatRight"
								style="float: right; margin-top:4px">
								<input type="button" value="Close"
									class="btn btn-danger rightDefaultMargin alignRight"
									style="float: right; background-color: blue;"
									onClick="onCancel('membersList.jsp')" />
							</div>
						</div>
						
						<div class="body" style="margin-bottom:20px; padding-bottom:0px">
						
							<div class="row clearfix">
							
							<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12" style="margin-bottom: 0px;">
                	
                  		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     	 <aa:zone name="PhotoChange" skipIfNotIncluded="true"> 
                      <% if(objUMB != null && null!= objUMB.getStrImagePath() && objUMB.getStrImagePath().trim().length() > 0 ){
                  			%>
						 <img src="<%=strContaxtImagePath%>/MemberProfile/<%=objUMB.getStrImagePath()%>" alt="" class="image--cover2 center" width="100%" height="100%" style=" margin-top: 40px;"/>
						<%}else{%>
                        	<img src="../Images/user.png" alt="" class="image--cover2 center" width="100%" height="100%" style=" margin-top: 40px;"/>
                     	<%} %> 
						
						
						
						</aa:zone>
						<!-- Image upload  -->
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 0px; padding-top: 5px;">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-left: 0px;">
						<input id="fileupload" type="file" name="files" data-url="/EventzAlley/MemberPhotoUploadAttachment" style=" width: 91px;" >
						<div id="progress" style="background-color:lightgrey"> <div class="bar" style="text-align:center;color:white;width:0%"></div>
							<!-- <button type="submit">Upload</button> -->
	               <!-- <button>Upload</button> -->
	               <div id="progress"> <div class="bar" style="width: 0%;"></div>
	               
					</div>
						
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<input type="button"  class="btn btn-primary " value="Refresh" onclick="photoChange()">
							</div>
							</div>
							  <!-- image upload ends  -->
							                    
</div>
             				</div>
							
							
							 <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12" style=" margin-bottom: 10px;">
							<!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 " style=" margin-bottom: 10px;">

									<div class="form-group form-float" style="margin-bottom: 0px;  margin-top: 15px;">

										<div class="form-line">
										
											<input type="text" class="form-control" name="username"
												value="" style="background-color: white" disabled > <label
												class="form-label" >ID*</label>
													 
										</div>
									</div>
								</div> -->
								
								
								
								 
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top: 15px;margin-bottom: 0px;">

										<div class="form-group form-float" style="margin-top: 15px;">

											<div class="form-line">
												<input type="text" class="form-control"
												FieldType = "mandatory" FieldName="First Name"
													name="<%=ILabelConstants.FirstName%>"
													id="<%=ILabelConstants.FirstName%>"
													value="<%=WebUtil.processString(objUMB.getStrFirstName())%>"
													style="background-color: white"> <label
													class="form-label" >First
													Name*</label>

											</div>
										</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 " style="margin-top: 15px;margin-bottom: 0px;">

										<div class="form-group form-float" style="margin-top: 15px;">

											<div class="form-line">
												<input type="text" class="form-control"
													name="<%=ILabelConstants.LastName%>"
													id="<%=ILabelConstants.LastName%>"
													value="<%=WebUtil.processString(objUMB.getStrLastName())%>"
													style="background-color: white"> <label
													class="form-label">Last
													Name</label>

											</div>
										</div>
								</div>	
								
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12"  style=" margin-bottom: 10px;">
                                    <div class="form-group" style=" margin-bottom: 0px;">
                                        <div class="form-line">
                                             <input type="text" name="<%=ILabelConstants.REG_DATE %>" class="datepicker form-control"
                                             value ="<%=WebUtil.formatDate(objUMB.getDtDate(), IUMCConstants.DATE_FORMAT) %>" 
                                            style="  margin-top: 15px;" placeholder="Registration Date"> 
                                            <input type="hidden" name="<%=ILabelConstants.hdnID %>" value="<%=objUMB.getLngID()%>" > 
                                           <input type="hidden" name="<%=ILabelConstants.MemberPhoto%>" value="<%=objUMB.getStrImagePath()%>">
                                            <input type="hidden" name="<%=ILabelConstants.hdnCreatedBy%>" value="<%=objUMB.getLngCreatedBy()%>">
                       				<%request.getSession().setAttribute(ISessionAttributes.MEMBER_PHOTO_ID, objUMB.getLngID());%> 
                       					 
                       					 <input  type="hidden" name="<%=ILabelConstants.hdnCreatedon%>" value="<%=objUMB.getDtCreatedOn()%>" >
                                             
                                        </div>
                                    </div>
                                </div> 
								
								
								
								
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style=" margin-top: 15px;margin-bottom: 0px;" >
										<div class="form-group form-float" >

											<div class="form-line">
										<select class="form-control show-tick"
											id="<%=ILabelConstants.selBloodGroup%>"
											name="<%=ILabelConstants.selBloodGroup%>">
											<option value="0">-- Select Blood Group--</option>
											<%
												if (null != lstBloodGroup) {
													for (int i = 0; i < lstBloodGroup.size(); i++) {
														BloodGroupBean objBGB = (BloodGroupBean) lstBloodGroup.get(i);
											%>

											<option value="<%=objBGB.getLngID()%>" <%if(objUMB.getLngBloodGroupID() == objBGB.getLngID()){ %> selected <% }%> ><%=objBGB.getStrName()%></option>

											<%
												}

												}
											%>


										</select>
										</div>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top: 15px;" >
										<div class="form-group form-float" >

											<div class="form-line">
										<select class="form-control show-tick"
											id="<%=ILabelConstants.selGender%>"
											name="<%=ILabelConstants.selGender%>">

											<option value="0">-- Select Gender*--</option>
											<%
												if (null != lstGender) {
													for (int i = 0; i < lstGender.size(); i++) {
														MyBosGender objGCB = (MyBosGender) lstGender.get(i);
											%>

											<option value="<%=objGCB.getLngID()%>" <%if(objUMB.getLngGenderID() == objGCB.getLngID()){ %> selected <% }%>><%=objGCB.getStrName()%></option>

											<%
												}

												}
											%>


										</select>
										</div>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-bottom: 15px; padding-top: 15px;">


                                    <div class="form-group form-float" style="margin-bottom: 0px;  ">
                                        <div class="form-line" id="bs_datepicker_container">
                                            <input type="text" class="datepicker form-control"
                                            FieldType = "mandatory" FieldName="Date of Birth"
                                             name="<%=ILabelConstants.DOB%>" placeholder="Date of Birth*"
                                            value="<%=WebUtil.formatDate(objUMB.getDtDOB(), IUMCConstants.DATE_FORMAT) %>" >
                                        </div>
                                    </div>
								</div>
								
									
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding-bottom: 0px; margin-top: 15px;" >
										<div class="form-group form-float" >

											<div class="form-line">
										<select class="form-control show-tick"
											id="<%=ILabelConstants.selRoleID%>"
											name="<%=ILabelConstants.selRoleID%>">
											<option value="0">-- Select Role* --</option>
											<%
												if (null != lstRole) {
													for (int i = 0; i < lstRole.size(); i++) {
														RoleBean objRB = (RoleBean) lstRole.get(i);
											%>

											<option value="<%=objRB.getLngID()%>" <%if(objUMB.getLngRoleID() == objRB.getLngID()){ %> selected <% }%> ><%=objRB.getStrName()%></option>

											<%
												}

												}
											%>


										</select>
										</div>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 " style="margin-bottom: 0px;">

										<div class="form-group form-float" style="margin-top: 15px;">

											<div class="form-line">
												<input type="text" class="form-control"
													name="<%=ILabelConstants.Occupation%>"
													id="<%=ILabelConstants.Occupation%>"
													value="<%=WebUtil.processString(objUMB.getStrOccupation())%>"
													style="background-color: white"> <label
													class="form-label" >Occupation</label>

											</div>
										</div>
								</div>
								
								</div>
								
								
									
									
									
									
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">

										<div class="form-group form-float" style="margin-top: 15px;">

											<div class="form-line">
												<input type="text" class="form-control"
													name="<%=ILabelConstants.primaryNumber%>"
													FieldType = "mandatory" FieldName="Primary Contact Number"
													id="<%=ILabelConstants.primaryNumber%>"
													value="<%=WebUtil.processString(objUMB.getStrPrimaryContactNumber())%>"
													style="background-color: white"
													onkeypress="return isNumber(event)"> <label
													class="form-label" >Primary Contact
													Number *</label>

											</div>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">

										<div class="form-group form-float" style="margin-top: 15px;">

											<div class="form-line">
												<input type="text" class="form-control"
													name="<%=ILabelConstants.secondaryNumber%>"
													id="<%=ILabelConstants.secondaryNumber%>"
													value="<%=WebUtil.processString(objUMB.getStrSecondaryContactNumber())%>"
													style="background-color: white"
													onkeypress="return isNumber(event)"> <label
													class="form-label" >Secondary Contact
													Number</label>

											</div>
										</div>
									</div>
									
									
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">

										<div class="form-group form-float" style="margin-top: 15px;">

											<div class="form-line">
												<input type="text" class="form-control"
												FieldType = "mandatory" FieldName="Primary e-mail"
													name="<%=ILabelConstants.emailID%>"
													id="<%=ILabelConstants.emailID%>"
													value="<%=WebUtil.processString(objUMB.getStrEmailID())%>"
													style="background-color: white"> 
													<label
													class="form-label" >Primary e-mail*</label>

											</div>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">

										<div class="form-group form-float" style="margin-top: 15px;">

											<div class="form-line">
												<input type="text" class="form-control"
													name="<%=ILabelConstants.SecondaryEmailID%>"
													id="<%=ILabelConstants.SecondaryEmailID%>"
													value="<%=WebUtil.processString(objUMB.getStrSecondaryEmailID())%>"
													style="background-color: white"> 
													<label
													class="form-label" >Secondary e-mail</label>

											</div>
										</div>
									</div>
									
									
								
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" >

										<div class="form-group form-float" style="margin-top: 15px;">

											<div class="form-line">
												<input type="text" class="form-control"
													name="<%=ILabelConstants.AssignedUserName%>"
													id="<%=ILabelConstants.AssignedUserName%>"
													value="<%=WebUtil.processString(objUMB.getStrAssignedUserName())%>"
													style="background-color: white"> <label
													class="form-label" >Assigned User Name</label>

											</div>
										</div>
								</div>
								
								<%-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">

										<div class="form-group form-float" style="margin-top: 15px;">

											<div class="form-line">
												<input type="text" class="form-control"
													name="<%=ILabelConstants.CompanyName%>"
													id="<%=ILabelConstants.CompanyName%>"
													value="<%=WebUtil.processString(objUMB.getStrCompanyName())%>"
													style="background-color: white"> <label
													class="form-label" >Company Name</label>

											</div>
										</div>
								</div> --%>
								
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">

										<div class="form-group form-float" style="margin-top: 15px;">

											<div class="form-line">
												<input type="text" class="form-control"
													name="<%=ILabelConstants.OtherOrganization%>"
													id="<%=ILabelConstants.OtherOrganization%>"
													value="<%=WebUtil.processString(objUMB.getStrPartOfAnyRunningClub())%>"
													style="background-color: white"> <label
													class="form-label" >Part of Any Organization?</label>

											</div>
										</div>
								</div>
								
								
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

										<div class="form-group form-float" style="margin-top: 15px;">

											<div class="form-line">
												<input type="text" class="form-control"
													name="<%=ILabelConstants.PersonalBestLink%>"
													id="<%=ILabelConstants.PersonalBestLink%>"
													value="<%=WebUtil.processString(objUMB.getStrPersonalBest())%>"
													style="background-color: white"> <label
													class="form-label" >Link (Personal Best Event)</label>

											</div>
										</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

										<div class="form-group form-float" style="margin-top: 15px;">

											<div class="form-line">
												<input type="text" class="form-control"
													name="<%=ILabelConstants.EmergencyCntNo%>"
													id="<%=ILabelConstants.EmergencyCntNo%>"
													value="<%=WebUtil.processString(objUMB.getStrEmergencyCntDetails())%>"
													style="background-color: white"> <label
													class="form-label" >Emergency Contact Number</label>

											</div>
										</div>
								</div>
								
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

										<div class="form-group form-float" style="margin-top: 15px;">

											<div class="form-line">
												<input type="text" class="form-control"
													name="<%=ILabelConstants.Street%>"
													id="<%=ILabelConstants.Street%>"
													value="<%=WebUtil.processString(objUMB.getStrStreet())%>"
													style="background-color: white"> <label
													class="form-label" >Street</label>

											</div>
										</div>
								</div>
								
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

										<div class="form-group form-float" style="margin-top: 15px;">

											<div class="form-line">
												<input type="text" class="form-control"
													name="<%=ILabelConstants.City%>"
													id="<%=ILabelConstants.City%>"
													value="<%=WebUtil.processString(objUMB.getStrCity())%>"
													style="background-color: white"> <label
													class="form-label" >City</label>

											</div>
										</div>
								</div>
								
								
								
								
								
								
								
								
								
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

										<div class="form-group form-float" style="margin-top: 15px;">

											<div class="form-line">
												<input type="text" class="form-control"
													name="<%=ILabelConstants.State%>"
													id="<%=ILabelConstants.State%>"
													value="<%=WebUtil.processString(objUMB.getStrState())%>"
													style="background-color: white"> <label
													class="form-label" >State</label>

											</div>
										</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top: 15px;margin-bottom: 25px;" >
										<div class="form-group form-float" >

											<div class="form-line">
										<select class="form-control show-tick"
											id="<%=ILabelConstants.Country%>"
											name="<%=ILabelConstants.Country%>">
											<option value="0">-- Select Country --</option>
											<%
												if (null != lstCountry) {
													for (int i = 0; i < lstCountry.size(); i++) {
														CountryBean objCB = (CountryBean) lstCountry.get(i);
											%>

											<option value="<%=objCB.getLngID()%>" <%if(objUMB.getLngCountryID() == objCB.getLngID()){ %> selected <% }%> ><%=objCB.getStrName()%></option>

											<%
												}

												}
											%>


										</select>
										</div>
										</div>
									</div>
								
								
								
								
								
								
								
								<%-- <div class="col-md-6 " style="margin-bottom: 5px;">

										<div class="form-group form-float" style="margin-top: 15px;">

											<div class="form-line">
												<input type="text" class="form-control"
													name="<%=ILabelConstants.Country%>"
													id="<%=ILabelConstants.Country%>"
													value="<%=WebUtil.processString(objUMB.getStrAddress())%>"
													style="background-color: white"> <label
													class="form-label" >Country</label>

											</div>
										</div>
								</div> --%>
								<div class="col-md-6 " style="margin-bottom: 5px;">

										<div class="form-group form-float" style="margin-top: 15px;">

											<div class="form-line">
												<input type="text" class="form-control"
													name="<%=ILabelConstants.ZipCode%>"
													id="<%=ILabelConstants.ZipCode%>"
													value="<%=WebUtil.processString(objUMB.getStrZipCode())%>"
													style="background-color: white"> <label
													class="form-label" >Zip Code</label>

											</div>
										</div>
								</div>
									
									
									<%-- <div class="col-sm-6" >
										<div class="form-group form-float">

											<textarea rows="2" cols="30" class="form-control"
												style="background-color: #eee"
												id="<%=ILabelConstants.address%>"
												name="<%=ILabelConstants.address%>"
												
												placeholder="Enter Address here..."><%=WebUtil.processString(objUMB.getStrAddress())%></textarea>

										</div>


									</div> --%>
								
								
								
								
							</div> <!-- end of row-clarifix -->
							
								
						
							
							
							
							
					
						</div> <!-- end of body -->
						<div class="panel-group full-body" id="accordion_19" role="tablist" aria-multiselectable="true" style="  margin-bottom: 0px; padding-bottom: 20px;">
                                        
                                        <div class="panel ">
                                            <div class="panel-heading" role="tab" id="headingTwo_19">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" style="color:red" data-toggle="collapse" href="#collapseTwo_19" aria-expanded="false" aria-controls="collapseTwo_19">
                                                        <i class="material-icons" style="color:red">account_circle</i>Change Password
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo_19" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_19">
                                                
                                                    
                                                    <div class="row clearfix ">
						<div class="col-md-6 " style="margin-bottom: 0px;">

										<div class="form-group form-float" style="margin-top: 15px;margin-left: 15px;padding-right: 20px">

											<div class="form-line">
												<input type="text" class="form-control"
												FieldType = "mandatory" FieldName="User Name"
													name="<%=ILabelConstants.UserName%>"
													id="<%=ILabelConstants.UserName%>"
													value="<%=WebUtil.processString(objUMB.getStrUserName())%>"
													style="background-color: white"> <label
													class="form-label" >User
													Name*</label>

											</div>
										</div>
						</div>
						
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-bottom: 0px;">

										<div class="form-group form-float" style="margin-top: 15px;padding-left: 15px;padding-right: 20px;">

											<div class="form-line">
												<input type="text" class="form-control "
												FieldType = "mandatory" FieldName="Password"
													name="<%=ILabelConstants.password%>"
													id="<%=ILabelConstants.password%>"
													toggle="#password-field"
													
													value="<%=WebUtil.processString(objUMB.getStrPassword())%>"
													style="background-color: white" > <!-- onblur="checkPass()" -->
													<label
													class="form-label">Password*</label>
													

											</div>
										</div>
						</div>
						<!-- <div class=" col-lg-1 col-md-1 col-sm-2 col-xs-2" style="margin-top: 20px;padding-left: 0px;">
						<span toggle="#password-field" class="fa fa-fw fa-eye field_icon toggle-password"></span></div>
 -->

						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-bottom: 0px;">

										<div class="form-group form-float" style="margin-top: 15px; margin-left: 15px;padding-right: 20px">

											<div class="form-line">
												<input type="text" class="form-control"
												FieldType = "mandatory" FieldName="Confirm Password"
													name="<%=ILabelConstants.confirmPassword%>"
													id="<%=ILabelConstants.confirmPassword%>"
													value="<%=WebUtil.processString(objUMB.getStrConfirmPassword())%>"
													style="background-color: white" >  <!-- onblur="checkConfPass()" -->
													<label
													class="form-label" >Confirm Password*</label>

											</div>
										</div>
						</div>
						<!-- <div class="demo-google-material-icon col-lg-1 col-md-1 col-sm-2 col-xs-2" style="margin-top: 20px;padding-left: 0px;">
						<span toggle="#password-field" class="fa fa-fw fa-eye field_icon toggle-confirmpassword"></span></div>
						 -->
						<div class="col-sm-6" style=" margin-top: 15px; margin-bottom:20px; padding-left:30px; padding-right: 35px;" >
										<div class="form-group form-float" >

											<div class="form-line">
										<select class="form-control show-tick"
											id="<%=ILabelConstants.selSecQuestion%>"
											name="<%=ILabelConstants.selSecQuestion%>">
											<option value="0">-- Select Security Question--</option>
											<%
												if (null != lstSecQuestion) {
													for (int i = 0; i < lstSecQuestion.size(); i++) {
														SecurityQuestionBean objSQB = (SecurityQuestionBean) lstSecQuestion.get(i);
											%>

											<option value="<%=objSQB.getLngID()%>" <%if(objUMB.getLngSecurityID() == objSQB.getLngID()){ %> selected <% }%> ><%=objSQB.getStrName()%></option>

											<%
												}

												}
											%>


										</select>
										</div>
										</div>
									</div>
						
						
						
						
						
						
						
						<div class="col-md-6 " style="margin-bottom: 0px; margin-left: 15px;padding-right: 38px">

										<div class="form-group form-float" >

											<div class="form-line">
												<input type="text" class="form-control"
													name="<%=ILabelConstants.SecAnswer%>"
													id="<%=ILabelConstants.SecAnswer%>"
													value="<%=WebUtil.processString(objUMB.getStrSecurityAnswer())%>"
													style="background-color: white"> <label
													class="form-label" >Security Answer</label>

											</div>
										</div>
						</div> 
					
						
						
						
						</div>
                                                
                                            </div>
                                        </div>
                                        
                                    </div>
						
						
						
						
						
						</div> <!-- end of class="card" -->
						<div class="card">
							<div id="ActionPanel" class="row no-margin-bottom default-margin "
							style="background-color: #006699;border:none; height: 50px; margin-right: 0px; margin-left: 0px; width: 100%">
							<div class="col-xs-10 col-md-10 col-sm-10 col-lg-10 floatleft" style="margin-top:4px">
								  <input type="button" value="Add New"  class="btn btn-primary "
									  onClick="gotoThisPage('memberMaster.jsp')"/> 
								 <input type="button" id="myBtn" value="Save"  class="btn btn-primary "
									 data-type="success"  onClick="onSave(),topFunction()" /> 
									<!-- <input type="button" value="Delete" class="btn btn-danger "
									 onClick="onDelete()" /> -->
							</div>
							<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2 floatRight"
								style="float: right; margin-top:4px">
								<input type="button" value="Close"
									class="btn btn-danger rightDefaultMargin alignRight"
									style="float: right; background-color: blue;"
									onClick="onCancel('membersList.jsp')" />
							</div>
						</div>
						</div>
				
				
				
				
				
				
				
				
				
				
				
				
				</div> <!-- end of class="col-lg-12 col-md-12 col-sm-12 col-xs-12" -->
			</div> <!-- end of row clearfix -->
			
		</div>
			<jsp:include page="../MyIncludes/incPageVariables.jsp"
								flush="true">

								<jsp:param name="USER_ACTION" value="<%=strUserAction%>" />

								<jsp:param name="IS_AJAX_SUBMIT" value="true" />
							</jsp:include>
	</section>
	

	<jsp:include page="../MyIncludes/incBottomPageReferences.jsp"
		flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>
	<!-- Jquery Core Js -->
	<!-- <script src="../plugins/jquery/jquery.min.js"></script> -->
	

	<!-- Bootstrap Core Js -->
	<script src="../plugins/bootstrap/js/bootstrap.js"></script>

	<!-- Select Plugin Js -->
	<script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

	<!-- Slimscroll Plugin Js -->
	<script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

	<!-- Waves Effect Plugin Js -->
	<script src="../plugins/node-waves/waves.js"></script>

	<!-- Jquery CountTo Plugin Js -->
	<script src="../plugins/jquery-countto/jquery.countTo.js"></script>

	<!-- Morris Plugin Js -->
	<!-- <script src="../plugins/raphael/raphael.min.js"></script>
	<script src="../plugins/morrisjs/morris.js"></script> -->
	
	<!-- Autosize Plugin Js -->
    <script src="../plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="../plugins/momentjs/moment.js"></script>
	
	 <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="../plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
	 <script src="../js/pages/forms/basic-form-elements.js"></script>

	 <!-- Bootstrap Material Datetime Picker Plugin Js -->
<!--  <script
		src="../plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>  -->

	<!-- Bootstrap Datepicker Plugin Js -->
	<!-- <script
		src="../plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>  -->

	 <!-- dialogs Sweet Alert  -->
     <script src="../js/pages/ui/dialogs.js"></script>

	<!-- Sparkline Chart Plugin Js -->
	<script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script>

	<!-- Custom Js -->
	<script src="../js/admin.js"></script>
	<!-- <script src="../js/pages/index.js"></script> -->

	<!-- Demo Js -->
	<script src="../js/demo.js"></script>
	
	
	<script src="../js/vendor/jquery.ui.widget.js"></script>
	<script src="../js/jquery.iframe-transport.js"></script>
	<script src="../js/jquery.fileupload.js"></script>
	
	
	<script type="text/javascript">
    
	$(document).on('click', '.toggle-password', function() {

	    $(this).toggleClass("fa-eye fa-eye-slash");
	    
	    var input = $("#<%=ILabelConstants.password%>");
	    input.attr('type') === 'password' ? input.attr('type','text') : input.attr('type','password')
	    	
	    
	    		
	});
	
	$(document).on('click', '.toggle-confirmpassword', function() {

	    $(this).toggleClass("fa-eye fa-eye-slash");
	    
	   
	    	
	    var input = $("#<%=ILabelConstants.confirmPassword%>");
	    input.attr('type') === 'password' ? input.attr('type','text') : input.attr('type','password')
	    		
	});
	
	
	 $(function() {

	        $('#fileupload').fileupload({
	        	dataType: 'json',
	            done: function (e, data) {
	            	alert('done');
	                $.each(data.result.files, function (index, file) {
	                    $('<p/>').text(file.name).appendTo(document.body);
	                });
	            },
	            progressall: function (e, data) {
	                var progress = parseInt(data.loaded / data.total * 100, 10);
	                $('#progress .bar').css(
	                    'width',
	                    progress + '%'
	                );
	            }
	        }); 
	        
	      });

	// Scroll up
	 window.onscroll = function() {scrollFunction()};

	 function scrollFunction() {
	     if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
	        // document.getElementById("myBtn").style.display = "block";
	     } else {
	        // document.getElementById("myBtn").style.display = "none";
	     }
	 }

	 // When the user clicks on the button, scroll to the top of the document
	 function topFunction() {
	     document.body.scrollTop = 0;
	     document.documentElement.scrollTop = 0;
	 }

	 // scroll up ends

	</script>
	</form>
</body>

</html>
