<!DOCTYPE html>

<%@page import="com.uthkrushta.mybos.configuration.bean.GlobalUrlSettingBean"%>
<%@page import="java.util.Date"%>
<%@page import="com.uthkrushta.mybos.master.controller.PhotoBibMapController"%>
<%@page import="com.uthkrushta.mybos.configuration.bean.EventPhotoUploadBean"%>
<%@page import="com.uthkrushta.mybos.configuration.controller.EventPhotoUploadController"%>
<%@page import="com.uthkrushta.basis.constants.ISessionAttributes"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeMasterBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventMasterBean"%>
<%@page import="java.awt.image.DataBuffer"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.uthkrushta.basis.bean.MessageBean"%>
<%@page import="com.uthkrushta.basis.controller.GenericController"%>

<%@page import="org.ajaxanywhere.AAUtils"%>
<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<html>
<%
	String strContextPath = "../";
	strContextPath = WebUtil.parseString(request, IUMCConstants.CONTEXT_PATH);
%>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>Eventzalley</title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" type="text/css">
<!-- Bootstrap Select Css -->
<link href="../plugins/bootstrap-select/css/bootstrap-select.css"
	rel="stylesheet" />

 <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Light Gallery Plugin Css -->
    <link href="../plugins/light-gallery/css/lightgallery.css" rel="stylesheet">

<link href="../Style/default.css"
	rel="stylesheet" />

<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->

 <link href="../css/style.css" rel="stylesheet">
  
<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="../css/themes/all-themes.css" rel="stylesheet" /> 

	
	<!-- ---------- To achieve Zoom-------------- -->
	<style>
		/* styles unrelated to zoom */
		* { border:0; margin:0; padding:0; }
		p { position:absolute; top:3px; right:28px; color:#555; font:bold 13px/1 sans-serif;}

		/* these styles are for the demo, but are not required for the plugin */
		.zoom {
			display:inline-block;
			position: relative;
		}
		
		/* magnifying glass icon */
		.zoom:after {
			content:'';
			display:block; 
			width:33px; 
			height:33px; 
			position:absolute; 
			top:0;
			right:0;
			background:url(icon.png);
		}

		.zoom img {
			display: block;
		}

		.zoom img::selection { background-color: transparent; }

		
	</style>


<!-- ----------------End of Zoom --------------------------- -->
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
<script type="text/javascript">
    
/*  $("#callDelete").click(function () {
	alert('fdfdsf');
	console.log("hi");
	
	
	
}); */  


   function onSave(){
    	
	   var eventID = $("#<%=ILabelConstants.selEventName%> option:selected").val();
   	
   	if(eventID == 0){
   		alert("Please Select Event Name");
   		return;
   	}
    		
    		document.forms[0].hdnUserAction.value = "SUBMIT";
            /* alert(document.forms[0].hdnUserAction.value);*/
            
         		
         	
         		
         		if(null!= document.forms[0].hdnAjaxSubmit && document.forms[0].hdnAjaxSubmit.value.toLowerCase()== 'true')	
         		{	
         			
         			ajaxAnywhere.submitAJAX();
         		}
         		else
         		{
         			document.forms[0].submit();
         		}
    		
    	
     		
    } 
   
   function onPublish(){
   	
	   var eventID = $("#<%=ILabelConstants.selEventName%> option:selected").val();
   	
   	if(eventID == 0){
   		alert("Please Select Event Name");
   		return;
   	}
    		
    		document.forms[0].hdnUserAction.value = "PUBLISH";
            /* alert(document.forms[0].hdnUserAction.value);*/
            
         		
         	
         		
         		if(null!= document.forms[0].hdnAjaxSubmit && document.forms[0].hdnAjaxSubmit.value.toLowerCase()== 'true')	
         		{	
         			
         			ajaxAnywhere.submitAJAX();
         		}
         		else
         		{
         			document.forms[0].submit();
         		}
    		
    	
     		
    } 
   
   function currentpageUpdate(event){
	   var x = $("#<%=ILabelConstants.hdnTempCurrentPage %>").val();
	  
	   $("#<%=ILabelConstants.hdnCurrentPage %>").val(x);
	   
	   filterEntries(event);
	 
   }
    
    
    function displayImage(){
    	
    	
    	var eventID = $("#<%=ILabelConstants.selEventName%> option:selected").val();
    	
    	if(eventID == 0){
    		alert("Please Select Event Name");
    		return;
    	}
    	
    	 document.forms[0].hdnUserAction.value = "DISPLAY_IMAGE"; 
    	if (document.forms[0].hdnAjaxSubmit.value.toLowerCase() == 'true') {
    		
    		ajaxAnywhere.submitAJAX();
    	} else {

    		document.forms[0].submit();
    	}
    	
    }
   
    // Scroll up
   /*  window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("myBtn").style.display = "block";
        } else {
            document.getElementById("myBtn").style.display = "none";
        }
    }  */

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
   
    // scroll up ends

    
    function eventChange(){
    	document.forms[0].hdnUserAction.value = "Nothing";
    	if (document.forms[0].hdnAjaxSubmit.value.toLowerCase() == 'true') {
    		
    		ajaxAnywhere.submitAJAX();
    	} else {

    		document.forms[0].submit();
    	}
    	
    }
   
	
    
    
    </script>

</head>

<%
	int Index = 1;
	String strUserAction = "GET";
	String strStatusText = "";
	String strStatusStyle = "";
	String imageContextPath = "/EventzAlley/servlet1/"; 	
	int intRowCounter = 1;
	long eventID = 0;
	
	//Pagination
	int intCurrentPage = 1;
	long lngRecordCount = 0;
	long lngTotalPage = 1;
	int intRecordsPerPage = 10;
	boolean blnEnableNext = false;
	boolean blnEnablePrev = false; 
	
	
	
	
	
	
	
	List lstPhotos = null;
	List lstEvent = DBUtil.getRecords(ILabelConstants.BN_EVENT_MASTER, IUMCConstants.GET_ACTIVE_ROWS+" AND dtStartDate <= '"+WebUtil.formatDate(new Date(), IUMCConstants.DB_DATE_FORMAT)+"' ", "dtStartDate DESC");
	EventPhotoUploadBean objEPUB = new EventPhotoUploadBean();
	PhotoBibMapController objGC = new PhotoBibMapController();
	
	ArrayList<MessageBean> arrMsg = null; 
	MessageBean objMsg;
	
	
	
	//Pagination 
	int intNumberOfRecords = WebUtil.parseInt(request, ILabelConstants.txtRecordsPerPage);
	intCurrentPage = WebUtil.parseInt(request, ILabelConstants.hdnCurrentPage);
	intRecordsPerPage = WebUtil.parseInt(request, ILabelConstants.txtRecordsPerPage);
	if(intRecordsPerPage <=0)
	   intRecordsPerPage = 10;
	if(intNumberOfRecords == 0){
	intNumberOfRecords = 10;
	}
	if(intCurrentPage == 0){
	intCurrentPage = 1;
	}
	intRowCounter=WebUtil.parseInt(request.getParameter(ILabelConstants.hdnRowCounter));
	AAUtils.addZonesToRefresh(request, "Status,List,Footer"); 
		//Ends Pagination 
	
	
	
	
	
	
	
	if (AAUtils.isAjaxRequest(request)) {
		strUserAction = WebUtil.processString(request.getParameter(ILabelConstants.hdnUserAction));
		
		if (strUserAction.equalsIgnoreCase(ILabelConstants.DISPLAY_IMAGE)) {
			lstPhotos = objGC.getList(request); 
					if(null!= lstPhotos)
					{
						intRowCounter = lstPhotos.size(); 
					}
					AAUtils.addZonesToRefresh(request, "sessionRefresh,List");
					}				
		if (strUserAction.equalsIgnoreCase("Nothing")) {
			
			intCurrentPage = 1;
			
		}
		 objGC.doAction(request, response);
		 lstPhotos = objGC.getList(request); 
		if(null!= lstPhotos)
		{
			intRowCounter = lstPhotos.size(); 
			AAUtils.addZonesToRefresh(request, "sessionRefresh,pagination");
		} 
		AAUtils.addZonesToRefresh(request, "sessionRefresh");
		}
	
	
	
	lngRecordCount =  objGC.getRecordsCount(request); 
	if(lngRecordCount > 0 && intRecordsPerPage > 0){
		lngTotalPage = Math.round(lngRecordCount / intRecordsPerPage);
		
		if(lngRecordCount % intRecordsPerPage > 0){
	lngTotalPage = lngTotalPage+1;
		}
	}
	
	if(intCurrentPage == lngTotalPage){
		blnEnableNext = false;
	}else{
		blnEnableNext = true;
	}
	if(intCurrentPage ==1){
		blnEnablePrev = false;
	}else {
		blnEnablePrev = true;
	}
	
	
	
	 arrMsg = objGC.getArrMsg();		//Getting the messages to display
		if(null!= arrMsg && arrMsg.size() > 0)
		{
			objMsg = MessageBean.getLeadingMessage(arrMsg);
			if(objMsg != null)
			{
		strStatusText = objMsg.getStrMsgText();
		strStatusStyle = objMsg.getStrMsgType();
			}
		}
		
		if(WebUtil.parseString(request, "statusText") != "") 	
		{
			strStatusText = WebUtil.parseString(request, "statusText");
			strStatusStyle = WebUtil.parseString(request, "statusStyle");
		}
	 
	 
	 
		GlobalUrlSettingBean objGISB = (GlobalUrlSettingBean) DBUtil.getRecord(IUMCConstants.BN_GLOBAL_IMAGE, IUMCConstants.GET_ACTIVE_ROWS, "");
		if(objGISB == null){
		objGISB = new GlobalUrlSettingBean();
		}
	
	
	
	AAUtils.addZonesToRefresh(request, "EventPhotoRefresh,List,Status");
	
	
	
	
%>



<body class="theme-blue" style="background-color: #e9e9e9;">
<form method="post">
	<!-- Page Loader -->




	<jsp:include page="../MyIncludes/incPageHeader.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
		 
	</jsp:include>
	<!-- #Top Bar -->

	<jsp:include page="../MyIncludes/incLeftPanel.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>

	




	<section class="content">
	
	
		
		
	 <jsp:include page="../MyIncludes/incPageHeaderInfo.jsp" flush="true">
					 
					    <jsp:param name="STATUS_STYLE" value="<%=strStatusStyle%>" />
					    <jsp:param name="STATUS_TEXT" value="<%=strStatusText%>" />
					</jsp:include> 
					
					
		<div class="container-fluid">
		
		<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card" style=" margin-bottom: 0px;">
                        <div class="header" >
							<h2 >Photo-BIB Map</h2>
						</div>
						<div id="ActionPanel" class="row no-margin-bottom default-margin "
							style="background-color: #006699;border:none; height: 50px; margin-right: 0px; margin-left: 0px; width: 100%">
							<div class="col-xs-10 col-md-10 col-sm-10 col-lg-10 floatleft" style="margin-top:4px">
								 <input type="button" value="Save"  class="btn btn-primary "
									 data-type="success" id="saveID" onClick="onSave(),topFunction()"/> 
									<input type="button" value="Publish"  class="btn btn-primary "
									 data-type="success" id="publishID" onClick="onPublish()"/> 
							</div>
							<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2 floatRight"
								style="float: right; margin-top:4px">
								<input type="button" value="Close"
									class="btn btn-danger rightDefaultMargin alignRight"
									style="float: right; background-color: blue;"
									onClick="onCancel('../myHome.jsp')" />
							</div>
						</div>
                        <div class="body" >
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12" style="padding-top: 20px;">
                                  
                                 <div class="col-sm-6" style="margin-top: 15px;" >
										<select class="form-control show-tick"
											id="<%=ILabelConstants.selEventName%>"
											name="<%=ILabelConstants.selEventName%>" onchange="eventChange()">

											<option value="0">-- Select Event*--</option>
											<%
												if (null != lstEvent) {
													for (int i = 0; i < lstEvent.size(); i++) {
														EventMasterBean objEMB = (EventMasterBean) lstEvent.get(i);
											%>

											<option value="<%=objEMB.getLngID()%>" ><%=objEMB.getStrName()%></option>

											<%
												}

												}
											%>


										</select>
									</div>
									
									<div class="col-sm-2" style="margin-top: 15px;" >
									<div class="form-group form-float" style="margin-bottom: 0px; ">
                                        
                                            <input type="button" class="form-control" style="background-color:#1f91f3" id="displayImages" value="View Photos" onclick="displayImage()" >
                                        
                                    </div>
										
									</div>
                                    
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	
			
			<input type="hidden" name="statusStyle" id="statusStyle" >
			<input type="hidden" name="statusText" id="statusText" >
			
		</div>
			
			
	</section>
	  
	
	
	<aa:zone name="List" skipIfNotIncluded="true"> 
	
	<section class="content" style=" margin-top: 0px;">
        <div class="container-fluid">
           
            
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        
                        <div class="body" style="padding-top: 0px;padding-bottom: 0px;">
                       
                            <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                             				<%
                      	    						if(null != lstPhotos){
                      	    							for(Index = 1; Index <= lstPhotos.size(); Index++){
                      	    							objEPUB = (EventPhotoUploadBean)lstPhotos.get(Index-1);  
                      	    				%>
                               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 " style=" margin-bottom: 0px;">
                                   <%--  <a href="<%=objEPUB.getStrImagePath()%>" data-sub-html="Demo Description">  --%>
                                   		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 zoom ex1">
                                        <img class="img-responsive thumbnail "  src="<%=objGISB.getStrName()%>event-image/<%=objEPUB.getLngEventID()%>/<%=objEPUB.getStrImagePath()%>"
                                        style="width: 500px;" onmouseover="callZoom()">
                                        
                                       
                                  <!--   </a> -->
                                    <input type="hidden" name="<%=ILabelConstants.hdnID_ + Index %>" value="<%=objEPUB.getLngID() %>" SIZE="2" > 
                                		</div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                               
                                <textarea rows="2" cols="30" class="form-control"
												style="background-color: #eee"  
												 id="<%=ILabelConstants.txtIMAGE_BIB_NUM%>" 
												name="<%=ILabelConstants.txtIMAGE_BIB_NUM_ + Index%>"
												
												placeholder="Enter BIB Numbers here..."><%=WebUtil.processString(objEPUB.getStrBibNoEntry()) %></textarea>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-10 col-xs-10">
                               
                                <textarea rows="2" cols="30" class="form-control"
												style="background-color: #eee"
												id="<%=ILabelConstants.txtIMAGE_DESCRIPTION%>"
												name="<%=ILabelConstants.txtIMAGE_DESCRIPTION_ + Index%>"
												
												placeholder="Enter Image Description here..."><%=WebUtil.processString(objEPUB.getStrImageDescription())%></textarea>
                                </div>
                                <div class="col-xs-2 col-sm-2 col-md-1 col-lg-1 demo-google-material-icon" style="margin-bottom: 0px; margin-top: 15px;padding-left: 0px;">
                                 <!--   <a href="javascript:void(0);" > <i class="material-icons" style="color:red">delete</i></a> -->
                               
                               
                               
                               
                               	<button  type="button"  onclick="deleteImage('<%=objEPUB.getStrImagePath()%>')" style="box-shadow:''" ><i class="material-icons" style="color:#c30000" >delete</i></button>
                               <!-- <i class="material-icons" >delete</i> -->
                              
                                </div>
                                </div> 
                                
                               			  <%	
                               			  }
                      						} else{%>
                      						<div style="padding-left: 15px;">
                      						
                      						No Records
                      						</div>
                      						
                     					<%} %>
                     					  
                                
                                
                            </div>
                            
                            <aa:zone name="pagination" skipIfNotIncluded="true">
                            <% if(lstPhotos != null && lstPhotos.size() > 0){%>
                            <table align="center" id="tableFooter" class="defaultRoundedBoxBottom" width="98%" style="border-collapse: collapse; background-color:#ffffff;" cellpadding="0" cellspacing="0">
							        
							        
							        
							        <tr>
							          <td width="40%" align="left" class="footerText">
							          
							          <div class="col-xs-3" style=" width: 55px;margin-top: 10px;padding-right: 0px;"> Page :</div> <div class="col-xs-3" style="padding-right: 0px;width: 85px;">
							          
							          
							          
							          
							          
							           <input type="text" class="form-control"  style="text-align:center;width: 86px;"
							           name="<%=ILabelConstants.hdnTempCurrentPage%>" id="<%=ILabelConstants.hdnTempCurrentPage%>" value="<%=intCurrentPage%>" 
							           SIZE="2" DataType ="positivenumericinteger"  onkeypress="currentpageUpdate(event)"></div> <!-- filterEntries(event) -->
							            <div class="col-xs-5" style="margin-top: 10px;padding-right: 0px;padding-left: 20px;"> of <%=lngTotalPage%></div>
							            
							            
							            
							            </td>
							            
							             <td width="25%">
							          	<div class="col-xs-5"> Records per Page :</div> <div class="col-xs-3"><input type="text" class="form-control"  style="text-align:center;width:50px;" name="<%=ILabelConstants.txtRecordsPerPage%>" value="<%=intRecordsPerPage%>" SIZE="2" DataType ="positivenumericinteger" disabled  onkeypress="filterEntries(event)"></div>
							          </td> 
							          
							          <%--  <td width="25%">
							          	<div class="col-xs-5"> Records per Page :</div> <div class="col-xs-3"><input type="text" class="form-control"  style="text-align:center;width:50px;" name="<%=ILabelConstants.txtRecordsPerPage%>" value="<%=intRecordsPerPage%>" SIZE="2" DataType ="positivenumericinteger"  onkeypress="filterEntries(event)"></div>
							          </td>  --%>
							          
							          <td width="43%" align="center">
							         	 <input type="button" value=" Prev " class="btn btn-primary" <%if(!blnEnablePrev){%> style="display:none;" <%}%> onClick="onPrev('<%=ILabelConstants.hdnCurrentPage%>','<%=ILabelConstants.hdnPageEdited%>')" name="btnPrev" >
							         	 <input type="button" value=" Next " class="btn btn-primary" <%if(!blnEnableNext){%>style="display:none;"<%}%> name="btnNext" onClick="onNextAutoSave('<%=ILabelConstants.hdnCurrentPage%>','<%=ILabelConstants.hdnPageEdited%>')">
							          </td>
							          <td width="18%" align="right" >&nbsp;Total Records : <%=lngRecordCount%></td>
							        </tr>
							      </table>
							      <%}%>
                      </aa:zone>
   	   
    
	
                    </table>
                        
<input type="hidden" id="globalSetting" value="<%=WebUtil.processString(objGISB.getStrName())%>">

 
                            
                            
                            
                            <jsp:include page="../MyIncludes/incPageVariables.jsp"
								flush="true">

								<jsp:param name="USER_ACTION" value="<%=strUserAction%>" />
								<jsp:param name="ROW_COUNTER" value="<%=intRowCounter%>" />
								<jsp:param name="CURRENT_PAGE" value="<%=intCurrentPage%>" />
								<jsp:param name="IS_AJAX_SUBMIT" value="true" />
							</jsp:include>
                            
                        </div>
                        <div class="card">
							<div id="ActionPanel" class="row no-margin-bottom default-margin "
							style="background-color: #006699;border:none; height: 50px; margin-right: 0px; margin-left: 0px; width: 100%">
							<div class="col-xs-10 col-md-10 col-sm-10 col-lg-10 floatleft" style="margin-top:4px">
								 <input type="button" value="Save"  class="btn btn-primary "
									 data-type="success" id="saveID" onClick="onSave(),topFunction()"/> 
									 <input type="button" value="Publish"  class="btn btn-primary "
									 data-type="success" id="publishID" onClick="onPublish()"/> 
									<!-- <input type="button" value="Delete" class="btn btn-danger "
									 onClick="onDelete()" /> -->
							</div>
							<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2 floatRight"
								style="float: right; margin-top:4px">
								<input type="button" value="Close"
									class="btn btn-danger rightDefaultMargin alignRight"
									style="float: right; background-color: blue;"
									onClick="onCancel('../myHome.jsp')" />
							</div>
						</div>
						</div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
   
                            
    </aa:zone>
	


	<jsp:include page="../MyIncludes/incBottomPageReferences.jsp"
		flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>
	<!-- Jquery Core Js -->
	<!-- <script src="../plugins/jquery/jquery.min.js"></script> -->
	
	<!-- Bootstrap Core Js -->
	<script src="../plugins/bootstrap/js/bootstrap.js"></script>
	
	  <!-- Validation Plugin Js -->
    <script src="../plugins/jquery-validation/jquery.validate.js"></script>

	<!-- Select Plugin Js -->
	<script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

	<!-- Slimscroll Plugin Js -->
	 <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script> 

	<!-- Waves Effect Plugin Js -->
	<script src="../plugins/node-waves/waves.js"></script>

	<!-- Jquery CountTo Plugin Js -->
	 <script src="../plugins/jquery-countto/jquery.countTo.js"></script> 

	
	<!-- Sparkline Chart Plugin Js -->
	 <script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script> 
	
	  <!-- Light Gallery Plugin Js -->
    <script src="../plugins/light-gallery/js/lightgallery-all.js"></script>

    <!-- Custom Js -->
    <script src="../js/pages/medias/image-gallery.js"></script>

	<!-- Custom Js -->
	<script src="../js/admin.js"></script>
	<!-- <script src="../js/pages/index.js"></script> -->
	

	<script src='../js/jquery.zoom.js'></script>
	<script>
	
		$(document).ready(function(){
			
			$('.ex1').zoom();
			
		});
		
		function callZoom(){
			$('.ex1').zoom();
		}

	function deleteImage(path){
	
		var eventID = $("#<%=ILabelConstants.selEventName%> option:selected").val();
		var urlVal = $("#globalSetting").val();
		var url = urlVal+"delete-image/"+path+"/"+eventID;
    	
		console.log("url === "+url);
		
		 $.ajax({
	            type: "DELETE",
	            url: url,
	            
	           success: function dummy(){
	            	$("#statusText").val("Deleted Successfully");
	            	$("#statusStyle").val("StatusOk");
	            	$("#displayImages").click();
	            	topFunction();
	            },
	            error: function (e) {
	            	console.log("ERROR : ", e);
	            }
	            
	        });
		
	}
	
		
		
		
	</script>


	  
	</form>
</body>

</html>
