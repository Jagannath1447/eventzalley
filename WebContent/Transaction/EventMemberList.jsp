<!DOCTYPE html>

<%@page import="com.uthkrushta.mybos.code.bean.StatusBean"%>
<%@page import="com.uthkrushta.mybos.transaction.controller.EventMemberListController"%>
<%@page import="com.uthkrushta.mybos.transaction.bean.view.EventMemberListViewBean"%>
<%@page import="java.util.Date"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeMasterBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventMasterBean"%>
<%@page import="java.awt.image.DataBuffer"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.uthkrushta.basis.bean.MessageBean"%>
<%@page import="com.uthkrushta.basis.controller.GenericController"%>

<%@page import="org.ajaxanywhere.AAUtils"%>
<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.mybos.code.bean.PaymentModeCodeBean"%>

<%@page import="com.uthkrushta.mybos.master.bean.AreaMasterBean"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<html>
<%
	String strContextPath = "../";
	strContextPath = WebUtil.parseString(request, IUMCConstants.CONTEXT_PATH);
%>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>Eventzalley</title>
<!-- Favicon-->
<!-- <link rel="icon" href="favicon.ico" type="image/x-icon"> -->

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" type="text/css">
<!-- Bootstrap Select Css -->
<link href="../plugins/bootstrap-select/css/bootstrap-select.css"
	rel="stylesheet" />

<link href="../Style/default.css"
	rel="stylesheet" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<link href="../css/themes/all-themes.css" rel="stylesheet" /> 
<script type="text/javascript">


function findMembers(){
	var eventName = $("#<%=ILabelConstants.selEventName%> option:selected").val();
	if(eventName == 0){
		alert("Please Select Event Name")
		return;
	}
	document.forms[0].hdnUserAction.value = "FindMembers";
	ajaxAnywhere.submitAJAX();
}




function findEventType(){
	var eventName = $("#<%=ILabelConstants.selEventName%> option:selected").val();
	if(eventName == 0){
		alert("Please Select Event Name")
		return;
	}
	document.forms[0].hdnUserAction.value = "FindEventType";
	
	ajaxAnywhere.submitAJAX();
}


function scrollWin() {
    window.scrollBy(0, 100); // Scroll down by 100px
}

    </script>

</head>

<%

String strUserAction="GET";
long eventID = 0;
long eventTypeID = 0;
int intRowCounter = 1;

//Pagination
	int intCurrentPage = 1;
	long lngRecordCount = 0;
	long lngTotalPage = 1;
	int intRecordsPerPage = 50;
	boolean blnEnableNext = false;
	boolean blnEnablePrev = false; 

	StatusBean objSB = new StatusBean();
List lstEventNames =  DBUtil.getRecords(ILabelConstants.BN_EVENT_MASTER, IUMCConstants.GET_ACTIVE_ROWS+" AND dtStartDate <= '"+WebUtil.formatDate(new Date(), IUMCConstants.DB_DATE_FORMAT)+"' ", "dtStartDate DESC");
List lstEventType = null;	
List lstMembers = null;
EventMemberListViewBean objEMLVB = new EventMemberListViewBean();
EventMemberListController objGC = new EventMemberListController();

//Pagination 
	int intNumberOfRecords = WebUtil.parseInt(request, ILabelConstants.txtRecordsPerPage);
	intCurrentPage = WebUtil.parseInt(request, ILabelConstants.hdnCurrentPage);
	intRecordsPerPage = WebUtil.parseInt(request, ILabelConstants.txtRecordsPerPage);
	if(intRecordsPerPage <=0)
	   intRecordsPerPage = 50;
	if(intNumberOfRecords == 0){
	intNumberOfRecords = 50;
	}
	if(intCurrentPage == 0){
	intCurrentPage = 1;
	}
	intRowCounter=WebUtil.parseInt(request.getParameter(ILabelConstants.hdnRowCounter));
	/* AAUtils.addZonesToRefresh(request, "Status,List,Footer");  */
		//Ends Pagination 












if (AAUtils.isAjaxRequest(request)) {
	strUserAction = WebUtil.processString(request.getParameter(ILabelConstants.hdnUserAction));
	intRowCounter = WebUtil.parseInt(request.getParameter(ILabelConstants.hdnRowCounter));	
	if (strUserAction.equalsIgnoreCase(ILabelConstants.FindEventType)) {
		eventID = WebUtil.parseInt(request, ILabelConstants.selEventName);
		
		if(eventID > 0){
			
			lstEventType = DBUtil.getRecords(ILabelConstants.BN_EVENT_TYPE_MASTER, IUMCConstants.GET_ACTIVE_ROWS+" AND lngEventID ="+eventID, "strName");
			
			AAUtils.addZonesToRefresh(request, "EventTypeRefresh");
			}	
			
		}
	if (strUserAction.equalsIgnoreCase(ILabelConstants.FindMembers)) {
		eventID = WebUtil.parseInt(request, ILabelConstants.selEventName);
		eventTypeID = WebUtil.parseInt(request, ILabelConstants.selEventTypeName);
		intCurrentPage =1;
		lstMembers = objGC.getList(request);
		objSB = (StatusBean)objGC.get(request);
		if(lstMembers != null){
			intRowCounter = lstMembers.size();
			}else{
				intRowCounter=0;
			}
		AAUtils.addZonesToRefresh(request, "List,footer,Search");
		}
	if (strUserAction.equalsIgnoreCase(ILabelConstants.GET)) {
		eventID = WebUtil.parseInt(request, ILabelConstants.selEventName);
		eventTypeID = WebUtil.parseInt(request, ILabelConstants.selEventTypeName);
		
		lstMembers = objGC.getList(request);
		objSB = (StatusBean)objGC.get(request);
		if(lstMembers != null){
			intRowCounter = lstMembers.size();
			}else{
				intRowCounter=0;
			}
		AAUtils.addZonesToRefresh(request, "List,footer,Search");
		}
	
	
}


		
eventTypeID = WebUtil.parseInt(request, ILabelConstants.selEventTypeName);		

		
		
		
lngRecordCount =  objGC.getRecordsCount(request); 
if(lngRecordCount > 0 && intRecordsPerPage > 0){
	lngTotalPage = Math.round(lngRecordCount / intRecordsPerPage);
	
	if(lngRecordCount % intRecordsPerPage > 0){
lngTotalPage = lngTotalPage+1;
	}
}

if(intCurrentPage == lngTotalPage){
	blnEnableNext = false;
}else{
	blnEnableNext = true;
}
if(intCurrentPage ==1){
	blnEnablePrev = false;
}else {
	blnEnablePrev = true;
}




%>



<body class="theme-blue" style="background-color: #e9e9e9">
<form method="post">
	<!-- Page Loader -->




	<jsp:include page="../MyIncludes/incPageHeader.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
		 
	</jsp:include>
	<!-- #Top Bar -->

	<style type="text/css">
		.bar {
		    height: 18px;
		    background: #1f91f3;
		}
	</style>

	<jsp:include page="../MyIncludes/incLeftPanel.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>

	




	<section class="content">
		<div class="container-fluid" style="margin-bottom: 30px;">
		
			<div class="row clearfix ">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="card" style="margin-bottom: 0px;">
						<div class="header"  >
							<h2 >Event-Member List</h2>
						</div>
						
						<div id="ActionPanel" class="row no-margin-bottom default-margin "
							style="background-color: #006699;border:none; height: 45px; margin-right: 0px; margin-left: 0px; width: 100%">
							
							
							<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2 floatRight"
								style="float: right;">
								<input type="button" value="Close"
									class="btn btn-danger rightDefaultMargin alignRight"
									style="float: right; background-color: blue;"
									onClick="onCancel('../myHome.jsp')" />
							</div>
						</div>
						 
						
						<div class="body" >
						
							<div class="row clearfix">
							
								<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style=" padding-top: 20px;margin-top: 25px;">
								<select class="form-control show-tick"
									id="<%=ILabelConstants.selEventName%>"
									name="<%=ILabelConstants.selEventName%>" onChange="findEventType()" >  
									<option value="0">-- Select Event Name*--</option>
									<%
										if (null != lstEventNames) {
											for (int i = 0; i < lstEventNames.size(); i++) {
												EventMasterBean objEMB = (EventMasterBean) lstEventNames.get(i);
									%>

									<option value="<%=objEMB.getLngID()%>"><%=objEMB.getStrName()%></option>
								
									<%
										}

										}
									%>


								</select>
							</div>
								<aa:zone name="EventTypeRefresh" skipIfNotIncluded="true">
							
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style=" padding-top: 20px;margin-top: 25px;">
								<select class="form-control show-tick"
									id="<%=ILabelConstants.selEventTypeName%>"
									name="<%=ILabelConstants.selEventTypeName%>"  >  <!-- onChange="findEventTypeMember()" -->
									<option value="0">All Competitions</option>
									<%
										if (null != lstEventType) {
											for (int i = 0; i < lstEventType.size(); i++) {
												EventTypeMasterBean objETMB = (EventTypeMasterBean) lstEventType.get(i);
									%>

									<option value="<%=objETMB.getLngID()%>"><%=objETMB.getStrName()%></option>
								
									<%
										}

										}
									%>


								</select>
							</div>
							</aa:zone>
							
							<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4" style=" padding-top: 20px;margin-top: 25px; ">
							<input type="button" value="View Members" style="background-color: #1f91f3;" onclick="findMembers()">
							
							</div>
						
						
						
						
						
						
						</div>
						
						</div>
						 
						 <aa:zone name="List" skipIfNotIncluded="true">
						 <% 	if(null!=lstMembers){ %>
						<div class="body">
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                    <div class="panel-group full-body" id="accordion_19" role="tablist" aria-multiselectable="true">
                                       
                                            
                                            <div id="collapseOne_19" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_19">
                                               
                                                  
                                                    <div class="body table-responsive" style="padding-top: 0px;padding-left: 0px;padding-right: 0px;">
                          
                            <table class="table table-hover" >
                                <thead>
                                    <tr>
                                       <%-- <th style=" padding-bottom: 0px; border-bottom-width: 0px;">
                         	 <input type="checkbox" id="md_checkbox_29" class="filled-in chk-col-teal" name="<%=ILabelConstants.chkMain%>" value="1" onclick="SelectAll('chkChild_',this,hdnRowCounter,hdnSelectedID)" />
                                <label for="md_checkbox_29" style=" margin-bottom: 0px;"></label>
                          </th> --%>
                                        <th>Name</th>
                                        <th>BIB No.</th>
                                        <th>Category</th>
                                        <th>Result</th>
                                        <!-- <th>DOB</th>
                                        <th>Gender</th>
                                        <th>Mobile No.</th>
                                        <th>Place</th>  -->
                                       <th></th>
                                       
                                       
                                       <!-- <img border="0" src="../Images/Filter.gif" width="16" height="16" ></th> -->
                                        
                                       	
                                        
                                    </tr>
                                    <aa:zone name="Search" skipIfNotIncluded="true">
                                    <tr id="tableFilter"  >
                                    	
                                    	<td><input  type="text" name="<%=ILabelConstants.txtNameFilter%>"  onkeypress="filterEntries(event)" value="<%=WebUtil.processString(objSB.getStrName()) %>" ></td> <%-- value="<%=WebUtil.processString(objSB.getStrName()) %>" --%>
                                         <td><input  type="text" name="<%=ILabelConstants.txtBibNoFilter%>" onkeypress="filterEntries(event)" value="<%=WebUtil.processString(objSB.getStrBibNo())%>" ></td> <%-- value="<%=WebUtil.processString(objSB.getStrPlace())%>" --%>
                                       
                                        <td><input  type="text" name="<%=ILabelConstants.txtCategoryFilter%>" onkeypress="filterEntries(event)" value="<%=WebUtil.processString(objSB.getStrCategory()) %>"> <%-- value="<%=WebUtil.processString(objSB.getStrMob()) %>" --%>
                                   		 <td></td>
                                   		<td style="padding-bottom: 0px; padding-top: 0px;" class="clickable">
                                   		
                                   		<input type="button" value="Search"
									
									style="float: right; background-color: #1f91f3; margin-top: 6px;"
									onClick="searchFilterEntries()" />
                                   		
                                   		
                                   		</td>
                                   </tr>
                                    </aa:zone>
                                    
                                </thead>
                                
                                <tbody>
                                <% 	
               			 				for(int index =1; index<=lstMembers.size(); index++)
               			 				{
               			 				objEMLVB = (EventMemberListViewBean) lstMembers.get(index-1);
               			 				
               			 				
               			 				%>
                                    <tr>
                                         <%-- <td style="padding-bottom: 1px;border-bottom-width: 1px;"><input type="checkbox" id="md_checkbox_29<%= +index %>" class="filled-in chk-col-teal" name="<%=ILabelConstants.chkChild_+index %>" value="<%=objEMLVB.getLngID() %>" onclick="CheckSelectSingle(this,chkMain,hdnRowCounter,hdnSelectedID)" />
                                		<label style=" margin-bottom: 0px;" for="md_checkbox_29<%= +index %>"></label></td>  --%>
                                        
                                       
                                       
                                        <td class="clickable" onClick="gotoThisPage('../Masters/memberMaster.jsp?<%=ILabelConstants.PAGE_TYPE %>=Edit&<%=ILabelConstants.ID %>=<%=objEMLVB.getLngMemberID()%>')">
                                         <input type="hidden"
									name="<%=ILabelConstants.hdnItemRowCounter%>" size="2"
									value="<%=intRowCounter%>"
									id="<%=ILabelConstants.hdnItemRowCounter%>">
                                        
                                        
                                        <%=WebUtil.processString(objEMLVB.getStrFirstName())%> <%=WebUtil.processString(objEMLVB.getStrLastName())%></td>
                                       <td><%=objEMLVB.getStrBibNo() %></td>
                                       <td><%=WebUtil.processString(objEMLVB.getStrCategoryName()) %></td>
                                       <td>
                                      <%if(eventTypeID == 0){ %>
                                       
                                       <input type="button" value="Details" style="background-color: #1f91f3;" onClick="gotoThisPage('memberResultUpdate.jsp?<%=ILabelConstants.PAGE_TYPE %>=Edit&<%=ILabelConstants.EID%>=<%=objEMLVB.getLngEventID()%>&<%=ILabelConstants.UID%>=<%=objEMLVB.getLngMemberID()%>')"></td>
                                       
										<%} else{ %>								
<input type="button" value="Details" style="background-color: #1f91f3;" onClick="gotoThisPage('memberResultUpdate.jsp?<%=ILabelConstants.PAGE_TYPE %>=Edit&<%=ILabelConstants.EID%>=<%=objEMLVB.getLngEventID()%>&<%=ILabelConstants.UID%>=<%=objEMLVB.getLngMemberID()%>&<%=ILabelConstants.ETID%>=<%=eventTypeID%>')">

<%} %>






<%-- <td><%=WebUtil.formatDate(objUMB.getDtDOB(), IUMCConstants.DATE_FORMAT)%></td>
                                        <td> <% if(lstGen != null){ 
                                        	for(int i= 0; i<lstGen.size(); i++){
                                        		objGen = (MyBosGender)lstGen.get(i);
                                        		if(objGen.getLngID() == objUMB.getLngGenderID() ){ %>
                                        	<%=objGen.getStrName() %>
                                        <%} }}%> </td>
                                        <td><%=WebUtil.processString(objUMB.getStrPrimaryContactNumber()) %></td>
                                        <td><%=WebUtil.processString(objUMB.getStrCity()) %></td> --%>
                                       <td></td>
                                       
                                       
                                    </tr>
                                     	<% }%>
                                    
                                    
                                     <aa:zone name="footer" skipIfNotIncluded="true">
                                     <table align="center" id="tableFooter" class="defaultRoundedBoxBottom" width="100%" style="border-collapse: collapse; background-color:#ffffff;" cellpadding="0" cellspacing="0">
							        
							        
							        
							        <tr>
							          <td width="40%" align="left" class="footerText">
							          
							          <div class="col-xs-3" style=" width: 55px;margin-top: 10px;padding-right: 0px;"> Page :</div> 
							          <div class="col-xs-3" style="padding-right: 0px;width: 85px;">
							          
							          
							          
							          
							          
							           <input type="text" class="form-control"  style="text-align:center;width: 86px;"
							           name="<%=ILabelConstants.hdnTempCurrentPage%>" id="<%=ILabelConstants.hdnTempCurrentPage%>" value="<%=intCurrentPage%>" 
							           SIZE="2" DataType ="positivenumericinteger"  onkeypress="currentpageUpdate(event)"></div> <!-- filterEntries(event) -->
							            <div class="col-xs-5" style="margin-top: 10px;padding-right: 0px;padding-left: 20px;"> of <%=lngTotalPage%></div>
							            
							            
							            
							            </td>
							          
							           <td width="25%">
							          	<div class="col-xs-5"> Records per Page :</div> <div class="col-xs-3"><input type="text" class="form-control"  style="text-align:center;width:50px;" name="<%=ILabelConstants.txtRecordsPerPage%>" value="<%=intRecordsPerPage%>" SIZE="2" DataType ="positivenumericinteger"  onkeypress="filterEntries(event)"></div>
							          </td>  
							          
							          <td width="43%" align="center">
							         	 <input type="button" value=" Prev " class="btn btn-primary" <%if(!blnEnablePrev){%> style="display:none;" <%}%> onClick="onPrev('<%=ILabelConstants.hdnCurrentPage%>','<%=ILabelConstants.hdnPageEdited%>')" name="btnPrev" >
							         	 <input type="button" value=" Next " class="btn btn-primary" <%if(!blnEnableNext){%>style="display:none;"<%}%> name="btnNext" onClick="onNext('<%=ILabelConstants.hdnCurrentPage%>','<%=ILabelConstants.hdnPageEdited%>')">
							          </td>
							          <td width="18%" align="right" >&nbsp;Total Records : <%=lngRecordCount%></td>
							        </tr>
							      </table> 
                                    </aa:zone>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                </tbody>
                                
                           
                         
                            </table>
                           
                        </div>
                                             
                                            </div>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                              
						<%}
                                     	else{%>
                                     	<div class="row" style=" padding-left: 30px; padding-bottom: 10px;">
                                     	<td style="border-bottom-width:10px;margin-left:25px; align:center">No Records</td>
                                     	</div>
                                     	
                                     	
                                     	<%}%>
                                     	<jsp:include page="../MyIncludes/incPageVariables.jsp" flush="true">
                           <jsp:param name="CURRENT_PAGE" value="<%=intCurrentPage%>" />
	                   <jsp:param name="USER_ACTION" value="<%=strUserAction%>" />
   		               <jsp:param name="ROW_COUNTER" value="<%=intRowCounter%>" />
   		               <jsp:param name="IS_AJAX_SUBMIT" value="true" />
   	                   </jsp:include>
                        </div>
						</aa:zone>
						
				
						
						
						</div>
						
					</div>
				</div>
			
		
			
	</section>
 




	<jsp:include page="../MyIncludes/incBottomPageReferences.jsp"
		flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>
	<!-- Jquery Core Js -->
	<!-- <script src="../plugins/jquery/jquery.min.js"></script> -->

	<!-- Bootstrap Core Js -->
	<script src="../plugins/bootstrap/js/bootstrap.js"></script>

	<!-- Select Plugin Js -->
	<script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

	<!-- Slimscroll Plugin Js -->
	 <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script> 

	<!-- Waves Effect Plugin Js -->
	<script src="../plugins/node-waves/waves.js"></script>

	<!-- Jquery CountTo Plugin Js -->
	 <script src="../plugins/jquery-countto/jquery.countTo.js"></script> 

	<!-- Morris Plugin Js -->
	<script src="../plugins/raphael/raphael.min.js"></script>
	<script src="../plugins/morrisjs/morris.js"></script>

	 <!-- Bootstrap Material Datetime Picker Plugin Js -->
	<!-- <script
		src="../plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>  -->

	<!--  Bootstrap Datepicker Plugin Js -->
	<!-- <script
		src="../plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>  --> 

	 <!-- dialogs Sweet Alert  -->
     <script src="../js/pages/ui/dialogs.js"></script>

	<!-- Sparkline Chart Plugin Js -->
	 <script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script> 

	<!-- Custom Js -->
	<script src="../js/admin.js"></script>
	<!-- <script src="../js/pages/index.js"></script> -->

	<!-- Demo Js -->
	<!-- <script src="../js/demo.js"></script> -->
	
	<!--  multiple file upload  --> 
	
    <script src="../js/vendor/jquery.ui.widget.js"></script>
	<script src="../js/jquery.iframe-transport.js"></script>
	<script src="../js/jquery.fileupload.js"></script>
	
	 <script>
	 function currentpageUpdate(event){
		   var x = $("#<%=ILabelConstants.hdnTempCurrentPage %>").val();
		  
		   $("#<%=ILabelConstants.hdnCurrentPage %>").val(x);
		   
		   filterEntries(event);
		 
	   }

    </script>
</form>
</body>

</html>
