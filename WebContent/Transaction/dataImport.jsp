<!DOCTYPE html>

<%@page import="com.uthkrushta.mybos.configuration.bean.GlobalUrlSettingBean"%>

<%@page import="java.util.Date"%>
<%@page import="com.uthkrushta.mybos.master.controller.EventListController"%>
<%@page import="com.uthkrushta.mybos.configuration.bean.EventPhotoUploadBean"%>
<%@page import="com.uthkrushta.mybos.configuration.controller.EventPhotoUploadController"%>
<%@page import="com.uthkrushta.basis.constants.ISessionAttributes"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeMasterBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventMasterBean"%>
<%@page import="java.awt.image.DataBuffer"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.uthkrushta.basis.bean.MessageBean"%>
<%@page import="com.uthkrushta.basis.controller.GenericController"%>

<%@page import="org.ajaxanywhere.AAUtils"%>
<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<html>
<%
	String strContextPath = "../";
	strContextPath = WebUtil.parseString(request, IUMCConstants.CONTEXT_PATH);
%>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>Eventzalley</title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" type="text/css">
<!-- Bootstrap Select Css -->
<link href="../plugins/bootstrap-select/css/bootstrap-select.css"
	rel="stylesheet" />
  <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Light Gallery Plugin Css -->
    <link href="../plugins/light-gallery/css/lightgallery.css" rel="stylesheet">

<link href="../Style/default.css"
	rel="stylesheet" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


<link href="../css/themes/all-themes.css" rel="stylesheet" /> 
<script type = "text/javascript">
function findEventType(){
	var eventName = $("#<%=ILabelConstants.selEventName%> option:selected").val();
	if(eventName == 0){
		alert("Please Select Event Name")
		return;
	}
	document.forms[0].hdnUserAction.value = "FindEventType";
	
	ajaxAnywhere.submitAJAX();
}
  

    </script>

</head>

<%


String strUserAction="GET";
long eventID = 0;
long eventTypeID = 0;


List lstEventNames =  DBUtil.getRecords(ILabelConstants.BN_EVENT_MASTER, IUMCConstants.GET_ACTIVE_ROWS+"AND intIsCompleted = 0  AND dtStartDate <= '"+WebUtil.formatDate(new Date(), IUMCConstants.DB_DATE_FORMAT)+"' ", "dtStartDate DESC");
List lstEventType = null;

Long memberID = (Long) session.getAttribute("LOGIN_ID");

long userID=0;


if(null != memberID ){
	userID = memberID.longValue();
}


int intCurrentPage = 1;
int intRowCounter = 1;
String strSelectedID = ";";
String strStatusText = "";
String strStatusStyle = "";


ArrayList<MessageBean> arrMsg = null; 
MessageBean objMsg;

GlobalUrlSettingBean objGISB = (GlobalUrlSettingBean) DBUtil.getRecord(IUMCConstants.BN_GLOBAL_IMAGE, IUMCConstants.GET_ACTIVE_ROWS, "");
if(objGISB == null){
objGISB = new GlobalUrlSettingBean();
}


if (AAUtils.isAjaxRequest(request)) {
	strUserAction = WebUtil.processString(request.getParameter(ILabelConstants.hdnUserAction));
	intRowCounter = WebUtil.parseInt(request.getParameter(ILabelConstants.hdnRowCounter));	
	if (strUserAction.equalsIgnoreCase(ILabelConstants.FindEventType)) {
		eventID = WebUtil.parseInt(request, ILabelConstants.selEventName);
		
		if(eventID > 0){
			
			lstEventType = DBUtil.getRecords(ILabelConstants.BN_EVENT_TYPE_MASTER, IUMCConstants.GET_ACTIVE_ROWS+" AND lngEventID ="+eventID, "strName");
			
			AAUtils.addZonesToRefresh(request, "EventTypeRefresh");
			}	
			
		}
	
	
	
	
}


		
eventTypeID = WebUtil.parseInt(request, ILabelConstants.selEventTypeName);		







//arrMsg = objGC.getArrMsg();		//Getting the messages to display
if(null!= arrMsg && arrMsg.size() > 0)
{
	objMsg = MessageBean.getLeadingMessage(arrMsg);
	if(objMsg != null)
	{
strStatusText = objMsg.getStrMsgText();
strStatusStyle = objMsg.getStrMsgType();
	}
}
AAUtils.addZonesToRefresh(request, "List");
%>



<body class="theme-blue" style="background-color: #e9e9e9;">
<form method="post" enctype="multipart/form-data" id="fileUploadForm">
	<!-- Page Loader -->




	<jsp:include page="../MyIncludes/incPageHeader.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
		 
	</jsp:include>
	<!-- #Top Bar -->



	<jsp:include page="../MyIncludes/incLeftPanel.jsp" flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>

	




	<section class="content">
	
	 <jsp:include page="../MyIncludes/incPageHeaderInfo.jsp" flush="true">
					 
					    <jsp:param name="STATUS_STYLE" value="<%=strStatusStyle%>" />
					    <jsp:param name="STATUS_TEXT" value="<%=strStatusText%>" />
					</jsp:include> 
		<div class="container-fluid">
		
		<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header" >
							<h2 >Import Data</h2>
						</div>
						<div id="ActionPanel" class="row no-margin-bottom default-margin "
							style="background-color: #006699;border:none; height: 45px; margin-right: 0px; margin-left: 0px; width: 100%">
							
							
							<div class="col-xs-2 col-md-2 col-sm-2 col-lg-2 floatRight"
								style="float: right;">
								<input type="button" value="Close"
									class="btn btn-danger rightDefaultMargin alignRight"
									style="float: right; background-color: blue;"
									onClick="onCancel('../myHome.jsp')" />
							</div>
						</div>
						
						
                        <div class="body">
                        
                        
                        
                        
                        
                         <div class="body" style="margin-top: 30px;">
                            <div class="row clearfix">
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12" style="background-color: #e8e3e3;padding-top: 20px;">
                                      
                                            <div class="col-xs-12 col-lg-2 col-sm-12 col-md-2 ">
												  Import Members 
                                               </div>       
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="padding-top: 0px;margin-top: 0px; margin-bottom: 20px; width: 200px;padding-left: 0px;">
								<select class="form-control show-tick"
									id="<%=ILabelConstants.selEventName2%>"
									name="<%=ILabelConstants.selEventName2%>"  >  
									<option value="0">Select Event Name*</option>
									<%
										if (null != lstEventNames) {
											for (int i = 0; i < lstEventNames.size(); i++) {
												EventMasterBean objEMB = (EventMasterBean) lstEventNames.get(i);
									%>

									<option value="<%=objEMB.getLngID()%>"><%=objEMB.getStrName()%></option>
								
									<%
										}

										}
									%>


								</select>
							</div>
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2" style="padding-right: 0px;margin-left: 0px; margin-bottom: 20px; padding-left: 0px;">
                                            <input type="file" id="dataUpload" value="Upload" style="width: 202px;">
                                            </div>
                                       		
                                       		
                                       		<div class="col-xs-12 ol-sm-12 col-md-2 col-lg-2">
                                       		
                                       		<input type="button" id="eventDataUpload" class="dataSubmit" value="Upload" style="background-color: #1f91f3; float:right">
                                       		</div>
                                    </div>
                                
                                <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12" style="background-color:#e8e3e3;padding-left: 0px;">
									 <div class="body" style=" margin-bottom: 25px;">
                                        <div class="col-xs-12 col-lg-2 col-sm-12 col-md-2 ">
                                                  Import Event Result
                                            </div>
                                            
                                            
							<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="padding-top: 0px;margin-top: 0px; margin-bottom: 20px; width: 200px;padding-left: 0px;">
								
							<select class="form-control show-tick"
									id="<%=ILabelConstants.selEventName%>"
									name="<%=ILabelConstants.selEventName%>" onChange="findEventType()" >  
									<option value="0">Select Event Name*</option>
									<%
										if (null != lstEventNames) {
											for (int i = 0; i < lstEventNames.size(); i++) {
												EventMasterBean objEMB = (EventMasterBean) lstEventNames.get(i);
									%>

									<option value="<%=objEMB.getLngID()%>"><%=objEMB.getStrName()%></option>
								
									<%
										}

										}
									%>


								</select>
							</div>
								<aa:zone name="EventTypeRefresh" skipIfNotIncluded="true">
							
							<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="padding-top: 0px;margin-top: 0px;width: 200px;margin-bottom: 20px;padding-left: 0px;">
								<select class="form-control show-tick"
									id="<%=ILabelConstants.selEventTypeName%>"
									name="<%=ILabelConstants.selEventTypeName%>" >  
									<option value="0">All Competitions</option>
									<%
										if (null != lstEventType) {
											for (int i = 0; i < lstEventType.size(); i++) {
												EventTypeMasterBean objETMB = (EventTypeMasterBean) lstEventType.get(i);
									%>

									<option value="<%=objETMB.getLngID()%>"><%=objETMB.getStrName()%></option>
								
									<%
										}

										}
									%>


								</select>
							</div>
							</aa:zone>
							
                                            
                                        	<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2" style="padding-right: 0px;margin-left: 0px; margin-bottom: 20px; padding-left: 0px;">
                                            <input type="file" id="resultUpload" value="Upload" style="width: 202px;">
                                            </div>
                                        	<div class="col-xs-12 ol-sm-12 col-md-2 col-lg-2" style="padding-left: 65px;">
                                       		
                                       		<div class="row">
                                       		<div class="col-xs-12 ol-sm-12 col-md-6 col-lg-6" style="padding-left: 0px;">
                                       		<input type="button" id="eventResultIncrement"  value="Add" style="background-color: #1f91f3">
                                       		</div>
                                       		<div class="col-xs-12 ol-sm-12 col-md-6 col-lg-6" style="padding-left: 20px;">
                                       		<input type="button" id="eventResultFresh" class="btnSubmit" value="Fresh Upload" style="background-color: #1f91f3; margin-left: 12px;" >
                                       		</div>
                                       		</div>
                                       		
                                       		</div>
                                       		</div>
                                        </div>
                                            
                                    </div>
                            </div>
                            </div>
                            <!-- <div id="resultStatus"> -->
                              <!-- <span id="result" style="color:blue">Result Status :</span> -->
                             <div class="loadSpinner" style="background-color: #f4f3f99c;position: fixed;display: block;top: 30%;left: 50%;padding-left: 25px;padding-top: 25px;padding-right: 25px;padding-bottom: 25px;">
                              <div class="preloader pl-size-xl "  >
                              <div class="spinner-layer" style="border-color:#1f91f3">
                                        <div class="circle-clipper left">
                                            <div class="circle"></div>
                                        </div>
                                        <div class="circle-clipper right">
                                            <div class="circle"></div>
                                        </div>
                                        
                                    </div> 
                                   </div>
                                   <br><span style="color:red">Please wait..</span>
                              </div> 
                              
                              <table  id="resultTable" border="1" style="margin-left: 25px;">								  
								  <thead>
								    <tr>
								      <th style="text-align:center">Result Status</th>
								    </tr>
								  </thead>
								  <tbody id="tBody"></tbody>
							</table>
                            <!-- </div> -->
                            </div>
                        <input type="hidden" value="<%=userID%>" id="userID">
                        
                         <jsp:include page="../MyIncludes/incPageVariables.jsp" flush="true">
                           <jsp:param name="CURRENT_PAGE" value="<%=intCurrentPage%>" />
	                   <jsp:param name="USER_ACTION" value="<%=strUserAction%>" />
   		               <jsp:param name="ROW_COUNTER" value="<%=intRowCounter%>" />
   		               <jsp:param name="IS_AJAX_SUBMIT" value="true" />
   	                   </jsp:include>
                        
                        
                        </div>
                        
                    </div>
                </div>
            </div>
		
		
			
		</div>
			
			<input type="hidden" id="globalSetting" value="<%=WebUtil.processString(objGISB.getStrName())%>">
			
	</section>
 


	<jsp:include page="../MyIncludes/incBottomPageReferences.jsp"
		flush="true">
		<jsp:param value="../" name="CONTEXT_PATH" />
	</jsp:include>
	<!-- Jquery Core Js -->
	<!-- <script src="../plugins/jquery/jquery.min.js"></script> -->

	<!-- Bootstrap Core Js -->
	<script src="../plugins/bootstrap/js/bootstrap.js"></script>

	<!-- Select Plugin Js -->
	<script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

	<!-- Slimscroll Plugin Js -->
	 <script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script> 

	<!-- Waves Effect Plugin Js -->
	<script src="../plugins/node-waves/waves.js"></script>

	<!-- Jquery CountTo Plugin Js -->
	 <script src="../plugins/jquery-countto/jquery.countTo.js"></script> 

	<!-- Morris Plugin Js -->
	<script src="../plugins/raphael/raphael.min.js"></script>
	<script src="../plugins/morrisjs/morris.js"></script>

	 <!-- Bootstrap Material Datetime Picker Plugin Js -->
	<!-- <script
		src="../plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>  -->

	<!--  Bootstrap Datepicker Plugin Js -->
	<!-- <script
		src="../plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>  --> 

	 <!-- Light Gallery Plugin Js -->
    <script src="../plugins/light-gallery/js/lightgallery-all.js"></script>

    <!-- Custom Js -->
    <script src="../js/pages/medias/image-gallery.js"></script>


	 <!-- dialogs Sweet Alert  -->
     <script src="../js/pages/ui/dialogs.js"></script>

	<!-- Sparkline Chart Plugin Js -->
	 <script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script> 

	<!-- Custom Js -->
	<script src="../js/admin.js"></script>
	<!-- <script src="../js/pages/index.js"></script> -->

	<!-- Demo Js -->
	<!-- <script src="../js/demo.js"></script> -->
	
	
	
	 <script>
	 
	 
	 
	 
	 $(".loadSpinner").hide();
	 
	 var userID = $("#userID").val();
	
 	
	 $(document).ready(function () {
			
		    $("#eventDataUpload").click(function (event) {
		    	
				var eventID = $("#<%=ILabelConstants.selEventName2%>  option:selected").val();
				
				if(eventID == 0){
					alert("Please Select EventName Name");
					return;
				}
		    	
		    	$("#eventDataUpload").prop("disabled", true);
		    	var trHtml = '<tr><td>Loading...</td></tr>';
		    	$('#resultTable tbody').html(trHtml);
		    	$(".loadSpinner").show();
		    	
		        //stop submit the form, we will post it manually.
		        event.preventDefault();

		        // Get form
		        var form = $('#fileUploadForm')[0];

		        
				// Create an FormData object 
		        var data = new FormData();
				
		    
		        
		        jQuery.each(jQuery('#dataUpload')[0].files, function(i, file) {
		            data.append('spreadsheet', file);
		        });

		       
		        
				// If you want to add an extra field for the FormData
		        data.append("spreadsheet", "This is some extra data, testing");

				// disabled the submit button
		        $("#eventDataUpload").prop("disabled", true);
		        $("#resultStatus").text("Upload Status : ")
		        
		        var urlVal = $("#globalSetting").val();
				var url = urlVal+"member-upload/event/"+eventID+"/"+userID;
				
				
		        $.ajax({
		            type: "POST",
		            enctype: 'multipart/form-data',
		            //url: "http://localhost:8080/result-upload/event/25/FULL",
		            url: url,
		            data: data,
		            processData: false,
		            contentType: false,
		            cache: false,
		            timeout: 600000,
		            success: function (resp) {

		                $("#result").text(data);
		                console.log("SUCCESS : ", resp);
		                
		                $("#eventDataUpload").prop("disabled", false);
		                var trHtml = '';
		                
		                if(resp.length == 1 && resp[0] == 'Success'){
		                	trHtml += '<tr><td style="background-color:#1a6910; color:white; padding-left: 15px;padding-right: 15px;">Success</td></tr>'
		                		$(".loadSpinner").hide();
		                }else{
		                	trHtml += '<tr><td style="background-color:#fb483a; color:white; padding-left: 15px;padding-right: 15px;">Correct these errors</td></tr>';
			                $.each(resp, function (i) {
	                            console.log('resp: ',resp, i);
	                            trHtml +=
		                            '<tr><td style="padding-left: 15px;padding-right: 15px;">'
		                            + resp[i]                                 
		                            + '</td></tr>';
	                            
	                        });
			              
		                }
		                $('#resultTable tbody').html(trHtml);
		                $("#eventDataUpload").prop("disabled", false);	
		                var val="stop"
		                	  $(".loadSpinner").hide();
		               

		            },
		            error: function (e) {
		              
		                var trHtml = '<tr><td style="color:red">'+e.responseText+'</td></tr>';
		                $('#resultTable tbody').html(trHtml);
		                console.log("ERROR : ", e);

		                $("#eventDataUpload").prop("disabled", false);		                
		                $(".loadSpinner").hide();
		            }
		        });

		    });

		});
	 
	 
	 $(document).ready(function () {
		    $("#eventResultFresh").click(function (event) {
		    	var eventID = $("#<%=ILabelConstants.selEventName%>  option:selected").val();
				var eventTypeID = $("#<%=ILabelConstants.selEventTypeName%>  option:selected").val();
				
				if(eventTypeID == 0){
					alert("Please Select Competition Name");
					return;
				}
		    	
				
		    	
		    	callResultUpload("FULL",eventTypeID);
		    	

		    });
		    
		    
		$("#eventResultIncrement").click(function (event) {
			var eventID = $("#<%=ILabelConstants.selEventName%>  option:selected").val();
			var eventTypeID = $("#<%=ILabelConstants.selEventTypeName%>  option:selected").val();
			
			
			
			if(eventTypeID == 0){
				alert("Please Select Competition Name");
				return;
			}	
			
				
				
		    	callResultUpload("INCR",eventTypeID);
		    	

		    });
		    
		    function callResultUpload(loadtype,eventTypeID){
		    	//stop submit the form, we will post it manually.
		        event.preventDefault();
				
		        $("#eventResultIncrement").prop("disabled", true);
		        $("#eventResultFresh").prop("disabled", true);
		    	var trHtml = '<tr><td>Loading...</td></tr>';
		    	$('#resultTable tbody').html(trHtml);
		    	$(".loadSpinner").show();
		    	
		    
		        // Get form
		        var form = $('#fileUploadForm')[0];

				// Create an FormData object 
		        var data = new FormData();
				
		        jQuery.each(jQuery('#resultUpload')[0].files, function(i, file) {
		            data.append('spreadsheet', file);
		        });

				// If you want to add an extra field for the FormData
		        data.append("spreadsheet", "This is some extra data, testing");

				// disabled the submit button
		        $("#eventResultFresh").prop("disabled", true);
		        $("#resultStatus").text("Upload Status : ")
		        
				
				var urlVal = $("#globalSetting").val();
				var url = urlVal+"result-upload/event/"+eventTypeID+"/"+loadtype+"/"+userID;
				/* var url = "http://localhost:8089/result-upload/event/"+eventTypeID+"/"+loadtype+"/"+userID; */
				
		        $.ajax({
		            type: "POST",
		            enctype: 'multipart/form-data',
		            //url: "http://localhost:8080/result-upload/event/25/FULL",
		            url: url,
		            data: data,
		            processData: false,
		            contentType: false,
		            cache: false,
		            timeout: 600000,
		            success: function (resp) {

		                //$("#result").text(data);
		                console.log("SUCCESS : ", resp);
		                $("#eventResultFresh").prop("disabled", false);
		                var trHtml = '';
		                
		                if(resp.length == 1 && resp[0] == 'Success'){
		                	trHtml += '<tr><td style="background-color:#1a6910; color:white; padding-left: 15px;padding-right: 15px;">Success</td></tr>'
		                		 $(".loadSpinner").hide();
		                }
		                else{
		                	trHtml += '<tr><td style="background-color:#fb483a; color:white;padding-left: 15px;padding-right: 15px;">Correct these errors</td></tr>';
			                $.each(resp, function (i) {
	                            console.log('resp: ',resp, i);
	                            trHtml +=
		                            '<tr><td style="padding-left: 15px;padding-right: 15px;">'
		                            + resp[i]                                 
		                            + '</td></tr>';
	                            
	                        });
		                }
		                $('#resultTable tbody').html(trHtml);
		                $("#eventResultFresh").prop("disabled", false);		                
		                $("#eventResultIncrement").prop("disabled", false);
		                $(".loadSpinner").hide();

		            },
		            error: function (e) {

		                //$("#result").text(e.responseText);
		                var trHtml = '<tr><td style="color:red">'+e.responseText+'</td></tr>';
		                $('#resultTable tbody').html(trHtml);
		                console.log("ERROR : ", e);

		                $("#eventResultFresh").prop("disabled", false);		                
		                $("#eventResultIncrement").prop("disabled", false);
		                $(".loadSpinner").hide();
		            }
		        });
		    }

		});
	 
	
	
	 
		    
		  
	 
    </script>
	
  
	</form>
</body>

</html>
