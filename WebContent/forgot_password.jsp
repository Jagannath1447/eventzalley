<!DOCTYPE html>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.mybos.configuration.bean.GlobalUrlSettingBean"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Forgot Password | EventzAlley</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

	
	
    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">
    <script type="text/javascript">
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    
    function checkPass()
    {
    	var pass=document.forms[0].newPassword.value;
    	 var rpass=document.forms[0].confirmPassword.value;

    	 if(!rpass.trim()=="")
    		 {
                if(pass!=rpass)
                    {
                       alert("Your New Password and Confirm password did not match");
                       return false;
                    }
    		 }

    	 
    	 
    }
    function checkConfPass()
    {
     var pass=document.forms[0].newPassword.value;
     var rpass=document.forms[0].confirmPassword.value;

     if(pass.trim()=="")
    	 {
    	  alert('Please First Enter the password');
    	  document.forms[0].confirmPassword.value="";
    	  document.forms[0].newPassword.focus(); 
    	  return false;
    	 }
     
     if(pass!=rpass)
    	 {
    	  alert("Your New Password and Confirm password did not match");
    	  return false;
    	 }
     else{
    	 return true;
     }
    }
    
    </script>
    
</head>
<%

GlobalUrlSettingBean objGISB = (GlobalUrlSettingBean) DBUtil.getRecord(IUMCConstants.BN_GLOBAL_IMAGE, IUMCConstants.GET_ACTIVE_ROWS, "");
if(objGISB == null){
objGISB = new GlobalUrlSettingBean();
}

%>
<body class="fp-page">
<form id="forgot_password" method="POST">
    <div class="fp-box">
        <div class="logo" style=" margin-bottom: 0px;">
            <a href="javascript:void(0);"><img src="Images/EventzalleyLogo.png" width="70%"  ></a>
           <!--  <small>Admin BootStrap Based - Material Design</small> -->
        </div>
        <div class="card">
            <div class="body" style="padding-bottom: 0px;">
            <div class="row clearfix ">
                
                   <div class="mobileNumberEntry">
                    <div class="msg">
                        Please Enter Your Mobile Number
                    </div>
                    
                    <div class="input-group ">
                        <span class="input-group-addon">
                            <i class="material-icons">perm_phone_msg</i>
                        </span>
                        <div class="form-line">
                            <!-- <input type="email" class="form-control" name="email" placeholder="Email" required autofocus> -->
                       	 <input type="text" class="form-control" name="<%=ILabelConstants.mobileNumber %>" id="<%=ILabelConstants.mobileNumber %>"
                       	  placeholder="Mobile Number" required autofocus onkeypress="return isNumber(event)">
                        </div>
                        
                        
                    </div>
                    <div class="col-xs-4" >
                            <button class="btn btn-block bg-pink waves-effect" type="button" onClick="gotoThisPage('sign-in.jsp')">SIGN IN</button>
                        </div>
                    <div class="col-xs-4" style="float:right">
                            <button class="btn btn-block bg-pink waves-effect" type="button" id="generateOTP" class="generateOTP">SUBMIT</button>
                        </div>
                        </div>
                        <div class="otpSubmit">
                        <div class="msg">
                        Please Enter OTP
                    </div>
                    <div class="input-group ">
                        <span class="input-group-addon">
                            <i class="material-icons">perm_phone_msg</i>
                        </span>
                        <div class="form-line">
                            <!-- <input type="email" class="form-control" name="email" placeholder="Email" required autofocus> -->
                       	 <input type="text" class="form-control" name="<%=ILabelConstants.otpNumber %>" id="<%=ILabelConstants.otpNumber %>" placeholder="OTP" required autofocus>
                        </div>
                    </div>
                    <div class="col-xs-3" style=" padding-right: 0px;">
                            <button class="btn btn-block bg-pink waves-effect" type="button" onClick="gotoThisPage('sign-in.jsp')">SIGN IN</button>
                        </div>
                     <div class="col-xs-5" >
                            <button class="btn btn-block bg-pink waves-effect" type="button" id="resendOTP" class="generateOTP">RESEND OTP</button>
                        </div>
                    <div class="col-xs-4" style="float:right">
                            <button class="btn btn-block bg-pink waves-effect" type="button" id="otpConfirm">SUBMIT</button>
                        </div>
                    
                    </div>
                     <div class="resetPassword">
                     <div class="msg">
                        Reset Password
                    </div>
                    <div class="input-group">
                    
                        <span class="input-group-addon">
                            <i class="material-icons">perm_phone_msg</i>
                        </span>
                        <div class="form-line">
                            <!-- <input type="email" class="form-control" name="email" placeholder="Email" required autofocus> -->
                       	 <input type="password" class="form-control" name="<%=ILabelConstants.newPassword%>" id="<%=ILabelConstants.newPassword %>"
                       	 
                       	  placeholder="New password" onblur="checkPass()" required autofocus>
                        </div>
                        
                    </div>
                     <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">perm_phone_msg</i>
                        </span>
                        <div class="form-line">
                            <!-- <input type="email" class="form-control" name="email" placeholder="Email" required autofocus> -->
                       	 <input type="password" class="form-control" name="<%=ILabelConstants.confirmPassword%>" 
                       
                       	 id="<%=ILabelConstants.confirmPassword %>" placeholder="Confirm Password" required autofocus>
                        </div>
                        
                    </div>
                    
                    
                    <div class="col-xs-4" >
                            <button class="btn btn-block bg-pink waves-effect" type="button" onClick="gotoThisPage('sign-in.jsp')">SIGN IN</button>
                        </div>
                    
                    <div class="col-xs-4" style="float:right">
                            <button class="btn btn-block bg-pink waves-effect" type="button" id="resetPassword">SUBMIT</button>
                        </div>
					</div>
					
					
					
					<div class="successful">
                     <div class="msg" style="color:#a96824">
                        Password changed successfully. Redirecting to login page. Please wait
                    </div>
                    
                    
					</div>
					
					
                   <!--  <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">RESET MY PASSWORD</button>

                    <div class="row m-t-20 m-b--5 align-center">
                        <a href="sign-in.jsp">Sign In!</a>
                    </div> -->
                    
                    
                    
                    
                    
                
                </div>
            </div>
        </div>
   
   <input type="hidden" id="globalSetting" value="<%=WebUtil.processString(objGISB.getStrName())%>">
    </div>
    
    <footer class="legal" style="padding:8px;text-align:center; color:white">
      <span align="left" style="font-size:12px">EventzAlley. All Rights Reserved. Application Powered By <u><a href="http://www.uthkrushta.com" style="color:white"  target="_blank">Uthkrushta Technologies</a></u>, Bangalore.</span>
      <span align="right" style="padding:8px;">
      </span>
      
    </footer>

    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="plugins/jquery-validation/jquery.validate.js"></script>
	
	  <script src="js/commonActions.js"></script>
  
  
    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/examples/forgot-password.js"></script>
    <script>
    
    $(".otpSubmit").hide();
	$(".resetPassword").hide();
	$(".successful").hide();
	
	
	
	$("#resendOTP").click(function (event) {
		
		$("#generateOTP").click();
		 $("#otpConfirm").prop("disabled", false);
		$('#<%=ILabelConstants.otpNumber%>').val('');
		
	});
	
    
$("#generateOTP").click(function (event) {
    	
    	var mobNumber = $("#<%=ILabelConstants.mobileNumber %>").val();
    	var phoneno = /^[0]?[789]\d{9}$/;  
    	if (!mobNumber.match(phoneno)) {
	        alert("Enter Valid Mobile Number!!");
	        $('#<%=ILabelConstants.mobileNumber%>').val('');
	        $('#<%=ILabelConstants.mobileNumber%>').focus();
	        return ;
	    }
    	
    	
    	
    	var urlVal = $("#globalSetting").val();
		var url = urlVal+"generateOtp/"+mobNumber;
    	
		console.log("url === "+url);
		
		 $.ajax({
	            type: "GET",
	            url: url,
	          //  data: data,
	            processData: false,
	            contentType: false,
	            cache: false,
	            timeout: 600000,
	            success: function (resp) {
	               console.log("SUCCESS : ", resp);
	            	if(resp.responseStatus === 1){
	                	
	                	$(".mobileNumberEntry").hide();
		            	$(".otpSubmit").show();
		            	$(".resetPassword").hide();
	                }else{
	                	//alert("Entered mobile number is not registered.");
	                	alert(resp.responseString); 
	                	$('#<%=ILabelConstants.mobileNumber%>').val('');
	         	        $('#<%=ILabelConstants.mobileNumber%>').focus();
	                }
	            },
	            error: function (e) {
	            	console.log("ERROR : ", e);
	            }
	        });
	    });
    
 
$("#otpConfirm").click(function (event) {
	
	 var mobNumber = $("#<%=ILabelConstants.mobileNumber %>").val();
	 var otpNumber = $("#<%=ILabelConstants.otpNumber %>").val();
   	
	 var otpRE = /^\d{6}(-\d{5})?(?!-)$/ ;
	 if( !otpNumber.match(otpRE)){			//!zip.match(decimal)		!zip.match(zipno) &&
			
			alert("Invalid OTP");
			 $('#<%=ILabelConstants.otpNumber%>').val('');
			 $("#otpConfirm").prop("disabled", true);
			return;
			}
   	var urlVal = $("#globalSetting").val();
		var url = urlVal+"validateOtp/"+otpNumber+"/"+mobNumber;
		 $.ajax({
	            type: "GET",
	            url: url,
	          //  data: data,
	            processData: false,
	            contentType: false,
	            cache: false,
	            timeout: 600000,
	            success: function (resp) {
	               // console.log("SUCCESS : ", resp)
	                if(resp.responseStatus === 1){
	                	$(".mobileNumberEntry").hide();
		               	$(".otpSubmit").hide();
		               	$(".resetPassword").show();
	                }else{
	                	alert("Invalid OTP.");
	                	$('#<%=ILabelConstants.otpNumber%>').val('');
	                	$('#<%=ILabelConstants.otpNumber%>').focus();
	       			 $("#otpConfirm").prop("disabled", true);
	                }
	            },
	            error: function (e) {
	            	console.log("ERROR : ", e);
	            }
	        });
	    });

$("#resetPassword").click(function (event) {
	
	 var mobNumber = $("#<%=ILabelConstants.mobileNumber %>").val();
	 var pwd = $("#<%=ILabelConstants.newPassword %>").val();
	var data = {
			mobileNumber: mobNumber,
			password : pwd
	}
	 
	 if(!checkConfPass()){
	 return;
	 }
	var urlVal = $("#globalSetting").val();
	var url = urlVal+"reset-password";
	 $.ajax({
         type: "POST",
         url: url,
         data: JSON.stringify(data),
         processData: false,
         contentType: "application/json; charset=utf-8",
         dataType: "json",
         cache: false,
         timeout: 600000,
         success: function (resp) {
             console.log("SUCCESS : ", resp);
             if(resp.responseStatus === 1){
            	 $(".mobileNumberEntry").hide();
	             $(".otpSubmit").hide();
	             $(".resetPassword").hide();
            	 $(".successful").show();
            	TimeOut();
            	 
             }else{
             	
             }
         },
         error: function (e) {
         	console.log("ERROR : ", e);
         }
     });


});
    
function TimeOut()
  {
     setTimeout(function() {
    	 window.location.href="sign-in.jsp";}, 2000);
  }


    
    </script>
    </form>
</body>

</html>