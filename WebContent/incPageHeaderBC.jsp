<%@page import="com.uthkrushta.basis.constants.ISessionAttributes"%>
<%@page import="com.uthkrushta.mybos.frameWork.bean.ApplicationMenuBean"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>


<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/" %>
<%
	String strContextPath = "";
	 strContextPath = WebUtil.parseString(request, IUMCConstants.CONTEXT_PATH);  
%>
 <html>
 <%-- <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <LINK rel="stylesheet" href="<%=strContextPath%>Style/default.css" type="text/css">
<script src="<%=strContextPath%>js/jquery.min.js" type="text/javascript"></script>
<script src="<%=strContextPath%>js/applicationValidation.js" type="text/javascript"></script>
    <title>Welcome To | myBOS Asset Manager</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
  <!--   <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css"> -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<%=strContextPath%>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<%=strContextPath%>plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<%=strContextPath%>plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="<%=strContextPath%>plugins/morrisjs/morris.css" rel="stylesheet" />
	
	 <!-- Sweetalert Css -->
	  
    <link href="<%=strContextPath%>plugins/sweetalert/sweetalert.css" rel="stylesheet" />
     
     <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	
    <!-- Custom Css -->
    <link href="<%=strContextPath%>css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<%=strContextPath%>css/themes/all-themes.css" rel="stylesheet" />
    
   
</head> --%>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Welcome To | Bootstrap Based Admin Template - Material Design</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<%=strContextPath%>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<%=strContextPath%>plugins/node-waves/waves.css" rel="stylesheet" />
     

    <!-- Animation Css -->
    <link href="<%=strContextPath%>plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="<%=strContextPath%>plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<%=strContextPath%>css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<%=strContextPath%>css/themes/all-themes.css" rel="stylesheet" />
</head>
<%

 	
 	String strDisplayName = "GUEST";
	String strPageHeader = "";
	String strHomePage = "";
	String strCenterProfile="";
	long lngRoleID = 0;
	/* long lngUserID = ((Long) session.getAttribute(ISessionAttributes.LOGIN_ID)).longValue();
	long lngCompID = ((Long) session.getAttribute(IUMCConstants.COMPANY_ID)).longValue();
	 */
	List lstClientMsgs=null;
	List<ApplicationMenuBean> lstQuickMenuItems = null;
	ApplicationMenuBean objQuickMenuItems =new ApplicationMenuBean();
	  
	strHomePage = strContextPath + WebUtil.processString(session.getAttribute("HOME_PAGE_URL")+"");
	strCenterProfile=strContextPath+"Masters/masterCompanyProfile.jsp?COMPANY_TYPE=1&ACTIVITY_ID=15&MENU_ID=2";
	
 %>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
	 <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
	<nav class="navbar" style="background-color:#006699;
    height: 81px;
    margin-bottom: 0px;
    ">
    	 
        <div class="container-fluid">
        <div class="navbar-brand collapse navbar-collapse" href="<%=strContextPath%>index.html" style=" padding-top:0px; padding-bottom:0px; width:250px;height:82px"> <img src="<%=strContextPath%>Images/LogoTrans.png"  width="82" height="60" style="width: 70%;padding-left: 30%;height: 78px;" ></div>  
            <div class="navbar-header" style=" padding-top:0px; padding-bottom:0px; ">
            
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
             	
               <a class="navbar-brand" href="index.html" >myBOS Asset Manager</a><br>           
			
				<div >
            <a class="navbar-brand" style="font-size:12px; padding-top:0px;padding-bottom: 0px;font-size:12px; height:40px" >Good sdahvncl kjasbck kjasbckj dabscvjklb abdcklb ,adbsklb aslibc</a>
            </div> 
            </div>
            
            <div class="collapse navbar-collapse" id="navbar-collapse" >
                <ul class="nav navbar-nav navbar-right" style="background-color:#006699">
                    <!-- Call Search -->
                    <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>
                    <!-- #END# Call Search -->
                    <!-- Notifications -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span class="label-count">7</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">NOTIFICATIONS</li>
                            <li class="body">
                                <ul class="menu">
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-light-green">
                                                <i class="material-icons">person_add</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>12 new members joined</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 14 mins ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-cyan">
                                                <i class="material-icons">add_shopping_cart</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>4 sales made</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 22 mins ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-red">
                                                <i class="material-icons">delete_forever</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>Nancy Doe</b> deleted account</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 3 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-orange">
                                                <i class="material-icons">mode_edit</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>Nancy</b> changed name</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 2 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-blue-grey">
                                                <i class="material-icons">comment</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>John</b> commented your post</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 4 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-light-green">
                                                <i class="material-icons">cached</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4><b>John</b> updated status</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 3 hours ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-purple">
                                                <i class="material-icons">settings</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>Settings updated</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> Yesterday
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="javascript:void(0);">View All Notifications</a>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Notifications -->
                    <!-- Tasks -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">flag</i>
                            <span class="label-count">9</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">TASKS</li>
                            <li class="body">
                                <ul class="menu tasks">
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Footer display issue
                                                <small>32%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 32%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Make new buttons
                                                <small>45%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-cyan" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Create new dashboard
                                                <small>54%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 54%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Solve transition issue
                                                <small>65%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 65%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Answer GitHub questions
                                                <small>92%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 92%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="javascript:void(0);">View All Tasks</a>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Tasks -->
                    <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->	
		
		
		
		
		
  <%--   <!-- Jquery Core Js -->
    <script src="<%=strContextPath%>plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<%=strContextPath%>plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<%=strContextPath%>plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<%=strContextPath%>plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<%=strContextPath%>plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="<%=strContextPath%>plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Morris Plugin Js -->
    <script src="<%=strContextPath%>plugins/raphael/raphael.min.js"></script>
    <script src="<%=strContextPath%>plugins/morrisjs/morris.js"></script>

    <!-- ChartJs -->
    <script src="<%=strContextPath%>plugins/chartjs/Chart.bundle.js"></script>

    <!-- Flot Charts Plugin Js -->
    <script src="<%=strContextPath%>plugins/flot-charts/jquery.flot.js"></script>
    <script src="<%=strContextPath%>plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="<%=strContextPath%>plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="<%=strContextPath%>plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="<%=strContextPath%>plugins/flot-charts/jquery.flot.time.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="<%=strContextPath%>plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="<%=strContextPath%>js/admin.js"></script>
    <script src="<%=strContextPath%>js/pages/index.js"></script>
    
    <!-- Custom Js -->
   
    <script src="<%=strContextPath%>js/pages/ui/dialogs.js"></script>
    
      <!-- Jquery Core Js -->
    <script src="<%=strContextPath%>plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<%=strContextPath%>plugins/bootstrap/js/bootstrap.js"></script>
   
    <script src="<%=strContextPath%>js/pages/forms/form-wizard.js"></script>
     <!-- JQuery Steps Plugin Js -->
    <script src="<%=strContextPath%>plugins/jquery-steps/jquery.steps.js"></script>
    <!-- Select Plugin Js -->
    <script src="<%=strContextPath%>plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<%=strContextPath%>plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="<%=strContextPath%>plugins/jquery-validation/jquery.validate.js"></script>
    
     <!-- Jquery Core Js -->
    <script src="<%=strContextPath%>plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<%=strContextPath%>plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="<%=strContextPath%>plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<%=strContextPath%>plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Bootstrap Notify Plugin Js -->
    <script src="<%=strContextPath%>plugins/bootstrap-notify/bootstrap-notify.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<%=strContextPath%>plugins/node-waves/waves.js"></script>

    <!-- SweetAlert Plugin Js -->
    <script src="<%=strContextPath%>plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Custom Js -->
    <script src="<%=strContextPath%>js/admin.js"></script>
    <script src="<%=strContextPath%>js/pages/ui/dialogs.js"></script>

  

    <!-- Demo Js -->
    <script src="js/demo.js"></script> --%>
	</body>
</html>	