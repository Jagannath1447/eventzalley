/**
 * Application specific Validation  
 */
//****************************************************************************************************
// This file belongs to Uthkrushta TechnoLogies India Pvt.Ltd.
// Any changes to the below codes may cause problem. 
//***************************************************************************************************

    var WhiteSpace = " \t\n\r";
  
//*************************************************
    //Function   : isBlank
    //Description: Checks if the given string is blank
    //In         : TextValue
    //Out        : Boolean - True/False
   
//*************************************************
        
    function isBlank(strTextValue)
    {
	var intLength = strTextValue.length
	var intCount
		for(intCount=0;intCount<intLength;intCount++)
		{
			if((strTextValue.charAt(intCount)!=" "))
			{
				if(intCount < intLength-1)
				{
					if((strTextValue.charCodeAt(intCount) == "13") && (strTextValue.charCodeAt(intCount+1) == "10"))
					{
						intCount++;
					}	
					else
					{
						return false;
					}
				}
				else
				{	
					return false;
				}	
			}	
		}
		return true
    }
 
//*************************************************************************
    //Function   :    ValidateForm
    //In         :    null
    //out        :    validates all form elements depending 
    //		      on the attributes specifies within the tags.
    //Description:    This function checks whether the string contains
    //                special characters
   
//***************************************************************************


function ValidateForm()
{
//	if(!CheckFieldLengths())
//		return false;

	var objFrmElements = document.forms[0].elements
	
	
	var intCntVal = 0
	for(intCntVal = 0 ; intCntVal < objFrmElements.length; intCntVal++)
	{
		strElementType = objFrmElements[intCntVal].type
		
		
		if(document.forms[0].elements[intCntVal].disabled ==true)
			strElementType = ""
		if((strElementType == "text") || (strElementType =="select-one") || (strElementType  == "password")||(strElementType  == "file") || (strElementType  =="textarea") ||(strElementType  == "hidden"))
		{
			
			
			// New Additions
			
			var intMaxLength = objFrmElements[intCntVal].getAttribute('MaxLength')
			var strFieldName = objFrmElements[intCntVal].getAttribute('FieldName')
			
			
			 if(strFieldName == "undefined")
				strFieldName = objFrmElements[intCntVal].name
						
			var strFieldType = objFrmElements[intCntVal].getAttribute('FieldType')
			
			var strDataType  = objFrmElements[intCntVal].getAttribute('DataType')
			var strStringMin = objFrmElements[intCntVal].getAttribute('StringMin')
			var strStringMax = objFrmElements[intCntVal].getAttribute('StringMax')
			var strDateType  = objFrmElements[intCntVal].getAttribute('DateType')
			var strMinDiff   = objFrmElements[intCntVal].getAttribute('MinDiff')
			var strBlankSpace  = objFrmElements[intCntVal].getAttribute('BlankSpace')
			var strSpecialChar  = objFrmElements[intCntVal].getAttribute('CharType')
			var strPwdChar  = objFrmElements[intCntVal].getAttribute('PwdCharType')
			var strDecimal = objFrmElements[intCntVal].getAttribute('AmountType')
			var strDynamic = objFrmElements[intCntVal].getAttribute('DynamicType')
			var strDeciPart = objFrmElements[intCntVal].getAttribute('DeciPart')
			
			
			
			
			if(strBlankSpace !=null)
			{
			     
			     if(!isBlank(objFrmElements[intCntVal].value))
			 	{
			     	strText = objFrmElements[intCntVal].value  
			    	for(var intCounter=0;intCounter<strText.length; intCounter++)
		       		{
			        
				        if(strText.charAt(intCounter) ==" ")
			    		{			 
						alert(strFieldName+"cannot have BlankSpaces.")
			    			objFrmElements[intCntVal].select();
			    			objFrmElements[intCntVal].focus();
			    			return false
			              } 
			        }
			    } 	
			}
			
			
			if(strDecimal != null)
			{
				if(strDecimal.toLowerCase() == "decimal")
				{
					 if(!isBlank(objFrmElements[intCntVal].value))
				 	 {
				     	
				     	strAmount = objFrmElements[intCntVal].value 

				    	intDeciIndex = -1;
						intDeciIndex =strAmount.indexOf(".")  
		
						if(intDeciIndex+3 < strAmount.length )
						{
							if(intDeciIndex != -1)
							{
								alert('Please enter a valid amount.(Number ofdigits after the decimal cannot exceed two.)')
								objFrmElements[intCntVal].focus();
								objFrmElements[intCntVal].select();
								return false;
							}
						}
						intMaxAmount = Math.pow(10,intMaxLength-3)
						intMaxAmount -=0.01;
							
						if(Number(strAmount) > intMaxAmount)
						{
							
							alert('Amount cannot be greater than'+intMaxAmount+' Rs.')
							objFrmElements[intCntVal].focus();
								objFrmElements[intCntVal].select();
							return false;
						}	
				     } 	
			   	}
			}
			
			
			if(strSpecialChar !=null)
			{
			  if(strSpecialChar.toLowerCase() == "specialchar")
			  {
			
			
			     if(!isBlank(objFrmElements[intCntVal].value))
			     {
				     
			        strText = objFrmElements[intCntVal].value  
			              
			        var strChars = "*|\":<>[]{}`;&!$#%?^~+-@=(),'";
	        
		   			for(var intCounter=0;intCounter<strText.length; intCounter++)
		       			{
		    				
		    				if(strChars.indexOf(strText.charAt(intCounter)) != -1)
		    				{
		    					alert("Please do not enter Special Characters.")
		    					objFrmElements[intCntVal].select();
		    					objFrmElements[intCntVal].focus();
		    					return false
		            		        } 
		    			 
		      			 }
	 			    
				    }
			   }
		  
		  	}	
			
			
			
			if(strPwdChar !=null)
			{
			  if(strPwdChar.toLowerCase() == "pwdchar")
			  {
			
			
				if(!isBlank(objFrmElements[intCntVal].value))
				{
				     
				       strText = objFrmElements[intCntVal].value  
			              
		          	        var strChars = "*|\":<>[]{}`;&!$#%?^~+-@=(),./'_";
	        
	        
		   			for(var intCounter=0;intCounter<strText.length; intCounter++)
		       			{
		    				if(strChars.indexOf(strText.charAt(intCounter)) != -1)
		    				{
		    					alert("Please do not enter Special Characters.")
		    					objFrmElements[intCntVal].select();
		    					objFrmElements[intCntVal].focus();
		    					return false
		            		} 
		    				
		    				      		
		      			 
		      			 }
	 			    
				    }
			   }
		  
		  	}	
			
			
			
			if(strFieldType !=null)
			{
			if(strFieldType.toLowerCase() == "mandatory")
			{
				
				if((strElementType == "select-one"))
		       {
			   		
			   		if(objFrmElements[intCntVal].value == 0)
			   		{
			      
			     	alert("Please select  " +strFieldName);
			    	
			     	objFrmElements[intCntVal].focus();
			     	return false; 
			   
			   		}
				}
				
				if(isBlank(objFrmElements[intCntVal].value))
				{
								
					if (strElementType == "file")
					 {

					  alert('Please select the file you wish to Upload.')
					  objFrmElements[intCntVal].focus()
					  return false;

					}
					if (strElementType == "hidden")
					 {

					  alert('Please select the ' + strFieldName +'.')
					  objFrmElements[intCntVal].select()
					  objFrmElements[intCntVal].focus()
					  
					  return false;

					}
					
					else
					{
					  alert('Please enter the ' + strFieldName + '.')
					  objFrmElements[intCntVal].select()
					  objFrmElements[intCntVal].focus()
					  return false;
					}
				}	
			}
			} 
			
			
			
			if (strStringMax != "" && strStringMax!=null)
			{
				strValue = objFrmElements[intCntVal].value
				if (strValue.length > strStringMax)
				{
					alert(strStringMin + 'cannot exceed ' + strStringMax + ' characters')
					objFrmElements[intCntVal].focus()
					return false;
				}
			}
			
			if (strStringMin != "" &&  strStringMin!=null)
			{
				strValue = objFrmElements[intCntVal].value
				if (strValue.length < strStringMin)
				{
					alert('Please enter atleast ' + strStringMin + ' characters for ' + strFieldName )
					objFrmElements[intCntVal].focus()
					return false;
				}
			}
			
			
			if(strDateType !=null)
			{
			 if (strDateType.toLowerCase() == "dateless")
			 {
				strValue = objFrmElements[intCntVal].value
				var strDateGiven = getMMDDYYYY(strValue)
				var strCurrentDate = new Date()
				if(CheckforEqualDates(strDateGiven,strCurrentDate))
				{
					alert(strFieldName + ' should be before current date')
					objFrmElements[intCntVal].select()
					
					return false;
				}
				if (strDateGiven > strCurrentDate)
				{
					alert(strFieldName + ' should be before current date')
					objFrmElements[intCntVal].select()
					return false;
				}
				
			 }
			}

			if(strDateType !=null)
			{
			
			 if (strDateType.toLowerCase() == "datelessequal")
			 {
				strValue = objFrmElements[intCntVal].value
 				var strDateGiven = getMMDDYYYY(strValue)
                           
				var strCurrentDate = new Date()
				if(!CheckforEqualDates(strDateGiven,strCurrentDate))
				{
					if (strDateGiven > strCurrentDate)
					{
						alert(strFieldName + ' should be before current date or same as current date')
						objFrmElements[intCntVal].select()
						return false;
					}
				}				
			 }
			} 

			
			if(strDateType !=null)
			{
			
			 if (strDateType.toLowerCase() == "datemore")
			 {
				strValue = objFrmElements[intCntVal].value
				var strDateGiven = getMMDDYYYY(strValue)
				var strCurrentDate = new Date()
				if(CheckforEqualDates(strDateGiven,strCurrentDate))
				{
					alert(strFieldName + ' should be after current date')
					objFrmElements[intCntVal].select()
					return false;
				}

				if (strDateGiven < strCurrentDate)
				{
					alert(strFieldName + ' should be after current date')
					objFrmElements[intCntVal].select()
					return false;
				}
			  }	
			}

			
			if(strDateType !=null)
			{
			
			 if (strDateType.toLowerCase() == "datemoreequal")
			 {
				
				strValue = objFrmElements[intCntVal].value
				var strDateGiven = getMMDDYYYY(strValue)
				var strCurrentDate = new Date()
				if(!CheckforEqualDates(strDateGiven,strCurrentDate))
				{
					if (strDateGiven < strCurrentDate)
					{
						alert(strFieldName + ' should be after current date or same as current date')
						objFrmElements[intCntVal].select()
						return false;
					}
				}				
			  }
			}
			
			
			if (strMinDiff != "" && strMinDiff!=null)
			{
				strValue = objFrmElements[intCntVal].value
				var strDateGiven = getMMDDYYYY(strValue)
				var strYear = strDateGiven.getFullYear()
				var strCurrentDate = new Date()
				var strCurrentYear = strCurrentDate.getFullYear()
				if ((Number(strYear) + Number(strMinDiff)) >= Number(strCurrentYear))
				{
					alert('Minimum age should be ' + strMinDiff)
					objFrmElements[intCntVal].focus()
					return false;
					
				}
			}					

			
		   if(strDataType !=null)
		   {	
			strDataType = strDataType.toLowerCase()
			if(strDataType.indexOf("numeric") != -1)
			{
				strValue = objFrmElements[intCntVal].value
				
				if(isNaN(objFrmElements[intCntVal].value))
				{
					alert('Please enter a valid number for ' + strFieldName + '. Please enter only numeric values.Exclude special characters like $ & , (comma)')
					objFrmElements[intCntVal].select()
					objFrmElements[intCntVal].focus()
					return false;
				}	
				else if ((strValue.indexOf("e") != -1) || (strValue.indexOf("E") != -1))
				{
					alert('Please enter a valid number for ' + strFieldName + '.')
					objFrmElements[intCntVal].select()
					objFrmElements[intCntVal].focus()
					return false;
				}				
				else
				{
					if((strDataType.toLowerCase() == "positivenumeric"))
					{
						if((!isBlank(objFrmElements[intCntVal].value)) && (objFrmElements[intCntVal].value < 0))
						{
							alert(strFieldName + ' should be positive value.')
							objFrmElements[intCntVal].select()
							objFrmElements[intCntVal].focus()
							return false;
						}
					}	
					if((strDataType.toLowerCase() == "positivenumericinteger"))
					{
						var intCount=0;
						
						for (intCnt =0;intCnt<strValue.length;intCnt++)
						{
						  
						   
							   if((strValue.charAt(intCnt) ==".")||(strValue.charAt(intCnt)=="+")||(strValue.charAt(intCnt) =="-"))
							   {
							      intCount=intCount+1;
							      break;  
							   }
							   else
							   {
							     intCount=0;
							     
							   }
												
						

                        }
						

						
						 if(intCount !=0)
						  {
						      alert(strFieldName + ' should be positive numeric integer.')
							objFrmElements[intCntVal].select()
							objFrmElements[intCntVal].focus()
							return false;
						  } 
						
						if(strValue < 0)
						{
							alert(strFieldName + ' cannot be less than ZERO.')
							objFrmElements[intCntVal].select()
							objFrmElements[intCntVal].focus()
							return false;
						}
					  
					}	
				}
			 } 
		   }
			
			
		 
			if(intMaxLength !=null)
			{
			
			
			if((strElementType == "textarea"))
			{
				if(isNaN(intMaxLength))
				{
					intMaxLength = 250;
				}
				strTextAreaValue = objFrmElements[intCntVal].value
				if(strTextAreaValue.length > intMaxLength)
				{
					alert(strFieldName + ' cannot exceed ' + intMaxLength + ' characters.\nCurrent Length = ' + strTextAreaValue.length)
					objFrmElements[intCntVal].focus()
					return false;
				}
			}
         }
			
		  if(strDataType !=null)
		  {	
			strDataType = strDataType.toLowerCase()
			
			if((strDataType == "email") && (isBlank(objFrmElements[intCntVal].value) == false))
			{
				
				var at="@"
				var dot="."
				var strValue = objFrmElements[intCntVal].value
				var lat=strValue.indexOf(at)
				var lstr=strValue.length
				var ldot=strValue.indexOf(dot)
				
				if (strValue.indexOf(at)==-1){
				   alert("Please enter a valid E-mail ID")
				   objFrmElements[intCntVal].focus()
				   return false
				}

				if (strValue.indexOf(at)==-1 || strValue.indexOf(at)==0 || strValue.indexOf(at)==strValue)
				{
				   alert("Please enter a valid E-mail ID")
				   objFrmElements[intCntVal].focus()
				   return false
				}
		
				if (strValue.indexOf(dot)==-1 ||strValue.indexOf(dot)==0 ||strValue.indexOf(dot)==strValue)
				{
				    alert("Please enter a valid E-mail ID")
				    objFrmElements[intCntVal].focus()
				    return false
				}
		
				 if (strValue.indexOf(at,(lat+1))!=-1){
				    alert("Please enter a valid E-mail ID")
				    objFrmElements[intCntVal].focus()
				    return false
				 }
		
				if (strValue.substring(lat-1,lat)==dot || strValue.substring(lat+1,lat+2)==dot)
				{
				    alert("Please enter a valid E-mail ID")
				    objFrmElements[intCntVal].focus()
				    return false
				 }

				 if (strValue.indexOf(dot,(lat+2))==-1){
				    alert("Please enter a valid E-mail ID")
				    objFrmElements[intCntVal].focus()
				    return false
				 }
				
				 if (strValue.indexOf(" ")!=-1){
				    alert("Please enter a valid E-mail ID")
				    objFrmElements[intCntVal].focus()
				    return false
				 }
		
		 				
				
				
			} 
		  }	
			
		}

		if((strElementType == "select-one"))
		{
			if(objFrmElements[intCntVal].name != "rejectAction")
			{
				var strFieldName = unescape(objFrmElements[intCntVal].FieldName)
				var strFieldType = unescape(objFrmElements[intCntVal].FieldType)
				if(strFieldType.toLowerCase() == "mandatory")
				{
					if((objFrmElements[intCntVal].value == "0") || (objFrmElements[intCntVal].value == "Select") ||(objFrmElements[intCntVal].value == ""))
					{
						alert('Please select the ' + strFieldName + '.')

						objFrmElements[intCntVal].focus()
						return false;
					}		
				}	
			}
		 }
		
		
		if(strDeciPart != null)
			{
				if(strDeciPart.toLowerCase() == "decimal")
				{
					 if(!isBlank(objFrmElements[intCntVal].value))
				 	 {
				     	
				     strFieldName = objFrmElements[intCntVal].getAttribute('FieldName')
				    	
				     	strAmount = objFrmElements[intCntVal].value 

				    	
				    	if(strAmount.indexOf(".") != -1)
				    	{
				    		strDecimalAmt = strAmount.substring(strAmount.indexOf(".")+1); 
				    		if(strDecimalAmt.length > 1)
				    		{
						    alert('Please enter a valid '+ strFieldName +'(Number of digits after the decimal cannot exceed one.)')
								objFrmElements[intCntVal].focus();
								objFrmElements[intCntVal].select();
								return false;
				    		
				    		}
				    	}
						
						intMaxAmount = Math.pow(10,intMaxLength-1)
						intMaxAmount -=0.1;
							
						if(Number(strAmount) > intMaxAmount)
						{
							alert(strFieldName +' cannot be greater than'+intMaxAmount)
							objFrmElements[intCntVal].focus();
								objFrmElements[intCntVal].select();
							return false;
						}	
				     } 	
			   	}
			}
		
		
		if(strDynamic != null)
			{
				if(strDynamic.toLowerCase() == "dynamic")
				{
					 if(!isBlank(objFrmElements[intCntVal].value))
				 	 {
				     	
				     	var strFieldName = unescape(objFrmElements[intCntVal].FieldName)
				     	strAmount = objFrmElements[intCntVal].value 

				    	intDeciIndex = -1;
					intDeciIndex =strAmount.indexOf(".")  
		
						if(intDeciIndex+3 < strAmount.length )
						{
							if(intDeciIndex != -1)
							{
								alert('Please enter a valid '+ strFieldName +'(Number of digits after the decimal cannot exceed two.)')
								objFrmElements[intCntVal].focus();
								objFrmElements[intCntVal].select();
								return false;
							}
						}
						intMaxAmount = Math.pow(10,intMaxLength-3)
						intMaxAmount -=0.01;
							
						if(Number(strAmount) > intMaxAmount)
						{
							
							alert(strFieldName + 'cannot be greater than '+intMaxAmount)
							objFrmElements[intCntVal].focus();
								objFrmElements[intCntVal].select();
							return false;
						}	
				     } 	
			   	}
			}
		

	}
	return true;
}