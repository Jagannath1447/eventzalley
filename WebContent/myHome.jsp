<!DOCTYPE html>
<%@page import="com.uthkrushta.mybos.code.bean.CertificateTypeBean"%>
<%@page import="com.uthkrushta.basis.bean.MessageBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.uthkrushta.mybos.master.controller.UserMasterController"%>
<%@page import="com.uthkrushta.basis.constants.ISessionAttributes"%>
<%@page import="org.ajaxanywhere.AAUtils"%>
<%@page import="com.uthkrushta.mybos.transaction.bean.view.GroupLeaderViewBean"%>
<%@page import="com.uthkrushta.mybos.configuration.bean.GlobalUrlSettingBean"%>
<%@page import="org.hibernate.HibernateException"%>
<%@page import="org.hibernate.transform.Transformers"%>
<%@page import="org.hibernate.Query"%>
<%@page import="com.uthkrushta.basis.util.HibernateUtil"%>
<%@page import="org.hibernate.Session"%>
<%@page
	import="com.uthkrushta.mybos.transaction.bean.view.MemberRunViewBean"%>
<%@page import="com.ibm.icu.text.DateFormat"%>
<%@page
	import="com.uthkrushta.mybos.configuration.bean.EventPhotoUploadBean"%>
<%@page
	import="com.uthkrushta.mybos.transaction.bean.EventLapTimeResultBean"%>
<%@page import="java.util.List"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeLapMasterBean"%>
<%@page import="java.awt.image.DataBuffer"%>
<%@page
	import="com.uthkrushta.mybos.transaction.bean.view.MemberRecentEventView"%>
<%@page import="com.uthkrushta.mybos.transaction.bean.EventResultBean"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventTypeMasterBean"%>
<%@page import="com.ibm.icu.text.SimpleDateFormat"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<%@page import="java.util.Date"%>
<%@page import="com.uthkrushta.mybos.master.bean.EventMasterBean"%>
<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="com.uthkrushta.utils.DBUtil"%>
<%@page import="com.uthkrushta.mybos.master.bean.UserMasterBean"%>
<%@page import="org.ajaxanywhere.AAUtils"%>
<%@ taglib prefix="aa" uri="http://ajaxanywhere.sourceforge.net/"%>
<html>

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<title>Eventzalley</title>
<!-- Favicon-->
<link rel="icon" href="favicon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext"
	rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet" type="text/css">

<!-- Bootstrap Core Css -->
<link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

<!-- Waves Effect Css -->
<link href="plugins/node-waves/waves.css" rel="stylesheet" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<!-- Animation Css -->
<link href="plugins/animate-css/animate.css" rel="stylesheet" />


<!-- Sweetalert Css -->
<!--  <link href="plugins/sweetalert/sweetalert.css" rel="stylesheet" /> -->

<!-- Light Gallery Plugin Css -->
<link href="dist/css/lightgallery.css" rel="stylesheet">

<!-- Morris Chart Css-->
<link href="plugins/morrisjs/morris.css" rel="stylesheet" />

<!-- Custom Css -->
<link href="css/style.css" rel="stylesheet">

<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="css/themes/all-themes.css" rel="stylesheet" />
<link href="Style/default.css"
	rel="stylesheet" />


<style type="text/css">
.wrapper {
	padding: 100px;
}

/* .image--cover2 {
  width: 150px;
  height: 150px;
  border-radius: 50%;
  

  object-fit: cover;
  object-position: center right;
}  */
.center {
	display: block;
	margin-left: auto;
	margin-right: auto;
	width: 90%;
}

.AdminHome {
	background-image: url("Images/admin_home_bg.jpg");
	background-size: contain;
	background-repeat: no-repeat;
	object-fit: contain;
	position: fixed;
	width: 100%;
	height: 100%;
}
</style>

<script type="text/javascript">

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function saveLogin(){
	
	
	var mob = $("#<%=ILabelConstants.primaryNumber%>").val();
	var password = $("#<%=ILabelConstants.password%>").val();
	
	
	var phoneno = /^[0]?[789]\d{9}$/; 
	
	if (!mob.match(phoneno)) {
        alert("Enter Valid Mobile Number!!");
        document.forms["primaryNumber"]["<%=ILabelConstants.primaryNumber %>"].focus();
        return ;
    }
	
	else if(password == ""){
		
		 alert('Please Enter Password');
	}else{
		
		document.forms[0].hdnUserAction.value = "SAVE_NEW_MEMBER"; 
		if(null!= document.forms[0].hdnAjaxSubmit && document.forms[0].hdnAjaxSubmit.value.toLowerCase()== 'true')	
 		{	
			$(".loginSignUp").hide();
			$(".loginEnable").hide();
 			ajaxAnywhere.submitAJAX();
 		}
 		else
 		{
 			document.forms[0].submit();
 		} 
	}
	
	
	
	
}


$(function() {
	
	$(".loginEnable").hide();
	$(".loginSignUp").hide();
});

function onPermission(){
	document.getElementById("primaryNumber").value = "";
	document.getElementById("password").value = "";
	$(".loginSignUp").show();
	
	
	
}
function onChangeMember(leader){
	
	var newMember = $("#<%=ILabelConstants.GroupMember%> option:selected").val();
	if( newMember != leader){
		$("#newMember").val(newMember);
		$(".loginEnable").show();
		$(".loginSignUp").hide();
	}else{
		$(".loginEnable").hide();
		$(".loginSignUp").hide();
	}
	
	
}



	
</script>



</head>
<%
	long userID = 0;
	//String strImageContextPath = "/EventzAlley/servlet1/";
	long lngCertificateType = 1;
	String strChartData = "";
	String userRunTime = "";
	String avgTime = "";
	List lstRunData = null;
	List lstEventTypeLap = null;
	String strUserAction = "GET";
	long newMemberID = 0;
	String strStatusText = "";
	String strStatusStyle = "";
	
	GlobalUrlSettingBean objGISB = (GlobalUrlSettingBean) DBUtil.getRecord(IUMCConstants.BN_GLOBAL_IMAGE,
			IUMCConstants.GET_ACTIVE_ROWS, "");
	if (objGISB == null) {
		objGISB = new GlobalUrlSettingBean();
	}
	
	ArrayList<MessageBean> arrMsg = null; 
	MessageBean objMsg;
	
	
	CertificateTypeBean objCTB = new CertificateTypeBean();
	UserMasterController objGC = new UserMasterController();
	MemberRecentEventView objMREV = new MemberRecentEventView();
	EventTypeLapMasterBean objETLMB = new EventTypeLapMasterBean();
	UserMasterBean objUMB = new UserMasterBean();
	MemberRunViewBean objMRVB = new MemberRunViewBean();
	EventPhotoUploadBean objEPUB = new EventPhotoUploadBean();
	EventPhotoUploadBean objEPUB2 = new EventPhotoUploadBean();
	/* long userID = ((Long) session.getAttribute("LOGIN_ID")).longValue(); */
	Long memberID = (Long) session.getAttribute("LOGIN_ID");

	String UserName = objUMB.getStrFirstName();

	if (null != memberID) {
		userID = memberID.longValue();
	}
	
	objMREV = (MemberRecentEventView) DBUtil.getRecord(ILabelConstants.TR_MEMBER_RECENT_VIEW_BN,
			" lngUserID = " + userID, "dtEventStartDate DESC");

	if (objMREV == null) {

		objMREV = new MemberRecentEventView();
	}
	
	List lstGroupLeaderMembers = (List)DBUtil.getRecords(ILabelConstants.BN_GROUP_LEADER_VIEW, " lngGroupLeaderID = "+userID+" AND lngEventID = "+objMREV.getLngEventID(), "strFirstName");
	
	
	String strCertificationRpt = "";		 //String strCertificationRpt = "Rpt/eventCompletionCertificate.rpt";
	
	
	if (AAUtils.isAjaxRequest(request)) {
		strUserAction = WebUtil.processString(request.getParameter(ILabelConstants.hdnUserAction));
		objGC.doAction(request, response);
		
		lstGroupLeaderMembers = (List)DBUtil.getRecords(ILabelConstants.BN_GROUP_LEADER_VIEW, " lngGroupLeaderID = "+userID+" AND lngEventID = "+objMREV.getLngEventID(), "strFirstName");
	
	}
	
	
	
	

	List lstLastEventPhotos = (List) DBUtil.getRecords(ILabelConstants.BN_EVENT_PHOTO_UPLOAD,
			IUMCConstants.GET_ACTIVE_ROWS + " AND intPublish = 1 AND lngEventID = " + objMREV.getLngEventID()
					+ " AND FIND_IN_SET( '" + objMREV.getStrBIBNO() + "', strBibNoEntry) > 0",
			"");

	objEPUB = (EventPhotoUploadBean) DBUtil.getRecord(ILabelConstants.BN_EVENT_PHOTO_UPLOAD,
			IUMCConstants.GET_ACTIVE_ROWS + " AND intPublish = 1 AND lngEventID = " + objMREV.getLngEventID()
					+ " AND strBibNoEntry LIKE '" + objMREV.getStrBIBNO() + "'",
			"");

	if (objEPUB == null) {
		objEPUB = new EventPhotoUploadBean();
	}

	String strWhereClause = "SELECT i.* FROM (SELECT @p1/*'*/:=/*'*/ " + userID
			+ ") parm1,(SELECT @p2/*'*/:=/*'*/ " + objMREV.getLngEventID() + ") parm2,(SELECT @p3/*'*/:=/*'*/ "
			+ objMREV.getLngEventTypeID() + ") parm3, tr_member_run_view i ";

	MemberRunViewBean objSTOCTVB = new MemberRunViewBean();
	Session session1 = null;
	try {
		session1 = HibernateUtil.getSessionFactory().openSession();

		Query query = session1.createSQLQuery(strWhereClause);

		lstRunData = query.setResultTransformer(Transformers.aliasToBean(objSTOCTVB.getClass())).list();
	} catch (HibernateException e) {
		e.printStackTrace();
	} finally {
		if (null != session1) {
			session1.flush();
			session1.close();
		}
	}

	if (objMREV != null) {
		if( objMREV.getLngIsTimed() == 1){
		lstEventTypeLap = (List) DBUtil.getRecords(ILabelConstants.BN_EVENT_TYPE_LAP,
				IUMCConstants.GET_ACTIVE_ROWS + " AND lngEventTypeID = " + objMREV.getLngEventTypeID(),
				"lngID");
		}
		
		lngCertificateType = objMREV.getLngCertificateID();
		if (lngCertificateType > 0) {
			objCTB = (CertificateTypeBean) DBUtil.getRecord(ILabelConstants.BN_CERTIFICATE_TYPE, IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+lngCertificateType, ""); 
			if(objCTB == null){
				objCTB = new CertificateTypeBean();
			}else{
				strCertificationRpt = "Rpt/"+objCTB.getStrCertificatePath();
				
				System.out.println("strCertificationRpt ========="+strCertificationRpt);
			}
			
		}  else {
			strCertificationRpt = "";
		} 
	}

	objUMB = (UserMasterBean) DBUtil.getRecord(ILabelConstants.BN_USER_MASTER_BEAN, "lngID = " + userID, "");
	if (objUMB == null) {
		objUMB = new UserMasterBean();
	}
	
	
	arrMsg = objGC.getArrMsg();		//Getting the messages to display
	if(null!= arrMsg && arrMsg.size() > 0)
	{
		objMsg = MessageBean.getLeadingMessage(arrMsg);
		if(objMsg != null)
		{
	strStatusText = objMsg.getStrMsgText();
	strStatusStyle = objMsg.getStrMsgType();
		}
	}
	
	
	
	AAUtils.addZonesToRefresh(request, "memberRefresh,RefreshSelectMember");
%>



<body class="theme-blue " style="background-color: #e9e9e9;">

	<form method="post">




		<jsp:include page="MyIncludes/incPageHeader.jsp" flush="true">
			<jsp:param value="" name="CONTEXT_PATH" />
		</jsp:include>
		<!-- #Top Bar -->




		<jsp:include page="MyIncludes/incLeftPanel.jsp" flush="true">
			<jsp:param value="" name="CONTEXT_PATH" />
		</jsp:include>


		<%
			if (objUMB.getLngRoleID() == IUMCConstants.MEMBER_ROLE) {
		%>
		
		
		
	<%
	if(lstGroupLeaderMembers != null ){
	if(lstGroupLeaderMembers.size()>1  ){
	%>
		<section class="content">
		
		<jsp:include page="MyIncludes/incPageHeaderInfo.jsp" flush="true">
					 
					    <jsp:param name="STATUS_STYLE" value="<%=strStatusStyle%>" />
					    <jsp:param name="STATUS_TEXT" value="<%=strStatusText%>" />
					</jsp:include> 
		
			<div class="container-fluid">
				<div class="row clearfix">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="row body" style="padding-bottom: 0px;">
							
							<div class="col-lg-2 col-md-2 col-sm-5 col-xs-5">
							Select Member :
							</div>
							<aa:zone name="RefreshSelectMember" skipIfNotIncluded="true" >
							<div class="col-lg-5 col-md-5 col-sm-7 col-xs-7" >
							<select   class="form-control show-tick"  name="<%=ILabelConstants.GroupMember%>" id="<%=ILabelConstants.GroupMember%>"
							onChange="onChangeMember(<%=userID%>)"> <!-- onChange="onSelectMember()" -->
							 <%
                          if(lstGroupLeaderMembers != null){
                        	for(int i=0;i<lstGroupLeaderMembers.size();i++)
                        	{
                        		GroupLeaderViewBean objGLVB = (GroupLeaderViewBean) lstGroupLeaderMembers.get(i);
                        	
                        		
                        	
                        	  %> 
                        	  <option  value="<%=objGLVB.getLngMemberID()%>"<% if(objGLVB.getLngMemberID() == userID ) {%> selected <%} %> ><%= objGLVB.getStrFirstName()+" "+objGLVB.getStrLastName() %></option>
                        	  
                        	  
                        	  
                        <% }
                          }
                          %> 
                          </select>
							</div>
							</aa:zone>
							
							<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12" style="margin-bottom: 25px;">
							<input type="button" value="Enable Login"  class="btn btn-primary loginEnable"
									  onClick="onPermission()"/> 
							</div>
							
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 loginSignUp">
							<div class="form-group form-float" style="margin-top: 15px;">

											<div class="form-line">
												<input type="text" class="form-control"
													name="<%=ILabelConstants.primaryNumber%>"
													id="<%=ILabelConstants.primaryNumber%>"
													
													style="background-color: white"
													onkeypress="return isNumber(event)"> <label
													class="form-label" >Mobile
													Number *</label>

											</div>
										</div>
							
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 loginSignUp">

										<div class="form-group form-float" style="margin-top: 15px;padding-left: 15px;padding-right: 20px;">

											<div class="form-line">
												<input type="password" class="form-control"
													name="<%=ILabelConstants.password%>"
													id="<%=ILabelConstants.password%>"
													
													style="background-color: white" onblur="checkPass()"> <label
													class="form-label">Password*</label>

											</div>
										</div>
						</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 loginSignUp" 
							style="display: block;margin-bottom: 0px;margin-top: 20px;padding-left: 0px;">

										<input type="button" value="Save"  class="btn btn-primary loginEnable"
									  onClick="saveLogin()"/> 
						</div>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" name="<%=ILabelConstants.newMember%>" id="<%=ILabelConstants.newMember%>">
			</div>
			
		</section>


<%} }%>
		
		
		
		
	<%
	if(lstGroupLeaderMembers != null ){
	if(lstGroupLeaderMembers.size()>1  ){
	%>	
		<section class="content" style="
    margin-top: 0px;
"><%}else{%><section class="content"><% } } else{ %><section class="content"><%} %>
<aa:zone name="memberRefresh" skipIfNotIncluded="true" >
			<div class="container-fluid">
				<div class="row clearfix">
				
				<% if(objMREV.getLngEventID() > 0) { %>
				
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="header">
								<!-- style="background-color: #70c4cc;" -->
								<h2>Congratulations on the good run! Have a look at your
									achievements below!</h2>
							</div>

							<div class="row body" style="padding-bottom: 0px;">

								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
									style="padding-bottom: 0px;">

									<div class="menu-info col-lg-6 col-md-6 col-sm-12 col-xs-12"
										style="margin-bottom: 20px">
										<h4>
											Event Name : <span style="color: #5f9a32"> <%=WebUtil.processString(objMREV.getStrEventName())%></span>
										</h4>

									</div>
									<div class="menu-info col-lg-6 col-md-6 col-sm-12 col-xs-12"
										style="margin-bottom: 20px">
										<h4>
											Competition Name : <span style="color: #5f9a32"> <%=WebUtil.processString(objMREV.getStrEventTypeName())%></span>
										</h4>

										<!-- <h4 style="color:#5f9a32;margin-top:5px">  -->

									</div>
								</div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<%if(objMREV.getLngIsTimed() ==1 ) { %>
									<div class="menu-info col-lg-4 col-md-4 col-sm-12 col-xs-12" style="margin-bottom: 20px">
									<%} else { %>
										<div class="menu-info col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-bottom: 20px">
										<%} %>
										<h4>
											BIB Number : <span style="color: #5f9a32"> <%=WebUtil.processString(objMREV.getStrBIBNO())%></span>
										</h4>
										<!-- <p>
                                 <h4 style="color:#5f9a32;margin-top:5px">  -->
										<!--  </p> -->
									</div>
<%if(objMREV.getLngIsTimed() ==1 ) { %>

									<div class="menu-info col-lg-4 col-md-4 col-sm-12 col-xs-12"
										style="margin-bottom: 20px">
										<h4>
											Gun Time : <span style="color: #5f9a32"> <%=WebUtil.processString(objMREV.getStrGun())%>
											</span>
										</h4>

									</div>
									<div class="menu-info col-lg-4 col-md-4 col-sm-12 col-xs-12"
										style="margin-bottom: 20px">
										<h4>
											Category Rank : <span style="color: #5f9a32"> <%
 	if (objMREV.getLngRank() != 0) {
 %>#<%=objMREV.getLngRank()%>
												<%
													}
												%>
											</span>
										</h4>
										</p>
									</div>
									<%} else {%>
									<div class="menu-info col-lg-4 col-md-4 col-sm-12 col-xs-12"
										style="margin-bottom: 20px">
									
									<input type="button" class="btn bg-teal waves-effect waves-effect m-r-20" value="Download Certificate" <%if(strCertificationRpt == ""){ %>disabled<%} %>
													onClick="openCertificate('<%=objMREV.getLngUserID()%>','<%=objMREV.getLngEventID()%>','<%=objMREV.getLngEventTypeID()%>','<%=strCertificationRpt%>')">
									
									</div>
									<%} %>
								</div>
							</div>
						</div>
					</div>

				</div>
				
				<%} %>
				
				
				
				<%
								if (lstRunData != null && objMREV.getLngIsTimed() ==1) {
							%>
				<div class="row clearfix row-eq-height">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
						<div class="card"
							style="margin-bottom: 30px; padding-bottom: 20px;">
							<div class="header">
								<!-- style="background-color: #70c4cc;" -->

								<h2>Home Photo</h2>
							</div>
							<div class="body"
								style="margin-bottom: 20px; padding-bottom: 0px;">
								<!-- background-color: #afecde; -->

								<div class="row clearfix">


									<%
										if (objEPUB != null && null != objEPUB.getStrImagePath()
													&& objEPUB.getStrImagePath().trim().length() > 0) {
									%>
									<img
										src="<%=objGISB.getStrName()%>event-image/thumbnail/<%=objMREV.getLngEventID()%>/<%=objEPUB.getStrImagePath()%>"
										alt="" class="image--cover2 center" width="80%" height="60%" />


									<%
										} else {
									%>
									<img src="Images/defaultImage.jpg" alt=""
										class="image--cover2 center" width="80%" height="60%" />
									<%
										}
									%>

								</div>
							</div>
						</div>
					</div>


	
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
						


						<div class="card" style="min-height: 100%">
							<div class="header">
								<h2>Last Run Chart</h2>

							</div>
							
							<div class="body"
								style="padding-bottom: 75px; padding-top: 50px;">
								<canvas id="line_chart" height="150"></canvas>
								<input type="hidden" value="0" class="distance"> <input
									type="hidden" value="0" class="userRunData"> <input
									type="hidden" value="0" class="avgRunData">
								<%
									for (int i = 0; i < lstRunData.size(); i++) {
												objMRVB = (MemberRunViewBean) lstRunData.get(i);

												if (objMRVB == null) {
													objMRVB = new MemberRunViewBean();
												}

												String time = WebUtil.formatDate(objMRVB.getTime(), IUMCConstants.TIME_FORMAT_24);
												String time2 = WebUtil.processString(objMRVB.getCategory_avg_time());

												int hr = Integer.parseInt(time.substring(0, 2));
												int min = Integer.parseInt(time.substring(3, 5));
												int sec = Integer.parseInt(time.substring(6, 8));
												System.out.println(" Time = " + hr + " and " + min + " and " + sec);
												userRunTime = (hr * 60) + min + "." + sec;

												int hr2 = Integer.parseInt(time2.substring(0, 2));
												int min2 = Integer.parseInt(time2.substring(3, 5));
												int sec2 = Integer.parseInt(time2.substring(6, 8));
												System.out.println(" Time = " + hr2 + " and " + min2 + " and " + sec2);
												avgTime = (hr2 * 60) + min2 + "." + sec2;
								%>
								<input type="hidden" class="distance"
									value="<%=WebUtil.processString(objMRVB.getDistance())%>">
								<input type="hidden" value="<%=userRunTime%>"
									class="userRunTime"> <input type="hidden"
									value="<%=avgTime%>" class="avgRunData">
								<%
									}
								%>


							</div>
							


						</div>

					</div>
				</div>
				<%
								}
							%>
			</div>
			
			
			<% if(objMREV.getLngEventID() > 0) { %>
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="card">
							<div class="header">
								<h2>Your Run Photos</h2>
							</div>





							<%
								if (lstLastEventPhotos != null) {
							%>
							<div class="body">

								<!-- <div id="aniimated-thumbnials" class="list-unstyled row clearfix"> -->
								<div id="aniimated-thumbnials" class="list-unstyled row clearfix">
								<div class="list-unstyled row clearfix">
									<%
										for (int i = 0; i < lstLastEventPhotos.size(); i++) {
													objEPUB2 = (EventPhotoUploadBean) lstLastEventPhotos.get(i);
									%>

									

									<div id="lightgallery" class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="margin-left: 15px;margin-right: 15px;margin-top: 15px;">
									          
						                <div   
						                >
						                    <a  href="<%=objGISB.getStrName()%>event-image/<%=objMREV.getLngEventID()%>/<%=objEPUB2.getStrImagePath()%>" 
						                    data-facebook-share-url="<%=objGISB.getStrName()%>event-image/thumbnail/<%=objMREV.getLngEventID()%>/<%=objEPUB2.getStrImagePath()%>" 
						                    download
						                    data-sub-html="<h4><%=objEPUB2.getStrImageDescription()%></h4>">
						                        <img class="img-responsive imgDownload" alt="<%=objGISB.getStrName()%>event-image/<%=objMREV.getLngEventID()%>/<%=objEPUB2.getStrImagePath()%>" 
						                        src=" <%=objGISB.getStrName()%>event-image/thumbnail/<%=objMREV.getLngEventID()%>/<%=objEPUB2.getStrImagePath()%>"
						                         style="width: 200px; height: 150px;margin-bottom: 5px;">
						                    </a>
						                </div>
						               
						            </div>
      
								<% } %>
      
								</div>
							</div>
							<%
							
							
										
									
								
								} else {
							%>


							<div class="body" style="font-size: 16px;">No Photos</div>

							<%
								}
							%>
						</div>
					</div>
				</div>
			</div>
			</div>
			<%} %>

			<div class="container-fluid">
				<div class="row clearfix ">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


						<div class="body" style="margin-bottom: 20px; padding-bottom: 0px">

							<%
								if (lstEventTypeLap != null && objMREV.getLngIsTimed() ==1) {

										if (null != objMREV.getStrPace()) {
											objMREV.setStrPace(objMREV.getStrPace().substring(3));
										}
							%>
							<div class="card">
								<div class="header" >
									<div class="row">
										<h4 class="col-lg-4 col-sm-12 col-xs-12">Your Run</h4>
										<h4 class="col-lg-4 col-sm-6 col-xs-6">
											Rank :
											<%
											if (objMREV.getLngRank() != 0) {
										%>#<%=objMREV.getLngRank()%>
											<%
												}
											%>
										</h4>
										<h4 class="col-lg-4 col-sm-6 col-xs-6"
											style="color: #5f9a32;">
											BIB:
											<%=WebUtil.processString(objMREV.getStrBIBNO())%>
										</h4>
										<!-- float:right" align="right" -->
									</div>
								</div>



								<div class="body">
									<div role="tabpanel" class="tab-pane fade in active" id="home">
										<div class="row col-lg-12">
											<div class="col-lg-4">
												Event Name :
												<%=WebUtil.processString(objMREV.getStrEventName())%></div>
											<div class="col-lg-4">
												Competition Name :
												<%=WebUtil.processString(objMREV.getStrEventTypeName())%></div>
											<div class="col-lg-4">
												<input type="button" class="btn bg-teal waves-effect waves-effect m-r-20" value="Download Certificate" <%if(strCertificationRpt == ""){ %>disabled<%} %>
													onClick="openCertificate('<%=objMREV.getLngUserID()%>','<%=objMREV.getLngEventID()%>','<%=objMREV.getLngEventTypeID()%>','<%=strCertificationRpt%>')">
											</div>
										</div>
										<div class="row col-lg-12">
											<div class="col-lg-4"
												style="color: #006699; font-weight: bold">
												CHIP TIME :
												<%=WebUtil.processString(objMREV.getStrChip())%></div>
											<div class="col-lg-4"
												style="color: #006699; font-weight: bold">
												GUN TIME :
												<%=WebUtil.processString(objMREV.getStrGun())%></div>
											<div class="col-lg-4"
												style="color: #006699; font-weight: bold">
												AVG PACE :
												<%=WebUtil.processString(objMREV.getStrPace())%>
												<font style="font-weight: normal">(mm:ss)</font>
											</div>

										</div>

										<table class="table table-hover"
											style="margin-left: 15px; width: 96%">
											<thead>
												<tr>
													<th>#</th>
													<th>Split</th>
													<th>Your Run Time</th>
													<th>Category Avg Time</th>

												</tr>
											</thead>
											<tbody>

												<%
													if (lstRunData != null) {
																for (int i = 0; i < lstRunData.size(); i++) {
																	objMRVB = (MemberRunViewBean) lstRunData.get(i);

																	if (objMRVB == null) {
																		objMRVB = new MemberRunViewBean();
																	}

																	/* strChartData += "[{'distance': '" + WebUtil.processString(objMRVB.getLap_name())
																			+ "', 'licensed': " + objMRVB.getTime() + ",'sorned': "+objMRVB.getCategory_avg_time()+"},"; */
												%>

												<tr>
													<td scope="row"><%=i + 1%></td>
													<td><%=WebUtil.processString(objMRVB.getLap_name())%></td>
													<td><%=WebUtil.formatDate(objMRVB.getTime(), IUMCConstants.TIME_FORMAT_24)%></td>
													<td><%=objMRVB.getCategory_avg_time()%></td>
													<td></td>

												</tr>
												<%
													}
															}

															strChartData += "]";
															//	out.print("strChartData"+ strChartData);
												%>
											</tbody>
										</table>
									</div>
								</div>

							</div>
							<%
								}
							%>

							<%-- 	<%else{
                                		<div class="body" style="font-size:16px; " >No Records	</div>
                                		
                                	<%}
                                	
                                	
                                                                %> --%>
							<!-- </div> -->

							<!-- Area Chart -->

							<!-- #END# Line Chart -->



						</div>


					</div>

				</div>
			</div>




			<div class="container-fluid">
				<div class="row clearfix ">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


						<div class="card"
							style="margin-bottom: 30px; padding-bottom: 20px">
							<div class="header">
								<!-- style="background-color: #70c4cc;" -->
								<h2>Rejuvenate and Recovery Tips</h2>
							</div>

							<div class="body"
								style="margin-bottom: 20px; padding-bottom: 0px;">
								<!-- background-color: #afecde; -->
								<ul>
									<li>Stretch your legs</li>
									<li>Do a short recovery run</li>
									<li>Hydrate with Fresh Fruit Juices</li>
									<li>Have a nutritious meal</li>
									<li>Get a Good night Sleep</li>
									<li>Consult a Physiotherapist if aches and pains persist</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="container-fluid">
				<div class="row clearfix ">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


						<div class="card"
							style="margin-bottom: 30px; padding-bottom: 20px">
							<div class="header">
								<!-- style="background-color: #70c4cc;" -->
								<h2>Register for these upcoming events and keep running!</h2>
							</div>

							<div class="body"
								style="margin-bottom: 20px; padding-bottom: 0px;">
								<!-- background-color: #afecde; -->
								<div class="row clearfix ">
									<!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<a
											href="https://www.eventzalley.com/event/skin-bank-walkathon/"
											target="_blank"> <img
											src="Images/UpcomingEvent/upcoming-event-1.png" alt=""
											class="image--cover2 center" width="80%" height="60%" />
										</a>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<a
											href="https://www.eventzalley.com/event/mcr-4th-edition-2019/"
											target="_blank"> <img
											src="Images/UpcomingEvent/upcoming-event-2.png" alt=""
											class="image--cover2 center" width="80%" height="60%" />
										</a>
									</div> -->
									
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<a
											href="https://www.eventzalley.com/event/lomo-run-club/"
											target="_blank"> <img
											src="https://www.eventzalley.com/wp-content/uploads/2017/03/1200x600a.jpg" alt=""
											class="image--cover2 center" width="80%" height="60%" />
										</a>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<a
											href="https://www.eventzalley.com/event/bengaluru-ride-100/"
											target="_blank"> <img
											src="https://www.eventzalley.com/wp-content/uploads/2018/12/Cycle-1200x600.jpg" alt=""
											class="image--cover2 center" width="80%" height="60%" />
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>








</aa:zone>
		</section>
		

		<jsp:include page="MyIncludes/incPageVariables.jsp" flush="true">
	                   <jsp:param name="USER_ACTION" value="<%=strUserAction%>" />
   		               <jsp:param name="IS_AJAX_SUBMIT" value="true" />
   	                   </jsp:include>

		<%
			} else {
		%>

		<section class="content AdminHome"></section>

		<%
			}
		%>


		<jsp:include page="MyIncludes/incBottomPageReferences.jsp"
			flush="true">
			<jsp:param value="" name="CONTEXT_PATH" />
		</jsp:include>
		<!-- Jquery Core Js -->
		<script src="plugins/jquery/jquery.min.js"></script>

		<!-- Bootstrap Core Js -->
		<script src="plugins/bootstrap/js/bootstrap.js"></script>

		<!-- Select Plugin Js -->
		<!--    <script src="plugins/bootstrap-select/js/bootstrap-select.js"></script> -->

		<!-- Slimscroll Plugin Js -->
		<script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

		<!-- Waves Effect Plugin Js -->
		<script src="plugins/node-waves/waves.js"></script>

		<!-- SweetAlert Plugin Js -->
		<!--  <script src="plugins/sweetalert/sweetalert.min.js"></script> -->
		<!-- dialogs Sweet Alert  -->
		<!-- <script src="js/pages/ui/dialogs.js"></script> -->

		<!-- Jquery CountTo Plugin Js -->
		<script src="plugins/jquery-countto/jquery.countTo.js"></script>

		<!-- Morris Plugin Js -->
		<script src="plugins/raphael/raphael.min.js"></script>
		<script src="plugins/morrisjs/morris.js"></script>


		<!-- ChartJs -->
		<script src="plugins/chartjs/Chart.bundle.js"></script>


		<!-- Light Gallery Plugin Js -->
		<script src="plugins/light-gallery/js/lightgallery-all.js"></script>

		<!-- Custom Js -->
		<script src="js/pages/medias/image-gallery.js"></script>
		<!-- Sparkline Chart Plugin Js -->
		<script src="plugins/jquery-sparkline/jquery.sparkline.js"></script>


		

		<!-- Sparkline Chart Plugin Js -->
		<script src="plugins/jquery-sparkline/jquery.sparkline.js"></script>

		<!-- Custom Js -->
		<script src="js/admin.js"></script>
		<!--   <script src="js/pages/index.js"></script> -->

		<!--    <script src="js/pages/charts/morris.js"></script> -->

		<!-- Demo Js -->
		<script src="js/demo.js"></script>
		<script src="plugins/light-gallery/js/lg-share.js"></script>
		<script src="plugins/light-gallery/js/lg-hash.js"></script> 
		<script type="text/javascript">
		function afterAjaxReload(){
			 $('#aniimated-thumbnials').lightGallery({
			        thumbnail: true,
			         selector: 'a' 
			    });
			 
			 $(function() {
					new Chart(document.getElementById("line_chart")
							.getContext("2d"), getChartJs('line'));

				});
			
		 }

			$(function() {
				new Chart(document.getElementById("line_chart")
						.getContext("2d"), getChartJs('line'));

			});

			function getChartJs(type) {
				var config = null;

				var arrLabelArray = new Array();
				var arrDataArray1 = new Array();
				var arrDataArray2 = new Array();
				$('.distance').each(function() {
					var arr1 = {};
					var arr2 = {};
					var arr3 = {};
					arr1.distance = $(this).val();
					arr2.userRunTime = $(this).next().val();
					arr3.avgRunData = $(this).next().next().val();

					//alert("arr1 = "+arr1.distance);

					arrLabelArray.push(arr1.distance);
					arrDataArray1.push(arr2.userRunTime);
					arrDataArray2.push(arr3.avgRunData);

				});

				//    alert("arrLabelArray = "+arrLabelArray);
				//    alert("arrDataArray1 = "+arrDataArray1);
				//    alert("arrDataArray2 = "+arrDataArray2);

				if (type === 'line') {
					config = {
						type : 'line',
						data : {
							labels : arrLabelArray, // ["0", "5", "8", "12", "15", "18", "20"],
							datasets : [
									{
										label : "Your Run ",
										data : arrDataArray1, //  [50.5, 59, 80, 81, 56, 55, 40],
										borderColor : 'rgba(0, 188, 212, 0.75)',
										backgroundColor : 'rgba(0, 188, 212, 0.3)',
										pointBorderColor : 'rgba(0, 188, 212, 0)',
										pointBackgroundColor : 'rgba(0, 188, 212, 0.9)',
										pointBorderWidth : 1
									},
									{
										label : "Category Avg Run ",
										data : arrDataArray2, // [28, 48, 40, 19, 86, 27, 90],
										borderColor : 'rgba(233, 30, 99, 0.75)',
										backgroundColor : 'rgba(233, 30, 99, 0.3)',
										pointBorderColor : 'rgba(233, 30, 99, 0)',
										pointBackgroundColor : 'rgba(233, 30, 99, 0.9)',
										pointBorderWidth : 1
									} ]
						},
						options : {
							responsive : true,
							legend : {
								display : true,
								labels : {
									fontColor : 'rgb(255, 99, 132)'
								}
							},
							scales : {
								yAxes : [ {
									scaleLabel : {
										display : true,
										labelString : 'Minutes'
									}
								} ],
								xAxes : [ {
									scaleLabel : {
										display : true,
										labelString : 'Kilometers'
									}
								} ]
							}
						}
					}
				}

				return config;
			}

			function openCertificate(UserID, EvntID, TypeID, strCertificatePath) {

				openPageInNewWin('reportViewer/rptCertificateViewer.jsp?ID='
						+ UserID + "&evnt=" + EvntID + "&comp=" + TypeID
						+ "&RP=" + strCertificatePath, 'CERT_RPT');
			}
		</script>
	</form>
</body>

</html>

