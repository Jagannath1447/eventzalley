


<%@page import="com.uthkrushta.basis.constants.IUMCConstants"%>
<%@page import="com.uthkrushta.basis.constants.ILabelConstants"%>
<%@page import="com.uthkrushta.utils.WebUtil"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@ page import="com.crystaldecisions.sdk.occa.report.application.OpenReportOptions,
                 com.crystaldecisions.sdk.occa.report.application.ReportClientDocument,
                 com.crystaldecisions.sdk.occa.report.exportoptions.ReportExportFormat,
                 com.crystaldecisions.sdk.occa.report.application.ParameterFieldController,
                 java.io.ByteArrayInputStream,
                 java.util.Calendar"
%>
 
<% 
String reportPath; 
String db_username;
String db_password;
long lngID = 0;
long eventID = 0;
long eventTypeID = 0;
long lngType=0;
long lngPrintType=0;
ReportClientDocument reportClientDocument;
ByteArrayInputStream byteArrayInputStream;
ParameterFieldController parameterFieldController;
byte[] byteArray;
int bytesRead;
String strContextPath="";

reportPath  = request.getParameter("RP");
lngID = WebUtil.parseLong(request, ILabelConstants.ID);
eventID = WebUtil.parseLong(request, "evnt");
eventTypeID = WebUtil.parseLong(request, "comp");

/* lngType=WebUtil.parseLong(request, "TYPE");
lngPrintType=WebUtil.parseLong(request, "PRINT_TYPE"); */
strContextPath=WebUtil.parseString(request, "CONTEXT_PATH"); 
Configuration configuration = new Configuration();

//Pass hibernate configuration file
configuration.configure("hibernate.cfg.xml");

db_username = configuration.getProperty("hibernate.connection.username");
db_password = configuration.getProperty("hibernate.connection.password");

/* reportPath = "Rpt/CustomerInvoice_Rpt.rpt"; */
/*
 * Instantiate ReportClientDocument and specify the Java Print Engine as the report processor.
 * Open a rpt file.
 */

reportClientDocument = new ReportClientDocument();

reportClientDocument.setReportAppServer(ReportClientDocument.inprocConnectionString);
reportClientDocument.open(reportPath, OpenReportOptions._openAsReadOnly);


/*
 * Set the database logon for all connections in main report.
 */

reportClientDocument.getDatabaseController().logon(db_username, db_password);

parameterFieldController = reportClientDocument.getDataDefController().getParameterFieldController();
parameterFieldController.setCurrentValue("",    "member_id",   lngID); 

parameterFieldController.setCurrentValue("",    "event_id",   eventID); 
parameterFieldController.setCurrentValue("",    "event_type",   eventTypeID); 

parameterFieldController.setCurrentValue("",    "status",   IUMCConstants.STATUS_ACTIVE); 
/* parameterFieldController.setCurrentValue("",    "item_status",   IUMCConstants.STATUS_ACTIVE); 
parameterFieldController.setCurrentValue("",    "type",   lngType); 
parameterFieldController.setCurrentValue("",    "app_context",   strContextPath);  */
/* parameterFieldController.setCurrentValue("",    "print_type",   IWEBConstants.STATUS_ACTIVE); */
/*
 * Retrieve PDF format of report and stream out to web browser.
 */

byteArrayInputStream = (ByteArrayInputStream) reportClientDocument.getPrintOutputController().export(ReportExportFormat.PDF);

response.reset();

response.setHeader("Content-disposition", "inline;filename=crreport.pdf");
response.setContentType("application/pdf");

byteArray = new byte[1024];
while((bytesRead = byteArrayInputStream.read(byteArray)) != -1) {
	response.getOutputStream().write(byteArray, 0, bytesRead);	
}

response.getOutputStream().flush();
response.getOutputStream().close();
			byteArrayInputStream.close();
reportClientDocument.close();

%>
