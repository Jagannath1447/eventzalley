package com.uthkrushta.basis.constants;

public interface ISessionAttributes {
	public static final String ROLE_ID = "ROLE_ID";
	public static final String DISP_NAME = "DISP_NAME";
	public static final String USER_ID = "USER_ID";
	public static final String MESSAGES = "MESSAGES";
	public static final String ERR_LIST = "ERR_LIST";
	public static final String LOGIN_ID = "LOGIN_ID";
	public static final String MEMBER_PHOTO_ID = "MEMBER_PHOTO_ID";
	public static final String LOGIN_USER = "LOGIN_USER";
	public static final String CENTER_CODE = "CENTER_CODE";
	public static final String COMP_MASTER = "COMP_MASTER";
	public static final String QUICK_MENU = "QUICK_MENU";
	public static final String LEFT_TREE_LIST = "LEFT_TREE_LIST";
	public static final String ACTIVE_FINANCIAL_YEAR="ACTIVE_FINANCIAL_YEAR";
	public static final String STAFF_REPORT="STAFF_REPORT";
	public static final String ATTANDACE_SIGNEDIN_REPORT="ATTANDACE_SIGNEDIN_REPORT";
	public static final String STAFF_SALARY_LIST="STAFF_SALARY_LIST";
	public static final String STAFF_SALARY_REPORT="STAFF_SALARY_REPORT";
	public static final String NO_OF_WORKING_DAYS="NO_OF_WORKING_DAYS";
	public static final String LIST_STAFF_SALARY_REPO="LIST_STAFF_SALARY_REPO";
	public static final String LIST_DETAILED_STAFF_SALARY_REPO="LIST_DETAILED_STAFF_SALARY_REPO";
	public static final String LIST_SERVICE_PAYMENT_LIST="LIST_SERVICE_PAYMENT_LIST";
	public static final String CURRENCY="CURRENCY";
	public static final String CURRENCY_SYMBOL="CURRENCY_SYMBOL";
	public static final String CURRENT_TIME_ZONE="CURRENT_TIME_ZONE";
	public static final String PERSONALTRAININGLST="PERSONALTRAININGLST";
	public static final String ASSIGNEDMEMBERSLST="ASSIGNEDMEMBERSLST";
	public static final String ATTENDANCELOGREPORT="ATTENDANCELOGREPORT";
	public static final String ASSESSMENTPENDINGREPORT="ASSESSMENTPENDINGREPORT";
	public static final String FEEDBACKREPORT="FEEDBACKREPORT";
	public static final String PRODUCT_TRANSFER_REPORT="PRODUCT_TRANSFER_REPORT";
	public static final String PRODUCT_TRANSFER_INCOMING_REPORT="PRODUCT_TRANSFER_INCOMING_REPORT";
	public static final String PRODUCT_TRANSFER_FROM_BRANCH="PRODUCT_TRANSFER_FROM_BRANCH";
	public static final String PRODUCT_TRANSFER_TO_BRANCH="PRODUCT_TRANSFER_TO_BRANCH";
	public static final String TRAINER_SCHEDULE="TRAINER_SCHEDULE";
	public static final String TRAINER_SCHEDULE_DATE="TRAINER_SCHEDULE_DATE";
	public static final String EXCEEDED_CHECK="EXCEEDED_CHECK";
	public static final String STAFF_SCHEDULE="STAFF_SCHEDULE";
	public static final String INT_STAFF_SCHEDULE="INT_STAFF_SCHEDULE";
	public static final String PT_DUE_MEM_LIST="PT_DUE_MEM_LIST";
	public static final String STAFF_SESSION_COUNT="STAFF_SESSION_COUNT";
	public static final String STAFF_DETAILED_SESSION="STAFF_DETAILED_SESSION";
	public static final String AMOUNT_FROM_STAFF="AMOUNT_FROM_STAFF";
	
	/*EventzAlley*/
	public static final String EVENT_TYPE_NAME = "EVENT_TYPE_NAME";
	public static final String EVENT_NAME ="EVENT_NAME";
	public static final String PHOTO_UPLOAD_PATH = "PHOTO_UPLOAD_PATH";
	public static final String EVENT_TYPE_ID = "EVENT_TYPE_ID";
	public static final String EVENT_ID ="EVENT_ID";
	public static final String EventPhotoID ="EventPhotoID";
	
	public static final String ATTENDANCELOGINLOGOUTREPORT="ATTENDANCELOGINLOGOUTREPORT";
	public static final String ATTENDANCELOGINFRMTO="ATTENDANCELOGINFRMTO";
	
	public static final String ATTENDANCE_REPO_LOGGTYPE="ATTENDANCE_REPO_LOGGTYPE";
	public static final String WEEK_START_DAY="WEEK_START_DAY";
	public static final String MEM_ATTENDANCE_LOG_STATUS="MEM_ATTENDANCE_LOG_STATUS";
	
	//Enhancement
	public static final String SALES_TAX_REPORT="SALES_TAX_REPORT";
	
	public static final String DETAILED_DATE_LIST = "DETAILED_DATE_LIST";
	public static final String DETAILED_MEMBER_STAFF_LIST = "DETAILED_MEMBER_STAFF_LIST";
	public static final String START_DATE="START_DATE";
	public static final String END_DATE="END_DATE";
	public static final String ATTENDANCE_SUMMARY_REPORT="ATTENDANCE_SUMMARY_REPORT";
	public static final String CURRENTLY_SIGNED_IN_REPORT_MEM_STATUS="CURRENTLY_SIGNED_IN_REPORT_MEM_STATUS";
	public static final String TODAY_SIGNED_IN_REPORT_MEM_STATUS="TODAY_SIGNED_IN_REPORT_MEM_STATUS";
	public static final String ATTENDACE_LOG_REPORT_MEM_STATUS="ATTENDACE_LOG_REPORT_MEM_STATUS";
	public static final String MEM_REPORT_MEM_STATUS="MEM_REPORT_MEM_STATUS";
	
	public static final String SALES_INV_REPORT_START_DATE="SALES_INV_REPORT_START_DATE";
	public static final String SALES_INV_REPORT_END_DATE="SALES_INV_REPORT_END_DATE";
	public static final String CURRENT_COMP_STATE_ID="CURRENT_COMP_STATE_ID";
}
