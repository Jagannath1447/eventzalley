package com.uthkrushta.basis.constants;



public interface IUMCConstants {
	//Eventzalley
	String ACTION_MATCHING_MAIL = "MATCHING_EMAIL";
	String DB_DATE_TIME_SEC_FORMAT = "yyyy-MM-dd HH:mm:ss";//(HH)to store the time in 24hr format
	int INT_ROLE_SUPERADMIN=1;
	int PROMOTION=2;
	 int TRANSACTIONAL=1;
	 String ACTION_SHOW_BATCH="Show Batch";
	 String BN_GLOBAL_SETTING = "GlobalSettingBean";
	 int ALL_MEMBER = 2;
	 
	 String BN_GLOBAL_IMAGE = "GlobalUrlSettingBean";
	 
	 //SMS
	 String LOAD_SMSTEMPLATE="LOAD_SMSTEMPLATE";
	 String LOAD_SMSTEMPLATE_TXT="LOAD_SMSTEMPLATE_TXT";
	 String BN_SMS_TEMP_NOTIFICATION="SMSTemplateNotificationBean";
	 String BN_SMS_VARIABLE_CODE_BEAN="SMSVariableCodeBean";
	 //SMS
	 String ACTION_SEND_SMS="ACTION_SEND_SMS";
	 String BN_SMS_TYPE="SmsTypeCodeBean";
	 String LOAD_SMS_TYPE="LOAD_SMS_TYPE";
	 String selSmsCategory="selSmsCategory";
	 String BN_SMS_TYPE_UTILITY_BEAN="SMSTypeUtilityBean";
	 String BN_SMS_TEMPLATE_BEAN="SmsTemplateBean";
	 String BN_SEND_TO_CODE_BEAN="SendToCodeBean";
	 
	//<QuestApp Mobile
		String BN_QP_STUDENT_MASTER_REG_TOKEN = "StudentMasterRegTokenBean";
		String URL_FIREBASE_SERVER = "https://fcm.googleapis.com/fcm/send";
		String FIREBASE_SERVER_KEY = "AAAAgaKSTCA:APA91bH35zSk3ETwp48IMvCj8DYVwmUEY6R5QGjYFVe23Ga_XxFMSG0m089hG18eJeyzGttVP6fOELY9IVQjEVhSzVe-_T4T8gfWrpeJn5mdALWTiSZiDdX9aRmYDysQjqCW2D50-jrZ9Mdlx2uQy0ejiB9ZfK0Mpg";
		String BN_QP_STUDENT_MASTER_VIEW = "StudentMasterViewBean";
		//QuestApp Mobile>	
	
		String COMPANY_ID = "COMPANY_ID" ;
	String ACTION_DETAILS= "ACTION_DETAILS";
	String ACTION_GET_STUDENTS = "ACTION_GET_STUDENTS";
	int STATUS_NEW = 1;
	int STATUS_ACTIVE = 2;
	int STATUS_INACTIVE = 3;
	int STATUS_DELETED = 4;
	int STATUS_REJECTED = 3;
	 
	int LOAN_STATUS_INPROCESS = 1;
	int LOAN_STATUS_SANCTIONED = 2;
	int LOAN_STATUS_DISBURSED = 3;
	int LOAN_STATUS_REJECTED = 4;
	int LOAN_STATUS_COMPLETED = 5;

	int MAIL_STATUS_NEW = 1;
	int MAIL_STATUS_SENT = 2;
	int MAIL_STATUS_FAILED = 3;
	
	int TEST_STATUS_NEW = 1;
	int TEST_STATUS_COMPLETED = 2;
	
	int PROPERTY_STATUS_PENDING = 1; 
	int PROPERTY_STATUS_APPROVED = 2;
	int PROPERTY_STATUS_REJECT = 3;
	int PROPERTY_STATUS_EXPIRE = 4;
	
	int FILE_UPLOAD_SINGLE = 1;
	int FILE_UPLOAD_MULTIPLE = 2;
	
	int NOT_SENT = 0;
	int SENT = 1;
	
	/*-----------Eventzalley--------------*/
	int MEMBER_ROLE = 4;
	int STAFF_ROLE =3;
	int STAFF_ROLE_TYPE =5;
	int MEMBER_ROLE_TYPE=2;
	long  ADMIN=2;
	int ADMIN_TYPE=1;
	int SUPER_ADMIN_ROLE_TYPE = 1;
	String USER_ID = "USER_ID";
	
	String BN_ACTIVITY_ROLE_BINDING_BEAN = "ActivityRoleBindingFrameWorkBean";
	String BN_APPLICATION_MASTER_MENU_BEAN = "ApplicationMasterBean";
	String BN_ROLE_TYPE ="RoleTypeBean";
	String BN_ROLE_BEAN = "RoleBean";
	String PAGE_HEADER = "HEADER";
	String PAGE_DESC = "DESC";
	String STATUS_TEXT = "STATUS";
	String STATUS_STYLE = "STYLE";
	String SHOW_BACK_BUTTON = "SHOW_BACK_BUTTON";
	String BACK_BUTTION_PATH = "BACK_BUTTION_PATH";
	String SUBMIT_GENERATE = "SUBMIT_GENERATE";
	
	String INC_PAGE_HEADER_URL = "MyIncludes/IncPageHeader.jsp";
	String LOOKUP_FILE_UPLOAD_URL = "../Lookup/lookupFileUploader.jsp";
	String PAGE_MODE_EDIT = "EDIT";
	String PAGE_MODE_NEW = "NEW";
	
	String ACTIVITY_ID = "ACTIVITY_ID";
	String ACTION_REGISTER ="REGISTER";
	String ACTION_REENABLE_TEST = "ACTION_REENABLE_TEST";
	String ACTION_ACTIVATE = "ACTIVATE";
	String ACTION_SUBMIT = "SUBMIT";
	String ACTION_SAVE_AND_NEXT = "SAVE_AND_NEXT";
	String ACTION_PUBLISH = "PUBLISH";
	String ACTION_GET = "GET";
	String ACTION_ADD = "ADD";
	String ACTION_CLOSE = "CLOSE";
	String ACTION_DEL = "DEL";
	String SAVE_NEW_MEMBER ="SAVE_NEW_MEMBER";
	String ACTION_ACT = "ACT";
	String ACTION_EDIT = "EDIT";
	String ACTION_DELETE_ITEM = "DELETE_ITEM";
	String ACTION_GET_NEXT_NUM = "GETNEXTNUM";
	String ACTION_SEND_MAIL = "SEND_MAIL";
	String ACTION_STATE_CHANGE = "STATE_CHANGE";
	String ACTION_GENERATE_QUESTION = "GENERATE";
	String ACTION_VIEW_ANSWER = "VIEW";
	
	String GENERATE_QUESTION = "GENERATE_QUESTIONS";
	
	String ACTION_GENERATE = "GENERATE";
	
	String CONTEXT_PATH="CONTEXT_PATH";
	
	String ADD = "ADD";
	String STR_BLANK = "";
    String ACTION_RESET= "RESET";
	String OK = "StatusOk";
	String WARNING = "StatusWarning";
	String ERROR = "StatusError";

	String DATE_FORMAT = "dd-MM-yyyy";
	String DB_DATE_FORMAT = "yyyy-MM-dd";
	String DB_DATE_TIME_FORMAT = "yyyy-MM-dd hh:mm";
    String DATE_YYYY_MM ="yyyy-MM";
	String TIME_FORMAT = "hh:mm:ss";
    String TIME_FORMAT_24 ="HH:mm:ss";
	String DELETE_RECORD = "DELETE_RECORD";
	String ACTION_APPROVE ="APPROVE";
	String ACTION_REJECT ="REJECT";

	
	int OBJ_MASTER = 2;
	int OBJ_MYWORK = 3;
	int CODE_BEAN = 1;

	int PAGINATION_SIZE = 15;
	int COMMIT_SIZE = 15;

	int MANAGE_PROJECT = 1;

	int ADMIN_POST = 1;
	int USER_POST = 2;
	
	int SINGLE_INSTANCE_PROCESSING = 1;
	
	String USERNAME = "USERNAME";
	String ROLEID = "ROLEID";
	String USERID = "USERID";
	String CITYID = "CITYID";
	String LOGIN_OBJ = "LOGIN_OBJ";
	String LOAN_REPORT_QUERY_STRING = "LOAN_REPORT_QUERY_STRING";
	// Bean name
	
	String BN_LOGIN_BEAN = "LoginBean";
	String BN_NAVIGATION_BEAN = "NavigationBean";
	String BN_COMPANY_MASTER_BEAN  = "CompanyMasterBean";
	String BN_DISTRICT_MASTER = "DistrictMasterBean";
	String BN_STATE_MASTER = "StateMasterBean";
	String BN_PRODUCT_MASTER = "ProductMasterBean";
	String BN_EMP_MASTER = "EmployeeMaster";
	String BN_EMP_STATUS = "EmployeeStatus";
	String BN_DEPT_MASTER= "EmployeeDept";
	
	String BN_RENT_MONTH_VIEW = "RentalMonthViewBean";
	String BN_PAY_MODE_MASTER = "PaymentModeCodeBean";
	String BN_TENANT_MASTER = "TenantMasterBean";
	String BN_AREA_MASTER = "AreaMasterBean";
	String BN_PROPERTY_MASTER = "PropertyMasterBean";
	//Beans for Question Pop application!!
	String BN_QP_CHAPTER_MASTER = "ChapterMaster";
	String BN_QP_EDUCATIONAL_INSTITUTE_MASTER = "EducationalInstituteMaster";
	String BN_QP_STAFF_MASTER = "StaffMasterBean";
	String BN_QP_STUDENT_MASTER = "StudentMasterBean";
	String BN_QP_CHAPTER_PLANNER_TRANSACTION = "ChapterPlannerTrns";
	String BN_QP_QUESTION_ANSWER_TRANSACTION = "QuestionAnswerTrns";
	
	
	String BN_RENTAL_AGREEMENT_MASTER = "RentalAgreementMasterBean";
	
	
	String BN_QP_STATUS = "QPStatus";
	String BN_QP_ROLE = "QPRole";
	String BN_QP_STATUS_TYPE = "QPStatusType";
	String BN_QP_SECURITY_QUESTION = "SecurityQuestion";
	String BN_QP_BATCH_CONFIGURATION = "BatchConfiguration";
	String BN_QP_STAFF_MASTER_VIEW="StaffMasterViewBean";
	String BN_QP_STAFF_MASTER_EDU_ROLE_STAT_VIEW="StaffMasterEduRoleStatViewBean";
	String BN_QP_QUES_ANS_TRANS_EDU_CHAP_VIEW="QuestionAnswerTransEduChapViewBean";
	String BN_QP_BATCH_CONFIGURATION_VIEW = "BatchConfigurationView";
	String BN_QP_QUES_ANS_CHAP_PLANR_VIEW = "QuesAnsCptrPlnrView";
	String BN_QP_QUES_SENT_TRNS="QuestionSentTrns";
	String BN_QP_STU_ANS_RECE_TRNS="StudentAnsReceivTrns";
	String BN_APPLICATION_MENU_BEAN="ApplicationMenuBean";
	String BN_QP_TRANSACTION_REPORT_VIEW= "TestReportView";
	String BN_QP_TEST_CONFIGURATION = "TestConfigurationBean";
	
	String BN_RENT_COLLECTION_TR = "RentCollectionBean";
	String BN_QP_TEST_ATTENDED_DETAILS="TestAttendedDetailsBean";
	String BN_QP_TEST_QUESTION_DETAILS="TestQuestionDetailsBean";
	
	String BN_QP_SELF_TEST_ATTENDED_DETAILS="SelfTestAttendedDetails";
	
	String BN_QP_TEST_QUESTION_DETAILS_ITEM="TestQuestionDetailsItemBean";
	String BN_QP_COMPLEXITY_BEAN="ComplexityBean";
	String BN_QP_ANSWER_BEAN="AnswerBean";
	String BN_STUDENT_MARKS_SUMMARY_VIEW = "StudentMarksSummaryView";
	
	String GET_ACTIVE_ROWS = " intStatus = "+ STATUS_ACTIVE;
	String GET_LOAN_STATUS = " intType = 2 ";
	String NAME_SORT_ASC = "strName";
	
	String BN_COUNTRY_MASTER = "CountryBean";
	String ST_MEMB_FIRST_NAME ="{[MEMB_FIRST_NAME]}";
	String ST_MEMB_LAST_NAME ="{[MEMB_LAST_NAME]}";
	String ST_CUST_MOB_NUM ="{[CUST_MOB_NUM]}";
	String ST_LOAN_TYPE ="{[LOAN_TYPE]}";
	String ST_BANK_NAME ="{[BANK_NAME]}";
	String ST_COMP_NAME ="{[COMP_NAME]}";
	String ST_PROPOSED_ROI ="{[PROPOSED_ROI]}";
	String ST_APPLICABLE_ROI ="{[APPLICABLE_ROI]}";
	String ST_PROC_FEE ="{[PROC_FEE]}";
	String ST_APPLIED_LOAN_AMT ="{[APPLIED_LOAN_AMT]}";
	String ST_PROPOSED_TENURE ="{[PROPOSED_TENURE]}";
	String ST_APPLICABLE_TENURE ="{[APPLICABLE_TENURE]}";
	String ST_PRE_CLOSE_CHARGE ="{[PRE_CLOSE_CHARGE]}";
	String ST_IS_PART_PAY_ALLOWED ="{[IS_PART_PAY_ALLOWED]}";
	String ST_PROC_PERIOD ="{[PROC_PERIOD]}";
	String ST_MEMB_ADDRESS ="{[MEMB_ADDRESS]}";
	String ST_MEMB_RANK = "{[MEMB_RANK]}";
	String ST_MEMB_CHIP = "{[MEMB_CHIP]}";
	String ST_MEMB_GUN ="{[MEMB_GUN]}";
	String ST_MEMB_PACE = "{[MEMB_PACE]}";
	String ST_MEMB_BIB ="{[MEMB_BIB]}";
	 String ST_CUST_PH_NO="{[CUST_PH_NO]}";
	 String ST_MEMB_MOB_NO="{[MEMB_MOB_NO]}";
	 String ST_MEMB_EMAIL="{[MEMB_EMAIL]}";
	 String ST_MEMB_USER_NAME="{[MEMB_USER_NAME]}";
	 String ST_MEMB_PASSWORD="{[MEMB_PASSWORD]}";
	 String ST_CENTER_NAME="{[CENTER_NAME]}";
	 String ST_CENTER_NO="{[CENTER_NO]}";
	 String ST_CENTER_ADDRESS="{[CENTER_ADDRESS]}";
	 String ST_CENTER_ADMIN="{[CENTER_ADMIN]}";
	 String ST_CENTER_EMAIL="{[CENTER_EMAIL]}";
	 String ST_MEMB_DOB="{[MEMB_DOB]}";
	 String ST_DOM="{[DOM]}";
	 String ST_DOC_NO="{[DOC_NO]}";
	 String ST_DOC_DATE="{[DOC_DATE]}";
	 String ST_DOC_AMT="{[DOC_AMT]}";
	 String ST_PLAN_TYPE="{[PLAN_TYPE]}";
	
	
	String ST_SANCTIONED_AMT = "{[SANCTIONED_LOAN_AMT]}";
	String ST_EMI = "{[ST_EMI]}";
	String ST_LOAN_ACC_NUMBER = "{[LOAN_ACC_NUMBER]}";
	String ST_SEND_TO = "{[SEND_TO]}";
	String ST_SMS_TEXT = "{[SMS_TEXT]}";
	
}
