package com.uthkrushta.basis.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.uthkrushta.basis.bean.MessageBean;





abstract public class GenericController {
	protected com.uthkrushta.basis.bean.MessageBean msg;
	protected ArrayList<MessageBean> arrMsg = null;
	
	public GenericController() {
		this.arrMsg = new ArrayList<MessageBean>();
	}
	
	public ArrayList<MessageBean> getArrMsg() {
		return arrMsg;
	}


	abstract public void doAction(HttpServletRequest request, HttpServletResponse response);
	abstract protected boolean modifyRecord(HttpServletRequest request);
	abstract protected boolean modifyRecords(HttpServletRequest request);
	abstract protected boolean validate(HttpServletRequest request);
	abstract public List<Object> getList(HttpServletRequest request);
	abstract public List<Object> getItemList(HttpServletRequest request);
	abstract public Object get(HttpServletRequest request);
	protected abstract String buildQueryString(HttpServletRequest request);
	abstract public long getRecordsCount(HttpServletRequest request);


	protected boolean deleteRecords() {
		// TODO Auto-generated method stub
		return false;
	}

	protected boolean validate() {
		// TODO Auto-generated method stub
		return false;
	}

	
}