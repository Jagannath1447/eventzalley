package com.uthkrushta.basis.factory;

import com.uthkrushta.basis.bean.MessageBean;



@SuppressWarnings("serial")
public class UCLException extends Exception{
	MessageBean msgBean = null;

	public UCLException(String strMsg, Throwable th, MessageBean msgBean) {
		super(strMsg, th);
		this.msgBean = msgBean;
	}

	public MessageBean getMsgBean() {
		return msgBean;
	}

	public void setMsg(MessageBean msgBean) {
		this.msgBean = msgBean;
	}

}
