package com.uthkrushta.basis.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil 
{
	public static final SessionFactory sessionFactory;

	static {
		try
		{
			Configuration conf = new Configuration();
			sessionFactory  = conf.configure().buildSessionFactory();
		}
		catch(Throwable ex)
		{
			System.err.println("Cant Load the session Factory" + ex.toString());
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory()
	{
		return sessionFactory;
	}
}
