package com.uthkrushta.basis.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import javax.mail.Session;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.ISessionAttributes;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.mybos.configuration.controller.EventPhotoUploadController;
import com.uthkrushta.mybos.master.bean.EventMasterBean;
import com.uthkrushta.utils.DBUtil;
import com.uthkrushta.utils.WebUtil;






@SuppressWarnings("serial")
public class UploadAttachment extends HttpServlet {
	EventPhotoUploadController objEPUC = new EventPhotoUploadController();
	
	
	private String strFolderPath;
	private File tempFile;
	private static final int THRESHOLD_SIZE     = 1024 * 1024 * 3;  // 3MB
    private static final int MAX_FILE_SIZE      = 1024 * 1024 * 40; // 40MB
    private static final int MAX_REQUEST_SIZE   = 1024 * 1024 * 50; // 50MB


	
    
	public UploadAttachment() {
        super();
    
    }
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("in do post");
		System.out.println(request+"  request");
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		/*PrintWriter out = response.getWriter();*/
		
		String fileName = null ;
		/*String EventTypeName = (String) request.getSession().getAttribute(ISessionAttributes.EVENT_TYPE_NAME);*/
		/*String EventName = (String) request.getSession().getAttribute(ISessionAttributes.EVENT_NAME);*/
		/*long eventID = WebUtil.parseLong(request, ILabelConstants.selEventName); */
		String eventName = ""; // request.getParameter("selEventName");
		/*EventMasterBean objEMB = new EventMasterBean();
		if(eventID > 0 ) {
			objEMB = (EventMasterBean) DBUtil.getRecord(ILabelConstants.BN_EVENT_MASTER, IUMCConstants.GET_ACTIVE_ROWS, "");
		
		}*/
		
		
		
		System.out.println(eventName+"  EventName");
		// Uploading of File Part
		//process only if its multi-part content
	    if(ServletFileUpload.isMultipartContent(request)){
            	// configures upload settings
	    		System.out.println("in do post 2");
                DiskFileItemFactory factory = new DiskFileItemFactory();
                factory.setSizeThreshold(THRESHOLD_SIZE);
                factory.setRepository(new File(System.getProperty("java.io.tmpdir")));
                ServletFileUpload upload = new ServletFileUpload(factory);
                upload.setFileSizeMax(MAX_FILE_SIZE);
                upload.setSizeMax(MAX_REQUEST_SIZE);
                
                
               
                ServletContext servletContext = getServletContext();
        		String contextPath = "/EventzAlley";			//servletContext.getRealPath("/") ;			//"../../../WebContent/eventImages"; +"/event_photos"		// request.getContextPath(); servletContext.getRealPath(File.separator);
//        		String contextPath = "D:/EventzAlley";
        		/*PrintWriter out2 = response.getWriter();*/
        		System.out.println("<br/>File system context path (in TestServlet): " + contextPath);
                System.out.println("new path try "+"../"+request.getServletPath());
        		
              
                

            	String fileNameToSave; 
            	//if(strFolderPath!=null){
            	if(true) {
            	try{
            		System.out.println("in do post 3");
            		 List formItems = upload.parseRequest(request);
                     Iterator iter = formItems.iterator();
                     while (iter.hasNext()) {
                    	 //System.out.println("in do post 4: "+iter.next().toString());
                    	 
                    	 
                    	 try {
                    		 
                    		 System.out.println("1");
                    		 FileItem item = null;
                    		 try {
                    			 item = (FileItem) iter.next();
                    			 
                    			 System.out.println("item "+item.toString());
                    			 
                        		 if (item != null && item.isFormField()) {

                       		      String name = item.getFieldName();//text1
                       		      eventName = item.getString();
                       		      System.out.println("item.getFieldName() "+item.getFieldName());
                       		      System.out.println("item.getString() "+item.getString());
	                       		   Path path = Paths.get(contextPath+File.separator+eventName);
	                               Files.createDirectories(path);
	                               System.out.println("path "+path);
	                               strFolderPath =path.toString() ;
	                               System.out.println("strFolderPath "+strFolderPath);
	                               

                       		    } 
                        		 
                    		 }catch(Exception e) {
                    			 System.out.println("Error "+e);
                    		 }
                    		 

                    		 
                    	 System.out.println("1.5");
                    	// processes only fields that are not form fields
                         if (item != null && !item.isFormField()) {
                               fileName = new File(item.getName()).getName();
                               
                               fileNameToSave = fileName;
                              if(fileName.length()>0)
                          	{
                            	  System.out.println("2");
                          		tempFile = new File(strFolderPath+File.separator+fileName);
                          		System.out.println("3");
                          		if(tempFile.exists()){
                          			System.out.println("4");
                          			int count = 0;
                          			do{
                          				
                          				tempFile = null;
                          				fileNameToSave = "test";
                          				System.out.println("5");
                          				tempFile = new File(strFolderPath+File.separator+fileNameToSave);
                          				System.out.println("6");
                          				count++;
                          			}while(tempFile.exists() && count < 1);
                          			tempFile = null;
                          		}
                          	}
                              System.out.println("7");
                             File storeFile = new File(strFolderPath+File.separator+fileNameToSave);
                              
                             
                             System.out.println("in do post 5");
                             // saves the file on disk
                             item.write(storeFile);
                             request.getSession().setAttribute(ISessionAttributes.EventPhotoID, eventName);
                             request.getSession().setAttribute(ISessionAttributes.PHOTO_UPLOAD_PATH,fileNameToSave);
                             objEPUC.modifyRecord(request);                           
                         }
                         
                    	 }catch(NoSuchElementException e) {
                    		 System.out.println("exception "+e);
                    	 }
                     }
                     
            	}catch(Exception ex){
            		ex.printStackTrace();
            		System.out.print(ex.getMessage());
            		response.sendError(HttpServletResponse.SC_NOT_MODIFIED, "Method Call should be post");
            	}
        }else{
            request.setAttribute("message",
                                 "Sorry this Servlet only handles file upload request");
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Method Call should be post");
        }
            	}
	    else{
	    	 request.setAttribute("message",
                     "Sorry this Servlet only handles file upload request");
	    	 response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Method Call should be post");
	    }
	   
	    System.out.println("in do post.. done");
	    
	   
	}
	
}
