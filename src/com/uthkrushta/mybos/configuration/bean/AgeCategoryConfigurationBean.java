package com.uthkrushta.mybos.configuration.bean;

public class AgeCategoryConfigurationBean {

	private long lngID;
	private String strName;
	private long lngGenderID;
	private long lngMinAge;
	private long lngMaxAge;
	private long lngEventID;
	private long lngEventTypeID;
	private int intStatus;
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
	public long getLngGenderID() {
		return lngGenderID;
	}
	public void setLngGenderID(long lngGenderID) {
		this.lngGenderID = lngGenderID;
	}
	public long getLngMinAge() {
		return lngMinAge;
	}
	public void setLngMinAge(long lngMinAge) {
		this.lngMinAge = lngMinAge;
	}
	public long getLngMaxAge() {
		return lngMaxAge;
	}
	public void setLngMaxAge(long lngMaxAge) {
		this.lngMaxAge = lngMaxAge;
	}
	public long getLngEventID() {
		return lngEventID;
	}
	public void setLngEventID(long lngEventID) {
		this.lngEventID = lngEventID;
	}
	public long getLngEventTypeID() {
		return lngEventTypeID;
	}
	public void setLngEventTypeID(long lngEventTypeID) {
		this.lngEventTypeID = lngEventTypeID;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	
	
}
