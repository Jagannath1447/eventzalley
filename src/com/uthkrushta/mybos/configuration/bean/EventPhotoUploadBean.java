package com.uthkrushta.mybos.configuration.bean;

import java.util.Date;

public class EventPhotoUploadBean {
	
	private long lngID;
	private long lngEventID;
	
	private String strImagePath;
	private String strImageDescription;
	private String strBibNoEntry;
	private int intStatus;
	private int intPublish;
	private long lngCreatedBy;
	private Date dtCreatedOn;
	private long lngUpdatedBy;
	private Date dtUpdatedOn;
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngEventID() {
		return lngEventID;
	}
	public void setLngEventID(long lngEventID) {
		this.lngEventID = lngEventID;
	}
	
	public String getStrImagePath() {
		return strImagePath;
	}
	public void setStrImagePath(String strImagePath) {
		this.strImagePath = strImagePath;
	}
	public String getStrImageDescription() {
		return strImageDescription;
	}
	public void setStrImageDescription(String strImageDescription) {
		this.strImageDescription = strImageDescription;
	}
	
	public String getStrBibNoEntry() {
		return strBibNoEntry;
	}
	public void setStrBibNoEntry(String strBibNoEntry) {
		this.strBibNoEntry = strBibNoEntry;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public int getIntPublish() {
		return intPublish;
	}
	public void setIntPublish(int intPublish) {
		this.intPublish = intPublish;
	}
	public long getLngCreatedBy() {
		return lngCreatedBy;
	}
	public void setLngCreatedBy(long lngCreatedBy) {
		this.lngCreatedBy = lngCreatedBy;
	}
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	public long getLngUpdatedBy() {
		return lngUpdatedBy;
	}
	public void setLngUpdatedBy(long lngUpdatedBy) {
		this.lngUpdatedBy = lngUpdatedBy;
	}
	public Date getDtUpdatedOn() {
		return dtUpdatedOn;
	}
	public void setDtUpdatedOn(Date dtUpdatedOn) {
		this.dtUpdatedOn = dtUpdatedOn;
	}
	
	
	
	
	
}
