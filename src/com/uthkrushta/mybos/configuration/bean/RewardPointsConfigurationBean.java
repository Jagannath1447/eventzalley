package com.uthkrushta.mybos.configuration.bean;

public class RewardPointsConfigurationBean {
 
	private long lngID;
	private double dblReferalPoints;
	private double dblAnivBDayPoints;
	private double dblAttendancePoints;
	private long   lngPresentDays;
	private double dblSalesPoints;
	private double dblSalesAmount;
	
	private int intPerReferal;
	private int intBA;
	private int intAttendance;
	private int intSales;
	
	private int intStatus;
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	
	public double getDblReferalPoints() {
		return dblReferalPoints;
	}
	public void setDblReferalPoints(double dblReferalPoints) {
		this.dblReferalPoints = dblReferalPoints;
	}
	public double getDblAnivBDayPoints() {
		return dblAnivBDayPoints;
	}
	public void setDblAnivBDayPoints(double dblAnivBDayPoints) {
		this.dblAnivBDayPoints = dblAnivBDayPoints;
	}
	public double getDblAttendancePoints() {
		return dblAttendancePoints;
	}
	public void setDblAttendancePoints(double dblAttendancePoints) {
		this.dblAttendancePoints = dblAttendancePoints;
	}
	public long getLngPresentDays() {
		return lngPresentDays;
	}
	public void setLngPresentDays(long lngPresentDays) {
		this.lngPresentDays = lngPresentDays;
	}
	public double getDblSalesPoints() {
		return dblSalesPoints;
	}
	public void setDblSalesPoints(double dblSalesPoints) {
		this.dblSalesPoints = dblSalesPoints;
	}
	public double getDblSalesAmount() {
		return dblSalesAmount;
	}
	public void setDblSalesAmount(double dblSalesAmount) {
		this.dblSalesAmount = dblSalesAmount;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public int getIntPerReferal() {
		return intPerReferal;
	}
	public void setIntPerReferal(int intPerReferal) {
		this.intPerReferal = intPerReferal;
	}
	public int getIntBA() {
		return intBA;
	}
	public void setIntBA(int intBA) {
		this.intBA = intBA;
	}
	public int getIntAttendance() {
		return intAttendance;
	}
	public void setIntAttendance(int intAttendance) {
		this.intAttendance = intAttendance;
	}
	public int getIntSales() {
		return intSales;
	}
	public void setIntSales(int intSales) {
		this.intSales = intSales;
	}
	
	
}
