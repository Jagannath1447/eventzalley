package com.uthkrushta.mybos.configuration.bean;

public class TransactionSettingBean {
	private long lngID;
	private String strName;
	private String strPrefix;
	private long lngStartNumber;
	private int intAutoNo;
	private int intAutoNoReset;
	private int intStatus;
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
	public String getStrPrefix() {
		return strPrefix;
	}
	public void setStrPrefix(String strPrefix) {
		this.strPrefix = strPrefix;
	}
	public long getLngStartNumber() {
		return lngStartNumber;
	}
	public void setLngStartNumber(long lngStartNumber) {
		this.lngStartNumber = lngStartNumber;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public int getIntAutoNo() {
		return intAutoNo;
	}
	public void setIntAutoNo(int intAutoNo) {
		this.intAutoNo = intAutoNo;
	}
	public int getIntAutoNoReset() {
		return intAutoNoReset;
	}
	public void setIntAutoNoReset(int intAutoNoReset) {
		this.intAutoNoReset = intAutoNoReset;
	}

}
