package com.uthkrushta.mybos.master.bean;

import java.util.Date;

public class EventTypeTimeMasterBean {

	private long lngID;
	private long lngEventTypeID;
	private long lngStartTimeID;
	private Date dtStartDate;
	private long lngBatchSize;
	private long lngEventID;
	private long lngCreatedBy;
	private Date dtCreatedOn;
	private long lngUpdatedBy;
	private Date dtUpdatedOn;
	private int intStatus;
	private long lngIsCompleted;
	
	public long getLngIsCompleted() {
		return lngIsCompleted;
	}
	public void setLngIsCompleted(long lngIsCompleted) {
		this.lngIsCompleted = lngIsCompleted;
	}
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngEventTypeID() {
		return lngEventTypeID;
	}
	public void setLngEventTypeID(long lngEventTypeID) {
		this.lngEventTypeID = lngEventTypeID;
	}
	
	public long getLngStartTimeID() {
		return lngStartTimeID;
	}
	public void setLngStartTimeID(long lngStartTimeID) {
		this.lngStartTimeID = lngStartTimeID;
	}
	public Date getDtStartDate() {
		return dtStartDate;
	}
	public void setDtStartDate(Date dtStartDate) {
		this.dtStartDate = dtStartDate;
	}
	public long getLngBatchSize() {
		return lngBatchSize;
	}
	public void setLngBatchSize(long lngBatchSize) {
		this.lngBatchSize = lngBatchSize;
	}
	public long getLngEventID() {
		return lngEventID;
	}
	public void setLngEventID(long lngEventID) {
		this.lngEventID = lngEventID;
	}
	public long getLngCreatedBy() {
		return lngCreatedBy;
	}
	public void setLngCreatedBy(long lngCreatedBy) {
		this.lngCreatedBy = lngCreatedBy;
	}
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	public long getLngUpdatedBy() {
		return lngUpdatedBy;
	}
	public void setLngUpdatedBy(long lngUpdatedBy) {
		this.lngUpdatedBy = lngUpdatedBy;
	}
	public Date getDtUpdatedOn() {
		return dtUpdatedOn;
	}
	public void setDtUpdatedOn(Date dtUpdatedOn) {
		this.dtUpdatedOn = dtUpdatedOn;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	
	
	
	
	
	
	
	
	
}
