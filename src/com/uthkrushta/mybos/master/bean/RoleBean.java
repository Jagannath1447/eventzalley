package com.uthkrushta.mybos.master.bean;

public class RoleBean {

	private long lngID;
	private String strName;
    private int intStatus;
	private long lngCompanyId;
	private int intRoleType;
	
	
	
	public RoleBean() {
		super();
		strName = "";
	}
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public long getLngCompanyId() {
		return lngCompanyId;
	}
	public void setLngCompanyId(long lngCompanyId) {
		this.lngCompanyId = lngCompanyId;
	}
	public int getIntRoleType() {
		return intRoleType;
	}
	public void setIntRoleType(int intRoleType) {
		this.intRoleType = intRoleType;
	}
	
	
}
