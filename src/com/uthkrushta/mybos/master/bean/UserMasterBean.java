package com.uthkrushta.mybos.master.bean;

import java.util.Date;

public class UserMasterBean {
	
	private long lngID;
	private Date dtDate;
	private long lngUserType;
	private long lngRoleID;
	private String strFirstName;
	private String strLastName;
	private long lngGenderID;
	private Date dtDOB;
	private long lngBloodGroupID;
	private String strOccupation;
	private String strPrimaryContactNumber;
	private String strSecondaryContactNumber;
	private String strUserName;
	private String strPassword;
	private String strConfirmPassword;
	private long lngSecurityID;
	private String strSecurityAnswer;
	private String strEmailID;
	private String strAddress;
	private String strImagePath;
	private long lngCountryID;
	private long lngStateID;
	
	private long lngCreatedBy;
	private Date dtCreatedOn;
	private long lngUpdatedBy;
	private Date dtUpdatedOn;
	private int intStatus;
	
	private String strSecondaryEmailID;
	private String strZipCode;
	private String strPartOfAnyRunningClub;
	private String strCompanyName;
	private String strAssignedUserName;
	private String strPersonalBest;
	private String strEmergencyCntDetails;
	private String strStreet;
	private String strCity;
	private String strState;
	private long lngCityID;
	
	
	private Date dtOtpDate;
	private long lngNumberOfOtp;
	
	
	
	
	
	
	
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public Date getDtDate() {
		return dtDate;
	}
	public void setDtDate(Date dtDate) {
		this.dtDate = dtDate;
	}
	public long getLngUserType() {
		return lngUserType;
	}
	public void setLngUserType(long lngUserType) {
		this.lngUserType = lngUserType;
	}
	public long getLngRoleID() {
		return lngRoleID;
	}
	public void setLngRoleID(long lngRoleID) {
		this.lngRoleID = lngRoleID;
	}
	public String getStrFirstName() {
		return strFirstName;
	}
	public void setStrFirstName(String strFirstName) {
		this.strFirstName = strFirstName;
	}
	public String getStrLastName() {
		return strLastName;
	}
	public void setStrLastName(String strLastName) {
		this.strLastName = strLastName;
	}
	public long getLngGenderID() {
		return lngGenderID;
	}
	public void setLngGenderID(long lngGenderID) {
		this.lngGenderID = lngGenderID;
	}
	public Date getDtDOB() {
		return dtDOB;
	}
	public void setDtDOB(Date dtDOB) {
		this.dtDOB = dtDOB;
	}
	
	public long getLngBloodGroupID() {
		return lngBloodGroupID;
	}
	public void setLngBloodGroupID(long lngBloodGroupID) {
		this.lngBloodGroupID = lngBloodGroupID;
	}
	public String getStrOccupation() {
		return strOccupation;
	}
	public void setStrOccupation(String strOccupation) {
		this.strOccupation = strOccupation;
	}
	public String getStrPrimaryContactNumber() {
		return strPrimaryContactNumber;
	}
	public void setStrPrimaryContactNumber(String strPrimaryContactNumber) {
		this.strPrimaryContactNumber = strPrimaryContactNumber;
	}
	public String getStrSecondaryContactNumber() {
		return strSecondaryContactNumber;
	}
	public void setStrSecondaryContactNumber(String strSecondaryContactNumber) {
		this.strSecondaryContactNumber = strSecondaryContactNumber;
	}
	
	
	public String getStrImagePath() {
		return strImagePath;
	}
	public void setStrImagePath(String strImagePath) {
		this.strImagePath = strImagePath;
	}
	public String getStrUserName() {
		return strUserName;
	}
	public void setStrUserName(String strUserName) {
		this.strUserName = strUserName;
	}
	public String getStrPassword() {
		return strPassword;
	}
	public void setStrPassword(String strPassword) {
		this.strPassword = strPassword;
	}
	public String getStrConfirmPassword() {
		return strConfirmPassword;
	}
	public void setStrConfirmPassword(String strConfirmPassword) {
		this.strConfirmPassword = strConfirmPassword;
	}
	public long getLngSecurityID() {
		return lngSecurityID;
	}
	public void setLngSecurityID(long lngSecurityID) {
		this.lngSecurityID = lngSecurityID;
	}
	public String getStrSecurityAnswer() {
		return strSecurityAnswer;
	}
	public void setStrSecurityAnswer(String strSecurityAnswer) {
		this.strSecurityAnswer = strSecurityAnswer;
	}
	public String getStrEmailID() {
		return strEmailID;
	}
	public void setStrEmailID(String strEmailID) {
		this.strEmailID = strEmailID;
	}
	public String getStrAddress() {
		return strAddress;
	}
	public void setStrAddress(String strAddress) {
		this.strAddress = strAddress;
	}
	public long getLngCountryID() {
		return lngCountryID;
	}
	public void setLngCountryID(long lngCountryID) {
		this.lngCountryID = lngCountryID;
	}
	public long getLngStateID() {
		return lngStateID;
	}
	public void setLngStateID(long lngStateID) {
		this.lngStateID = lngStateID;
	}
	public long getLngCreatedBy() {
		return lngCreatedBy;
	}
	public void setLngCreatedBy(long lngCreatedBy) {
		this.lngCreatedBy = lngCreatedBy;
	}
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	public long getLngUpdatedBy() {
		return lngUpdatedBy;
	}
	public void setLngUpdatedBy(long lngUpdatedBy) {
		this.lngUpdatedBy = lngUpdatedBy;
	}
	public Date getDtUpdatedOn() {
		return dtUpdatedOn;
	}
	public void setDtUpdatedOn(Date dtUpdatedOn) {
		this.dtUpdatedOn = dtUpdatedOn;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public String getStrSecondaryEmailID() {
		return strSecondaryEmailID;
	}
	public void setStrSecondaryEmailID(String strSecondaryEmailID) {
		this.strSecondaryEmailID = strSecondaryEmailID;
	}
	public String getStrZipCode() {
		return strZipCode;
	}
	public void setStrZipCode(String strZipCode) {
		this.strZipCode = strZipCode;
	}
	public String getStrPartOfAnyRunningClub() {
		return strPartOfAnyRunningClub;
	}
	public void setStrPartOfAnyRunningClub(String strPartOfAnyRunningClub) {
		this.strPartOfAnyRunningClub = strPartOfAnyRunningClub;
	}
	public String getStrCompanyName() {
		return strCompanyName;
	}
	public void setStrCompanyName(String strCompanyName) {
		this.strCompanyName = strCompanyName;
	}
	public String getStrAssignedUserName() {
		return strAssignedUserName;
	}
	public void setStrAssignedUserName(String strAssignedUserName) {
		this.strAssignedUserName = strAssignedUserName;
	}
	public String getStrPersonalBest() {
		return strPersonalBest;
	}
	public void setStrPersonalBest(String strPersonalBest) {
		this.strPersonalBest = strPersonalBest;
	}
	public String getStrEmergencyCntDetails() {
		return strEmergencyCntDetails;
	}
	public void setStrEmergencyCntDetails(String strEmergencyCntDetails) {
		this.strEmergencyCntDetails = strEmergencyCntDetails;
	}
	public String getStrStreet() {
		return strStreet;
	}
	public void setStrStreet(String strStreet) {
		this.strStreet = strStreet;
	}
	public String getStrCity() {
		return strCity;
	}
	public void setStrCity(String strCity) {
		this.strCity = strCity;
	}
	public String getStrState() {
		return strState;
	}
	public void setStrState(String strState) {
		this.strState = strState;
	}
	public long getLngCityID() {
		return lngCityID;
	}
	public void setLngCityID(long lngCityID) {
		this.lngCityID = lngCityID;
	}
	public Date getDtOtpDate() {
		return dtOtpDate;
	}
	public void setDtOtpDate(Date dtOtpDate) {
		this.dtOtpDate = dtOtpDate;
	}
	public long getLngNumberOfOtp() {
		return lngNumberOfOtp;
	}
	public void setLngNumberOfOtp(long lngNumberOfOtp) {
		this.lngNumberOfOtp = lngNumberOfOtp;
	}
	
	
	




}
