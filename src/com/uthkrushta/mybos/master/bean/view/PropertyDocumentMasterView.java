package com.uthkrushta.mybos.master.bean.view;

public class PropertyDocumentMasterView {
	private long lngID;
	private long lngPropertyID;
	private String strDescription;
	private String strDocumentPath;
	
	public long getLngPropertyID() {
		return lngPropertyID;
	}
	public void setLngPropertyID(long lngPropertyID) {
		this.lngPropertyID = lngPropertyID;
	}
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrDescription() {
		return strDescription;
	}
	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}
	public String getStrDocumentPath() {
		return strDocumentPath;
	}
	public void setStrDocumentPath(String strDocumentPath) {
		this.strDocumentPath = strDocumentPath;
	}
	
	

}
