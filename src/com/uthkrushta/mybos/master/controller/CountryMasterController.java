package com.uthkrushta.mybos.master.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ajaxanywhere.AAUtils;

import com.uthkrushta.basis.bean.MessageBean;
import com.uthkrushta.basis.bean.NameValueBean;
import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.ITextConstants;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.basis.controller.GenericController;
import com.uthkrushta.mybos.master.bean.CountryBean;
import com.uthkrushta.mybos.master.bean.StateMasterBean;
import com.uthkrushta.utils.DBUtil;
import com.uthkrushta.utils.UMBException;
import com.uthkrushta.utils.UTHException;
import com.uthkrushta.utils.VetoCheck;
import com.uthkrushta.utils.VetoMessage;
import com.uthkrushta.utils.WebUtil;

public class CountryMasterController extends GenericController{ 
CountryBean objCountryMasterBean = new CountryBean();
long lngCompanyId =0;
	
boolean blnModified = false;

//is required??? I guess no
public boolean modifyUser(HttpServletRequest request){
	// logic to store the values
	// Fill object from UI
	objCountryMasterBean.setLngID(WebUtil.parseLong(request, "hdnID"));
	objCountryMasterBean.setStrName(WebUtil.parseString(request, "txtCountryName"));
	objCountryMasterBean.setIntStatus(IUMCConstants.STATUS_ACTIVE);
	// delegate the call to framework
	
		try {
			blnModified = DBUtil.modifyRecord(objCountryMasterBean);
		} catch (UTHException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	return blnModified;
}

//getting all the list of countries
@SuppressWarnings("unchecked")
public List<Object> getList(HttpServletRequest request)
{
	String strWhere = buildQueryString(request);//Building Where Clause
	AAUtils.addZonesToRefresh(request, "Status,List,Footer");//Refresh the Zones which to refreshed
	return DBUtil.getRecords(IUMCConstants.BN_COUNTRY_MASTER,strWhere, "strName");
}

@Override
public void doAction(HttpServletRequest request, HttpServletResponse response) {
	String strUserAction = WebUtil.parseString(request, ILabelConstants.hdnUserAction);//Getting User Action
	if (null != strUserAction) {
			if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_SUBMIT))
			{//Save objects to database
				modifyRecords(request);
			}
			else if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_DEL))
			{ //deleting selected records
				deleteRecords(request);
			}
			else if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_ADD))
			{//Refreshing the zones
				AAUtils.addZonesToRefresh(request, "List,Footer");
			}
		}
	}
	
//is required??? I guess no
@Override
protected boolean modifyRecord(HttpServletRequest request) {
	// TODO Auto-generated method stub
boolean blnModified = false;
	objCountryMasterBean.setLngID(WebUtil.parseLong(request, "hdnID"));
	objCountryMasterBean.setStrName(WebUtil.parseString(request, "txtCountryName"));
	objCountryMasterBean.setIntStatus(IUMCConstants.STATUS_ACTIVE);
	// delegate the call to framework
	
		
			try {
				blnModified = DBUtil.modifyRecord(objCountryMasterBean);
			} catch (UTHException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	return blnModified;
}

//Save objects to database
@Override
protected boolean modifyRecords(HttpServletRequest request) {
	boolean blnModified = false;
	//ArrayList<Object> arrCountryMasterList = new ArrayList<Object>(); //Not used
	int intRowCounter = WebUtil.parseInt(request, ILabelConstants.hdnRowCounter);
	for (int i = 1; i <= intRowCounter; i++) {
		objCountryMasterBean = new CountryBean(); 
		objCountryMasterBean.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnID_ + i));
		objCountryMasterBean.setStrName(WebUtil.parseString(request, ILabelConstants.txtName_ + i));
		objCountryMasterBean.setIntStatus(IUMCConstants.STATUS_ACTIVE);
		if(objCountryMasterBean.getStrName().trim().length() == 0) continue;
		if (validate(request,objCountryMasterBean)) {
			try {
				blnModified = DBUtil.modifyRecord(objCountryMasterBean);
		} catch (UTHException e) {
			e.printStackTrace();
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED );
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status");
			blnModified = false;
			break;
		}
		}else{
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED + "\"" + objCountryMasterBean.getStrName() + "\""+ ITextConstants.MSG_DUPLICATE_ENTRY);
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status");
			blnModified = false;
			break;
		}
	}
	if (blnModified) {
		msg = new MessageBean();
		msg.setStrMsgText(ITextConstants.MSG_SAVED_SUCCESS);
		msg.setStrMsgType(IUMCConstants.OK);
		msg.setBlnIsLeadingMessage(true);
		this.arrMsg.add(msg);
		AAUtils.addZonesToRefresh(request, "Status,List,Footer");
		blnModified = true;
	}else{
		msg = new MessageBean();
		msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED );
		msg.setStrMsgType(IUMCConstants.ERROR);
		msg.setBlnIsLeadingMessage(true);
		this.arrMsg.add(msg);
		AAUtils.addZonesToRefresh(request, "Status");
		blnModified =  false;
	}
	return blnModified;
}

//deleting selected records
protected boolean deleteRecords(HttpServletRequest request) {
	boolean blnDeleted=false;
	String strSelectedID=WebUtil.parseString(request, ILabelConstants.hdnSelectedID);
	
	try{
		
		
		blnDeleted = DBUtil.deleteRecords(IUMCConstants.BN_COUNTRY_MASTER,strSelectedID );
		
		if(blnDeleted){
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS);
			msg.setStrMsgType(IUMCConstants.OK);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status,List,Footer");
			return true;
		}
		else
		{
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_DELETE_FAILED);
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status,List,Footer");
			return false;
		}
	}catch(UTHException ex){
		msg = new MessageBean();
		msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS );
		msg.setStrMsgType(IUMCConstants.ERROR);
		msg.setBlnIsLeadingMessage(true);
		this.arrMsg.add(msg);
		AAUtils.addZonesToRefresh(request, "Status");
		ex.printStackTrace();
		return false;
		
	}
	
}

//validating whether the Country name entered already exists, if exists we'll not save it
protected boolean validate(HttpServletRequest request,CountryBean objCountryMasterBean) {
	NameValueBean objNameValueBean = null;
	ArrayList<NameValueBean> arrNameValueList = new ArrayList<NameValueBean>();
	if(objCountryMasterBean != null){
		
		objNameValueBean = new NameValueBean("lngID", ""+objCountryMasterBean.getLngID(), true);
		arrNameValueList.add(objNameValueBean);
		
		objNameValueBean = new NameValueBean("strName",objCountryMasterBean.getStrName(),false);
		arrNameValueList.add(objNameValueBean);
		return DBUtil.checkIsUnique(IUMCConstants.BN_COUNTRY_MASTER, IUMCConstants.BN_COUNTRY_MASTER, arrNameValueList);
	}else{
		return true;
	}
}

@Override
public Object get(HttpServletRequest request) {
	// TODO Auto-generated method stub
	return null;
}


@Override
public String buildQueryString(HttpServletRequest request) {
	String strWhereClause = " intStatus = "+IUMCConstants.STATUS_ACTIVE;
	
	if(WebUtil.parseString(request, ILabelConstants.txtNameFilter).length() > 0){
		strWhereClause += " AND strName LIKE '"+ WebUtil.parseString(request, ILabelConstants.txtNameFilter) + "'";
	}
	
	strWhereClause = strWhereClause.replace("*", "%");
	return strWhereClause;
}

@Override
public long getRecordsCount(HttpServletRequest request) {
	String strWhereClause = buildQueryString(request);
	return DBUtil.getRecordsCount(IUMCConstants.BN_COUNTRY_MASTER, strWhereClause);
}

@Override
public List<Object> getItemList(HttpServletRequest request) {
	// TODO Auto-generated method stub
	return null;
}

@Override
protected boolean validate(HttpServletRequest request) {
	// TODO Auto-generated method stub
	return false;
}


/*protected boolean deleteRecords1(HttpServletRequest request) {
	boolean blnDeleted = false;
	boolean blnIsDeletable = false;
	String strItems = "";
	String strDelete = "";
	int intUserAction = 2;
	long lngActivityID = WebUtil.parseLong(request,IUMCConstants.ACTIVITY_ID);
	String strSelectedID=WebUtil.parseString(request, ILabelConstants.hdnSelectedID);
	try{
		
		//Write generalized code to restrict the used data
		VetoMessage objVeto =  (VetoMessage) DBUtil.vetoCheck(request, lngActivityID, strSelectedID,intUserAction);
		if(null == objVeto)
		{
			objVeto = new VetoMessage();
		}
		if(!objVeto.getStrids().equals("") && !objVeto.getStrids().equalsIgnoreCase(";"))
		{	
			blnDeleted = DBUtil.deleteRecords(IUMCConstants.BN_COUNTRY_MASTER,objVeto.getStrids() );
		  	
			//deleting material batches
		  	String strID="";
			strID = objVeto.getStrids().replaceAll(";", ",");
			strID = strID.substring(1,strID.length()-1);
			if(blnDeleted)
			{
				blnDeleted= DBUtil.updateRecords(IUMCConstants.BN_MATERIAL_BATCH, "intStatus="+IUMCConstants.STATUS_DELETED, "lngMaterialID IN ("+strID+")");
			}
		}
		if(!objVeto.getStrUnselIds().equals("") &&  !objVeto.getStrUnselIds().equalsIgnoreCase(";"))
		  {
			  String strMyIds = objVeto.getStrUnselIds().substring(1,objVeto.getStrUnselIds().length()-1);
			  strMyIds = strMyIds.replace(";", ",");
			  List lstCountry = (List)DBUtil.getRecords(IUMCConstants.BN_COUNTRY_MASTER, IUMCConstants.GET_ACTIVE_ROWS+" and lngID IN ("+strMyIds+")","");
			  if(null != lstCountry)
			  {
				  for(int m=0;m<lstCountry.size();m++)
				  {
					  CountryBean  objCB = (CountryBean)lstCountry.get(m);
					  strItems += objCB.getStrName()+",";
				  }
				  strItems = strItems.substring(0, strItems.length()-1);
			  }
		  }	
		
		
		if(blnDeleted){
			msg = new MessageBean();
			if(objVeto.isBlnIsdel() == true)
			{
				msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS+" '"+strItems+"' Used");
			}
			else
			{
				msg.setStrMsgText(ITextConstants.MSG_DELETE_SUCCESS);
			}
			msg.setStrMsgType(IUMCConstants.OK);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status,List,Footer");
			return true;
		}
		else
		{
			msg = new MessageBean();
			msg.setStrMsgText("'"+strItems+"' "+ITextConstants.MSG_USED);
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status,List,Footer");
			return false;
		}
	}catch(UMBException ex){
		msg = new MessageBean();
		msg.setStrMsgText(ITextConstants.MSG_DELETE_FAILED );
		msg.setStrMsgType(IUMCConstants.ERROR);
		msg.setBlnIsLeadingMessage(true);
		this.arrMsg.add(msg);
		AAUtils.addZonesToRefresh(request, "Status");
		ex.printStackTrace();
		return false;
		
	}
}*/

}

