package com.uthkrushta.mybos.master.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ajaxanywhere.AAUtils;

import com.uthkrushta.basis.bean.MessageBean;
import com.uthkrushta.basis.bean.PaginationBean;
import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.ISessionAttributes;
import com.uthkrushta.basis.constants.ITextConstants;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.basis.controller.GenericController;
import com.uthkrushta.basis.util.SendMail;
import com.uthkrushta.mybos.code.bean.RoleTypeBean;
import com.uthkrushta.mybos.code.bean.StatusBean;
import com.uthkrushta.mybos.master.bean.RoleBean;
import com.uthkrushta.mybos.master.bean.UserMasterBean;
import com.uthkrushta.utils.DBUtil;
import com.uthkrushta.utils.UTHException;
import com.uthkrushta.utils.WebUtil;

public class UserMasterController extends GenericController {

	UserMasterBean objUMB = new UserMasterBean(); 
	private UserMasterBean objUserMasterBean = new UserMasterBean();
	StatusBean objSB = new StatusBean();
	
	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		String strUserAction = WebUtil.parseString(request, ILabelConstants.hdnUserAction);//Getting User Action
		if (null != strUserAction) {
			if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_SUBMIT))
			{//Save objects to database
				modifyRecord(request);
			}else if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_DEL))
			{//Save objects to database
				delete(request);
			}
			else if(strUserAction.equalsIgnoreCase(IUMCConstants.SAVE_NEW_MEMBER))
			{//Save objects to database
				modifyNewMember(request);
			}
			
			
			
		}
		else if(strUserAction.equalsIgnoreCase(IUMCConstants.ACTION_MATCHING_MAIL)){ matchingmail(request);}
	
	
	}
	
	public boolean delete(HttpServletRequest request)
	{
		boolean blnDeleted=false;
		try
		{
			blnDeleted=DBUtil.deleteRecords(ILabelConstants.BN_USER_MASTER_BEAN, WebUtil.parseString(request, ILabelConstants.hdnSelectedID));
			if(blnDeleted)
			{
				msg=new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_DELETED_SUCCESS);
				msg.setStrMsgType(IUMCConstants.OK);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefreh(request, "Status,List,Footer");
			}
			else
			{
				msg=new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_DELETED_FAILED);
				msg.setStrMsgType(IUMCConstants.ERROR);
				msg.setBlnIsLeadingMessage(true);
				this.arrMsg.add(msg);
				AAUtils.addZonesToRefreh(request, "Status");
			}
		}
			catch(UTHException ex)
			{
				ex.printStackTrace();
				msg=new MessageBean();
				msg.setStrMsgText(ITextConstants.MSG_DELETED_FAILED);
				msg.setStrMsgType(IUMCConstants.ERROR);
				msg.setBlnIsLeadingMessage(true);
				AAUtils.addZonesToRefreh(request, "Status");
			}
			return blnDeleted;
		}

	public boolean updatePhoto(HttpServletRequest request) {
		boolean update = false;
		 long userID = 0;
         Long memberID = (Long) request.getSession().getAttribute("LOGIN_ID");
         if(null != memberID ){
         	userID = memberID.longValue();
         }
         String strImagePath =  (String) request.getSession().getAttribute(ISessionAttributes.PHOTO_UPLOAD_PATH);
		
		try {
			DBUtil.updateRecords(ILabelConstants.BN_USER_MASTER_BEAN, "strImagePath = '"+strImagePath+"'", IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+userID);
		} catch (UTHException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (update) {
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_SUCCESS);
			msg.setStrMsgType(IUMCConstants.OK);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status,List,Footer,PhotoChange");
			update = true;
		}else{
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED );
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status");
			update =  false;
		}
		
		return update;
		
		
		
		
	}
	
	public boolean updateMemberPhoto(HttpServletRequest request) {
		boolean update = false;
		 long userID = 0;
         userID =  (Long) request.getSession().getAttribute(ISessionAttributes.MEMBER_PHOTO_ID);
         
         String strImagePath =  (String) request.getSession().getAttribute(ISessionAttributes.PHOTO_UPLOAD_PATH);
		
		try {
			DBUtil.updateRecords(ILabelConstants.BN_USER_MASTER_BEAN, "strImagePath = '"+strImagePath+"'", IUMCConstants.GET_ACTIVE_ROWS+" AND lngID = "+userID);
		} catch (UTHException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (update) {
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_SUCCESS);
			msg.setStrMsgType(IUMCConstants.OK);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status,List,Footer,PhotoChange");
			update = true;
		}else{
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED );
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status");
			update =  false;
		}
		
		return update;
		
		
		
		
	}
	
	
	protected boolean modifyNewMember(HttpServletRequest request) {
		boolean blnModified = false;
		long newMemberID = WebUtil.parseLong(request, ILabelConstants.newMember);
		String mobNum = WebUtil.parseString(request, ILabelConstants.primaryNumber);
		String password = WebUtil.parseString(request, ILabelConstants.password);
		String strUpdateClause= " strPrimaryContactNumber ='"+mobNum+"' , strUserName = '"+mobNum+"' , strPassword = '"+password+"'";
		String strWhereClause=" lngID = "+newMemberID;
		
		
		
		try {
			blnModified = DBUtil.updateRecords(ILabelConstants.BN_USER_MASTER_BEAN, strUpdateClause, strWhereClause);
			if(blnModified) {
				blnModified = DBUtil.updateRecords(ILabelConstants.BN_EVENT_TYPE_REGISTRATION, " lngGroupLeaderID = "+newMemberID, " lngMemberID = "+newMemberID);
			}
			
		} catch (UTHException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		if (blnModified) {
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_SUCCESS);
			msg.setStrMsgType(IUMCConstants.OK);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status,selectMember");
			blnModified = true;
		}else{
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED );
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status");
			blnModified =  false;
		}
		
		
		
		return blnModified;
			
	}
	
	
	
	
	
	@Override
	protected boolean modifyRecord(HttpServletRequest request) {
		// TODO Auto-generated method stub
		boolean blnModified = false;
		long userID = ((Long) request.getSession().getAttribute(ISessionAttributes.LOGIN_ID)).longValue();
		objUMB.setLngID(WebUtil.parseLong(request, ILabelConstants.hdnID));
		objUMB.setDtDate(WebUtil.parseDate(request, ILabelConstants.REG_DATE, IUMCConstants.DATE_FORMAT));
		objUMB.setStrFirstName(WebUtil.parseString(request, ILabelConstants.FirstName));
		objUMB.setStrLastName(WebUtil.parseString(request, ILabelConstants.LastName));
		objUMB.setLngGenderID(WebUtil.parseLong(request, ILabelConstants.selGender));
		objUMB.setDtDOB(WebUtil.parseDate(request, ILabelConstants.DOB, IUMCConstants.DATE_FORMAT));
		objUMB.setLngBloodGroupID(WebUtil.parseLong(request, ILabelConstants.selBloodGroup));
		objUMB.setLngRoleID(WebUtil.parseLong(request, ILabelConstants.selRoleID));
		objUMB.setStrOccupation(WebUtil.parseString(request, ILabelConstants.Occupation));
		objUMB.setStrPrimaryContactNumber(WebUtil.parseString(request, ILabelConstants.primaryNumber));
		objUMB.setStrSecondaryContactNumber(WebUtil.parseString(request, ILabelConstants.secondaryNumber));
		objUMB.setStrEmailID(WebUtil.parseString(request, ILabelConstants.emailID));
		objUMB.setStrAddress(WebUtil.parseString(request, ILabelConstants.address));
		objUMB.setStrUserName(WebUtil.parseString(request, ILabelConstants.UserName));
		objUMB.setStrPassword(WebUtil.parseString(request, ILabelConstants.password));
		objUMB.setStrConfirmPassword(WebUtil.parseString(request, ILabelConstants.confirmPassword));
		objUMB.setLngSecurityID(WebUtil.parseLong(request, ILabelConstants.selSecQuestion));
		objUMB.setStrSecurityAnswer(WebUtil.parseString(request, ILabelConstants.SecAnswer));
		objUMB.setStrImagePath(WebUtil.parseString(request, ILabelConstants.MemberPhoto));
		objUMB.setStrSecondaryEmailID(WebUtil.parseString(request, ILabelConstants.SecondaryEmailID));
		objUMB.setStrZipCode(WebUtil.parseString(request, ILabelConstants.ZipCode));
		objUMB.setStrCompanyName(WebUtil.parseString(request, ILabelConstants.CompanyName));
		objUMB.setStrPartOfAnyRunningClub(WebUtil.parseString(request, ILabelConstants.OtherOrganization));
		objUMB.setStrAssignedUserName(WebUtil.parseString(request, ILabelConstants.AssignedUserName));
		objUMB.setStrPersonalBest(WebUtil.parseString(request, ILabelConstants.PersonalBestLink));
		objUMB.setStrEmergencyCntDetails(WebUtil.parseString(request, ILabelConstants.EmergencyCntNo));
		objUMB.setStrStreet(WebUtil.parseString(request, ILabelConstants.Street));
		objUMB.setStrCity(WebUtil.parseString(request, ILabelConstants.City));
		objUMB.setStrState(WebUtil.parseString(request, ILabelConstants.State));
		objUMB.setLngCountryID(WebUtil.parseLong(request, ILabelConstants.Country));
		
		if(objUMB.getLngID() > 0)
		{
			objUMB.setDtCreatedOn(WebUtil.parseDate(request, ILabelConstants.hdnCreatedon, IUMCConstants.DB_DATE_TIME_SEC_FORMAT));
			objUMB.setLngCreatedBy(WebUtil.parseLong(request, ILabelConstants.hdnCreatedBy));
			objUMB.setDtUpdatedOn(new Date());
			objUMB.setLngUpdatedBy(userID);
		}
		else
		{	
			objUMB.setDtCreatedOn(new Date());
			objUMB.setLngCreatedBy(userID);
			objUMB.setDtUpdatedOn(new Date());
			objUMB.setLngUpdatedBy(userID);
			
		}
		
		objUMB.setIntStatus(2);
		
		try {
			blnModified = DBUtil.modifyRecord(objUMB);
		} catch (UTHException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (blnModified) {
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_SUCCESS);
			msg.setStrMsgType(IUMCConstants.OK);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status,List,Footer");
			blnModified = true;
		}else{
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_SAVED_FAILED );
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true);
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status");
			blnModified =  false;
		}
		return blnModified;
	}
	
	
protected boolean matchingmail(HttpServletRequest request) {
		
		boolean blnModified = false;
		
		String strWhereClause = "";
		String strValidEmail = "";
	
		
		strValidEmail = WebUtil.parseString(request, ILabelConstants.txtValidEmail);
		
			
		strWhereClause = "strEmail ='"+strValidEmail+"' ";
		strWhereClause += " AND " + IUMCConstants.GET_ACTIVE_ROWS;
		
		objUserMasterBean = (UserMasterBean) DBUtil.getRecord(ILabelConstants.BN_USER_MASTER_BEAN, strWhereClause , "");

				
		if(null == objUserMasterBean ){
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_EMAIL_MATCHING_FAILED );
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setBlnIsLeadingMessage(true); 
			blnModified = false;
			this.arrMsg.add(msg);
			AAUtils.addZonesToRefresh(request, "Status");
			
		}else {
			
			
			
			blnModified = sendResetMail(request,objUserMasterBean.getLngID());
			if(blnModified){
			msg = new MessageBean();
			msg.setStrMsgText(ITextConstants.MSG_RESET_EMAIL );
			msg.setStrMsgType(IUMCConstants.OK);
			msg.setBlnIsLeadingMessage(true); 
			blnModified = false;
			this.arrMsg.add(msg);
			}
			AAUtils.addZonesToRefresh(request, "Status");
			
		}
				
			
		return blnModified;
	}

private boolean sendResetMail(HttpServletRequest request,long lngID ) {
	
	
	boolean blnMailSent = false;
	
  String strURL = "http://23.226.236.128//PwdReset.jsp?ID=" + lngID ; 
	
   String 	strDescription = "";
   String strValidEmail = "";
   String strSubject = "";
   String strFrom = "";
   
  
   strFrom = "108@108bespoke.com";
   strSubject = "Reset Password";
   strValidEmail = WebUtil.parseString(request, ILabelConstants.txtValidEmail);
   
   strDescription = "<p>Please <a href="  + strURL + ">Click Here</a> to Reset Your Password</p>";

   
   
	
	

	
	
	try {
		blnMailSent = SendMail.sendMail(strValidEmail, strFrom, null, null, strSubject, strDescription);//(strFrom,strValidEmail , null, null, strSubject , strDescription,false,"","" );
		//
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		blnMailSent = false;
		msg = new MessageBean();
		msg.setBlnIsLeadingMessage(true);
		msg.setStrMsgText(ITextConstants.MSG_MAIL_ERROR);
		msg.setStrMsgType(IUMCConstants.ERROR);
	}
	return blnMailSent; 
	
	
	
}



private String buildQuery(HttpServletRequest request) {
	 objSB = new StatusBean();
	 RoleBean objRB = new RoleBean();
	 String strMemberRoleID = "("; 
	List lstMemberRoles = DBUtil.getRecords(ILabelConstants.BN_ROLE_MASTER, IUMCConstants.GET_ACTIVE_ROWS+" AND intRoleType = "+IUMCConstants.MEMBER_ROLE_TYPE, "");
	if(lstMemberRoles != null && lstMemberRoles.size() >0) {
		for(int i = 0; i<lstMemberRoles.size(); i++) {
			objRB = (RoleBean) lstMemberRoles.get(i);
			if(i != lstMemberRoles.size()-1 ) {
				strMemberRoleID = strMemberRoleID+objRB.getLngID()+",";
			}else {
				strMemberRoleID = strMemberRoleID+objRB.getLngID()+")";
			}
		}
		
		
	}else {
		strMemberRoleID = "(0)";
	}
	
	String strWhere = IUMCConstants.GET_ACTIVE_ROWS+" AND lngRoleID IN  "+strMemberRoleID ;
	
	if(WebUtil.parseString(request, ILabelConstants.txtNameFilter).length() > 0){
		strWhere += " AND strFirstName LIKE '" + request.getParameter(ILabelConstants.txtNameFilter) + "'";
	}
	
	if(WebUtil.parseLong(request, ILabelConstants.txtGenFilter) > 0){
		strWhere += " AND lngGenderID LIKE '" + request.getParameter(ILabelConstants.txtGenFilter) + "'";
	}
	
	if(WebUtil.parseString(request, ILabelConstants.txtMobFilter).length() >  0){
		strWhere += " AND strPrimaryContactNumber LIKE '" + request.getParameter(ILabelConstants.txtMobFilter) + "'";
	}
	if(WebUtil.parseString(request, ILabelConstants.txtPlaceFilter).length() >  0){
		strWhere += " AND strCity LIKE '" + request.getParameter(ILabelConstants.txtPlaceFilter) + "'";
	}
	
	objSB.setGenID(WebUtil.parseLong(request, ILabelConstants.txtGenFilter));
	objSB.setStrMob(WebUtil.parseString(request, ILabelConstants.txtMobFilter));
	objSB.setStrName(WebUtil.parseString(request, ILabelConstants.txtNameFilter));
	objSB.setStrPlace(WebUtil.parseString(request, ILabelConstants.txtPlaceFilter));
	
	strWhere = strWhere.replace("*", "%");
	return strWhere;
}


private String buildStaffQuery(HttpServletRequest request) {
	 objSB = new StatusBean();
	 RoleBean objRB = new RoleBean();
	 String strStaffRoleID = "("; 
	List lstStaffRoles = DBUtil.getRecords(ILabelConstants.BN_ROLE_MASTER, IUMCConstants.GET_ACTIVE_ROWS+" AND intRoleType = "+IUMCConstants.STAFF_ROLE_TYPE, "");
	if(lstStaffRoles != null && lstStaffRoles.size() >0) {
		for(int i = 0; i<lstStaffRoles.size(); i++) {
			objRB = (RoleBean) lstStaffRoles.get(i);
			if(i != lstStaffRoles.size()-1 ) {
				strStaffRoleID = strStaffRoleID+objRB.getLngID()+",";
			}else {
				strStaffRoleID = strStaffRoleID+objRB.getLngID()+")";
			}
		}
		
		
	}else {
		strStaffRoleID = "(0)";
	}
	
	String strWhere = IUMCConstants.GET_ACTIVE_ROWS+" AND lngRoleID IN "+strStaffRoleID;
	
	if(WebUtil.parseString(request, ILabelConstants.txtNameFilter).length() > 0){
		strWhere += " AND strFirstName LIKE '" + request.getParameter(ILabelConstants.txtNameFilter) + "'";
	}
	
	if(WebUtil.parseLong(request, ILabelConstants.txtGenFilter) > 0){
		strWhere += " AND lngGenderID LIKE '" + request.getParameter(ILabelConstants.txtGenFilter) + "'";
	}
	
	if(WebUtil.parseString(request, ILabelConstants.txtMobFilter).length() >  0){
		strWhere += " AND strPrimaryContactNumber LIKE '" + request.getParameter(ILabelConstants.txtMobFilter) + "'";
	}
	if(WebUtil.parseString(request, ILabelConstants.txtPlaceFilter).length() >  0){
		strWhere += " AND strCity LIKE '" + request.getParameter(ILabelConstants.txtPlaceFilter) + "'";
	}
	
	objSB.setGenID(WebUtil.parseLong(request, ILabelConstants.txtGenFilter));
	objSB.setStrMob(WebUtil.parseString(request, ILabelConstants.txtMobFilter));
	objSB.setStrName(WebUtil.parseString(request, ILabelConstants.txtNameFilter));
	objSB.setStrPlace(WebUtil.parseString(request, ILabelConstants.txtPlaceFilter));
	
	strWhere = strWhere.replace("*", "%");
	return strWhere;
}

	@Override
	protected boolean modifyRecords(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean validate(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Object> getList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		
		String strWhere = buildQuery(request);
		
		int intNumberOfRecords = WebUtil.parseInt(request, ILabelConstants.txtRecordsPerPage);
        int intCurrentPage = WebUtil.parseInt(request, ILabelConstants.hdnCurrentPage);
        
        if(intNumberOfRecords == 0){
            intNumberOfRecords = 10;
        }
        if(intCurrentPage == 0){
            intCurrentPage = 1;
        }
	
        PaginationBean objPaginationBean = new PaginationBean();
        objPaginationBean.setIntNumberOfRecords(intNumberOfRecords);
        objPaginationBean.setIntStartingCount((intCurrentPage-1)*intNumberOfRecords);
		
		/*String strWhereClause = IUMCConstants.GET_ACTIVE_ROWS+" AND lngRoleID = "+IUMCConstants.MEMBER_ROLE ;*/
		return DBUtil.getRecords(ILabelConstants.BN_USER_MASTER_BEAN, strWhere, "strFirstName", objPaginationBean);
	}
	
	
	public List<Object> getStaffList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		
		String strWhere = buildStaffQuery(request);
		
		int intNumberOfRecords = WebUtil.parseInt(request, ILabelConstants.txtRecordsPerPage);
        int intCurrentPage = WebUtil.parseInt(request, ILabelConstants.hdnCurrentPage);
        
        if(intNumberOfRecords == 0){
            intNumberOfRecords = 10;
        }
        if(intCurrentPage == 0){
            intCurrentPage = 1;
        }
	
        PaginationBean objPaginationBean = new PaginationBean();
        objPaginationBean.setIntNumberOfRecords(intNumberOfRecords);
        objPaginationBean.setIntStartingCount((intCurrentPage-1)*intNumberOfRecords);
		
		/*String strWhereClause = IUMCConstants.GET_ACTIVE_ROWS+" AND lngRoleID = "+IUMCConstants.MEMBER_ROLE ;*/
		return DBUtil.getRecords(ILabelConstants.BN_USER_MASTER_BEAN, strWhere, "strFirstName", objPaginationBean);
	}
	

	@Override
	public List<Object> getItemList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public long getRoleTypeID(HttpServletRequest request)
	{
		 long lngStaffType=0;
		 UserMasterBean objSMBFromSession=(UserMasterBean)request.getSession().getAttribute(ISessionAttributes.LOGIN_ID);
		 if(null!=objSMBFromSession)
		 {
			 RoleBean objRB=new RoleBean();
			 objRB=(RoleBean)DBUtil.getRecord(IUMCConstants.BN_ROLE_BEAN, IUMCConstants.GET_ACTIVE_ROWS+" and lngID = "+objSMBFromSession.getLngRoleID(), "");
			 if(null!= objRB){
				 lngStaffType=objRB.getIntRoleType();
			 }else{
				 lngStaffType = 0;
			 }
			 
		 }
		return lngStaffType;
	}
	
	@Override
	public Object get(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return objSB;
	}

	@Override
	protected String buildQueryString(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getRecordsCount(HttpServletRequest request) {
		// TODO Auto-generated method stub
		
		String strWhere = buildQuery(request);
		
		 /*String strWhereClause = IUMCConstants.GET_ACTIVE_ROWS + " AND lngRoleID = "+IUMCConstants.MEMBER_ROLE;*/
		
		return DBUtil.getRecordsCount(ILabelConstants.BN_USER_MASTER_BEAN, strWhere);
		
	}

	
	
	
	
	
}
