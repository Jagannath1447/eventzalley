package com.uthkrushta.mybos.transaction.bean;

import java.sql.Date;

public class EventTypeRegistrationBean {
	
	private long lngID;
	private long lngEventID;
	private long lngEventTypeID;
	
	private long lngMemberID;
	
	private long lngAgeCategoryID;
	
	private String strBIBNo;
	private long lngGroupLeaderID;
	private long lngCreatedBy;
	private Date dtCreatedOn;
	private long lngUpdatedBy;
	private Date dtUpdatedOn;
	private int intStatus;
	
	private String strBibName;
	private String strID;
	private String strRegistrationNumber;
	private String strAssignedUserID;
	private String strEventTypeAmount;
	private String strDiscountAmount;
	private String strAmountPaid;
	private String strTransactonID;
	private String strTransactionMessage;
	private String strPaymentmode;
	private String strpaymentMode;
	private String strTshirtSize;
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngEventID() {
		return lngEventID;
	}
	public void setLngEventID(long lngEventID) {
		this.lngEventID = lngEventID;
	}
	public long getLngEventTypeID() {
		return lngEventTypeID;
	}
	public void setLngEventTypeID(long lngEventTypeID) {
		this.lngEventTypeID = lngEventTypeID;
	}
	
	public long getLngMemberID() {
		return lngMemberID;
	}
	public void setLngMemberID(long lngMemberID) {
		this.lngMemberID = lngMemberID;
	}
	
	
	public long getLngAgeCategoryID() {
		return lngAgeCategoryID;
	}
	public void setLngAgeCategoryID(long lngAgeCategoryID) {
		this.lngAgeCategoryID = lngAgeCategoryID;
	}
	public String getStrBIBNo() {
		return strBIBNo;
	}
	public void setStrBIBNo(String strBIBNo) {
		this.strBIBNo = strBIBNo;
	}
	public long getLngGroupLeaderID() {
		return lngGroupLeaderID;
	}
	public void setLngGroupLeaderID(long lngGroupLeaderID) {
		this.lngGroupLeaderID = lngGroupLeaderID;
	}
	public long getLngCreatedBy() {
		return lngCreatedBy;
	}
	public void setLngCreatedBy(long lngCreatedBy) {
		this.lngCreatedBy = lngCreatedBy;
	}
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	public long getLngUpdatedBy() {
		return lngUpdatedBy;
	}
	public void setLngUpdatedBy(long lngUpdatedBy) {
		this.lngUpdatedBy = lngUpdatedBy;
	}
	public Date getDtUpdatedOn() {
		return dtUpdatedOn;
	}
	public void setDtUpdatedOn(Date dtUpdatedOn) {
		this.dtUpdatedOn = dtUpdatedOn;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public String getStrBibName() {
		return strBibName;
	}
	public void setStrBibName(String strBibName) {
		this.strBibName = strBibName;
	}
	public String getStrID() {
		return strID;
	}
	public void setStrID(String strID) {
		this.strID = strID;
	}
	public String getStrRegistrationNumber() {
		return strRegistrationNumber;
	}
	public void setStrRegistrationNumber(String strRegistrationNumber) {
		this.strRegistrationNumber = strRegistrationNumber;
	}
	public String getStrAssignedUserID() {
		return strAssignedUserID;
	}
	public void setStrAssignedUserID(String strAssignedUserID) {
		this.strAssignedUserID = strAssignedUserID;
	}
	public String getStrEventTypeAmount() {
		return strEventTypeAmount;
	}
	public void setStrEventTypeAmount(String strEventTypeAmount) {
		this.strEventTypeAmount = strEventTypeAmount;
	}
	public String getStrDiscountAmount() {
		return strDiscountAmount;
	}
	public void setStrDiscountAmount(String strDiscountAmount) {
		this.strDiscountAmount = strDiscountAmount;
	}
	public String getStrAmountPaid() {
		return strAmountPaid;
	}
	public void setStrAmountPaid(String strAmountPaid) {
		this.strAmountPaid = strAmountPaid;
	}
	public String getStrTransactonID() {
		return strTransactonID;
	}
	public void setStrTransactonID(String strTransactonID) {
		this.strTransactonID = strTransactonID;
	}
	public String getStrTransactionMessage() {
		return strTransactionMessage;
	}
	public void setStrTransactionMessage(String strTransactionMessage) {
		this.strTransactionMessage = strTransactionMessage;
	}
	public String getStrPaymentmode() {
		return strPaymentmode;
	}
	public void setStrPaymentmode(String strPaymentmode) {
		this.strPaymentmode = strPaymentmode;
	}
	public String getStrpaymentMode() {
		return strpaymentMode;
	}
	public void setStrpaymentMode(String strpaymentMode) {
		this.strpaymentMode = strpaymentMode;
	}
	public String getStrTshirtSize() {
		return strTshirtSize;
	}
	public void setStrTshirtSize(String strTshirtSize) {
		this.strTshirtSize = strTshirtSize;
	}
	

}
