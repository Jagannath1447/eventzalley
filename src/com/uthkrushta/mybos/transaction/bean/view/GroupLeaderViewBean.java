package com.uthkrushta.mybos.transaction.bean.view;

public class GroupLeaderViewBean {

	private long lngID;
	private long lngMemberID;
	private String strFirstName;
	private String strLastName;
	private long lngGroupLeaderID;
	
	private long lngEventID;
	private long LngCompetitionID;
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public long getLngMemberID() {
		return lngMemberID;
	}
	public void setLngMemberID(long lngMemberID) {
		this.lngMemberID = lngMemberID;
	}
	public String getStrFirstName() {
		return strFirstName;
	}
	public void setStrFirstName(String strFirstName) {
		this.strFirstName = strFirstName;
	}
	public String getStrLastName() {
		return strLastName;
	}
	public void setStrLastName(String strLastName) {
		this.strLastName = strLastName;
	}
	public long getLngGroupLeaderID() {
		return lngGroupLeaderID;
	}
	public void setLngGroupLeaderID(long lngGroupLeaderID) {
		this.lngGroupLeaderID = lngGroupLeaderID;
	}
	public long getLngEventID() {
		return lngEventID;
	}
	public void setLngEventID(long lngEventID) {
		this.lngEventID = lngEventID;
	}
	public long getLngCompetitionID() {
		return LngCompetitionID;
	}
	public void setLngCompetitionID(long lngCompetitionID) {
		LngCompetitionID = lngCompetitionID;
	}
	
	
	
}
