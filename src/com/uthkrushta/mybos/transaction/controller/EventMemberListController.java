package com.uthkrushta.mybos.transaction.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.uthkrushta.basis.bean.PaginationBean;
import com.uthkrushta.basis.constants.ILabelConstants;
import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.basis.controller.GenericController;
import com.uthkrushta.mybos.code.bean.StatusBean;
import com.uthkrushta.utils.DBUtil;
import com.uthkrushta.utils.WebUtil;

public class EventMemberListController extends GenericController{
	StatusBean objSB = new StatusBean();
	@Override
	public void doAction(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected boolean modifyRecord(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean modifyRecords(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected boolean validate(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Object> getList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		List lstMembers = null;
		String strWhere = buildQueryString(request);
		
		
		int intNumberOfRecords = WebUtil.parseInt(request, ILabelConstants.txtRecordsPerPage);
        int intCurrentPage = WebUtil.parseInt(request, ILabelConstants.hdnCurrentPage);
        
        if(intNumberOfRecords == 0){
            intNumberOfRecords = 50;
        }
        if(intCurrentPage == 0){
            intCurrentPage = 1;
        }
	
        PaginationBean objPaginationBean = new PaginationBean();
        objPaginationBean.setIntNumberOfRecords(intNumberOfRecords);
        objPaginationBean.setIntStartingCount((intCurrentPage-1)*intNumberOfRecords);
		
		
		return lstMembers = DBUtil.getRecords(ILabelConstants.BN_EVENT_MEMBER_LIST_VIEW,strWhere, "strFirstName",objPaginationBean);
	}

	@Override
	public List<Object> getItemList(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object get(HttpServletRequest request) {
		// TODO Auto-generated method stub
		return objSB;
	}

	@Override
	protected String buildQueryString(HttpServletRequest request) {
		// TODO Auto-generated method stub
		
		List lstMembers = null;
		long eventID = WebUtil.parseInt(request, ILabelConstants.selEventName);
		long eventTypeID = WebUtil.parseInt(request, ILabelConstants.selEventTypeName);
		String strWhere="";
		
		if(eventID > 0){
			
			if(eventTypeID >0){
				strWhere = "lngEventID ="+eventID+" AND lngEventTypeID = "+eventTypeID;
			}else {
				strWhere = "lngEventID ="+eventID;
			}
		}
		
			if(WebUtil.parseString(request, ILabelConstants.txtNameFilter).length() > 0){
				strWhere += " AND strFirstName LIKE '" + request.getParameter(ILabelConstants.txtNameFilter) + "'";
			}
			
			if(WebUtil.parseString(request, ILabelConstants.txtBibNoFilter).length() > 0){
				strWhere += " AND strBibNo LIKE '" + request.getParameter(ILabelConstants.txtBibNoFilter) + "'";
			}
			
			if(WebUtil.parseString(request, ILabelConstants.txtCategoryFilter).length() >  0){
				strWhere += " AND strCategoryName LIKE '" + request.getParameter(ILabelConstants.txtCategoryFilter) + "'";
			}
			
			objSB.setStrName(WebUtil.parseString(request, ILabelConstants.txtNameFilter));
			objSB.setStrBibNo(WebUtil.parseString(request, ILabelConstants.txtBibNoFilter));
			objSB.setStrCategory(WebUtil.parseString(request, ILabelConstants.txtCategoryFilter));
			
		
			strWhere = strWhere.replace("*", "%");
			return strWhere;
		
		
	}

	@Override
	public long getRecordsCount(HttpServletRequest request) {
		// TODO Auto-generated method stub
		
		String strWhere = buildQueryString(request);
				return DBUtil.getRecordsCount(ILabelConstants.BN_EVENT_MEMBER_LIST_VIEW, strWhere);
						
						
			
	}

}
