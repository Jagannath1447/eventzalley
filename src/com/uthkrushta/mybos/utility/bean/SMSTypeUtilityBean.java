package com.uthkrushta.mybos.utility.bean;

public class SMSTypeUtilityBean {

	private long lngID;
	private String strName;
	private int intStatus;
	private long lngMsgCategoryID;
	
	public long getLngID() {
		return lngID;
	}
	public void setLngID(long lngID) {
		this.lngID = lngID;
	}
	public String getStrName() {
		return strName;
	}
	public void setStrName(String strName) {
		this.strName = strName;
	}
	public int getIntStatus() {
		return intStatus;
	}
	public void setIntStatus(int intStatus) {
		this.intStatus = intStatus;
	}
	public long getLngMsgCategoryID() {
		return lngMsgCategoryID;
	}
	public void setLngMsgCategoryID(long lngMsgCategoryID) {
		this.lngMsgCategoryID = lngMsgCategoryID;
	}
	
	
}
