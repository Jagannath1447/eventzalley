package com.uthkrushta.utils;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.TimerTask;

import com.uthkrushta.basis.constants.IUMCConstants;
import com.uthkrushta.qp.master.controller.RandomQuestionGeneratorController;
import com.uthkrushta.qp.master.controller.StudentQuesAnsSntController;
import com.uthkrushta.qp.transaction.bean.QuestionAnswerTrns;

public class AutoTrigger extends TimerTask{
 
	
	Date now;
	@Override
	public void run() {
		 final AutoTrigger timer = new AutoTrigger();
		boolean b = false;
	
		QuestionAnswerTrns objQAT = new QuestionAnswerTrns();
		StudentQuesAnsSntController objSQASC = new StudentQuesAnsSntController();
		
		long lngTimeNow = objSQASC.getTime();
		
		RandomQuestionGeneratorController objRQG = new RandomQuestionGeneratorController();
		System.out.println("*************************Inside autotrigger***********************************");
		
	    	now = new Date(); // initialize date
		 	System.out.println("Time is :" + now); 
		 	
		 	SimpleDateFormat sdf=new SimpleDateFormat("HH");
			  Date dt=new Date();
			  String strDt=sdf.format(dt);
		 	long lngHrs= Long.parseLong(strDt);
		 	
		 	if(lngHrs > 8 && lngHrs < 22)
		 	{
		 		System.out.println("generateQuestion()----------------------------------------------------------------------------------------------------------------------------");
		 		objRQG.generateQuestion();
		 		System.out.println("after generateQuestion()----------------------------------------------------------------------------------------------------------------------------");
		 		
		 	}
		 	/*if(lngTimeNow == 22)
		 	{
		 		objRQG.sendMailToSudentsToViewAnswer();
		 	}*/
		 		
		 /*if(!b){
			 System.out.println("all the questions have been generated, no more questions to display");
			 timer.cancel();
			 
		 }*/
		 	//objQAT = (QuestionAnswerTrns)objRQG.get();
		 	
	}
}
