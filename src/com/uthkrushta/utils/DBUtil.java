package com.uthkrushta.utils;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;




import net.sf.ehcache.Statistics;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.uthkrushta.basis.bean.MessageBean;
import com.uthkrushta.basis.bean.NameValueBean;
import com.uthkrushta.basis.bean.PaginationBean;
import com.uthkrushta.basis.constants.IUMCConstants;



public class DBUtil {
	
	
	
	public static long getRecordsCount(String strTableName, String strWhereClause) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		String SQL_QUERY = null;
		List lstReturn=null;
		SQL_QUERY = "SELECT COUNT(*) FROM " + strTableName;
		if (null != strWhereClause && !strWhereClause.equalsIgnoreCase("")) {
			SQL_QUERY = SQL_QUERY + "  WHERE " + strWhereClause + " ";
		}
		Query query = session.createQuery(SQL_QUERY);
		lstReturn = query.list();
		if (null != lstReturn && lstReturn.size() > 0) {
			return ((Long)lstReturn.get(0)).longValue();
		}
		return 0;
	}
	
public static boolean importExcel(String strquery) throws UTHException {
		
	  
		
		boolean success=false;
		Session session = null;
		UTHException exUAVS = null;
		MessageBean msg = null;
		Statistics stat=null;
		ResultSet rs=null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			Transaction tx = null;
			tx = session.beginTransaction();
			Query qq=session.createSQLQuery(strquery);
		    qq.executeUpdate();
	     	tx.commit();
			
			
				msg = new MessageBean();
				msg.setStrMsgText("Records Successfully Uploaded");
				//msg.setStrMsgType(IUCLConstants.OK);
				
			
			
		} catch (HibernateException ex) {
			//long s=stat.getQueryExecutionCount();
			success=false;
			msg = new MessageBean();
			msg.setStrMsgText(ex.getMessage());
			exUAVS = new UTHException("Hibernate Exception", ex, msg);
			throw exUAVS;
		} finally {
			if (null != session) {
				session.flush();
				session.close();
			}
		}
		return true;
	}

	
	
	@SuppressWarnings("rawtypes")
	public static List getRecords(String strTableName, String strWhereClause, String strSortClause) {
		return getRecords( null,  strTableName,  strWhereClause,  strSortClause,null);
	}

	public static List getRecords(String strTableName, String strWhereClause, String strSortClause,PaginationBean objPaginationBean) {
		
		return getRecords( null,  strTableName,  strWhereClause,  strSortClause,objPaginationBean);
	}
//	@SuppressWarnings("rawtypes")
//	public static List getRecords(String strSelectList, String strTableName, String strWhereClause, String strSortClause , PaginationBean objPaginationBean) {
//		Session session = HibernateUtil.getSessionFactory().openSession();
//		String SQL_QUERY = null;
//		if (null == strSelectList || strSelectList.equals("")) {
//			SQL_QUERY = "";
//		} else {
//			SQL_QUERY = "select " + strSelectList;
//		}
//		SQL_QUERY = SQL_QUERY + " from " + strTableName;
//		if (null != strWhereClause && !strWhereClause.equalsIgnoreCase("")) {
//			SQL_QUERY = SQL_QUERY + "  where " + strWhereClause + " ";
//		}
//		if (null != strSortClause && !strSortClause.equalsIgnoreCase("")) {
//			SQL_QUERY = SQL_QUERY + " order by " + strSortClause;
//		}
//		Query query = session.createQuery(SQL_QUERY);
//		if(null != objPaginationBean){
//			query.setFirstResult(objPaginationBean.getIntStartingCount());
//			query.setMaxResults(objPaginationBean.getIntNumberOfRecords());
//		}
//		/*System.out.println(" Before pagination : " + query.list().size());
//		
//		query.setMaxResults(5);
//		System.out.println(" After pagination : " + query.list().size());*/
//		if (null != query.list() && query.list().size() > 0) {
//			return query.list();
//		}
//		return null;
//	}

	
	@SuppressWarnings("rawtypes")
	public static List getRecords(String strSelectList, String strTableName, String strWhereClause, String strSortClause , PaginationBean objPaginationBean) {
		Session session = null;
		List lstReturn = null;
		
		try{
			session = new HibernateUtil().getSessionFactory().openSession();
			String SQL_QUERY = null;
			if (null == strSelectList || strSelectList.equals("")) {
				SQL_QUERY = "";
			} else {
				SQL_QUERY = "select " + strSelectList;
			}
			SQL_QUERY = SQL_QUERY + " from " + strTableName;
			if (null != strWhereClause && !strWhereClause.equalsIgnoreCase("")) {
				SQL_QUERY = SQL_QUERY + "  where " + strWhereClause + " ";
			}
			if (null != strSortClause && !strSortClause.equalsIgnoreCase("")) {
				SQL_QUERY = SQL_QUERY + " order by " + strSortClause;
			}
			Query query = session.createQuery(SQL_QUERY);
			if(null != objPaginationBean){
				query.setFirstResult(objPaginationBean.getIntStartingCount());
				query.setMaxResults(objPaginationBean.getIntNumberOfRecords());
			}
			
			lstReturn = query.list();
			
			if (null != lstReturn && lstReturn.size() > 0) {
				return lstReturn;
			}
			
		}catch(HibernateException e){
			e.printStackTrace();
		}finally{
			if(null != session){
				session.flush();
				session.close();
			}
		}
		
		return null;
	}
	
	public static Object getRecord(String strTableName, String strWhereClause, String strSortClause) {
		return getRecord(null, strTableName, strWhereClause, strSortClause);
	}

	public static Object getRecord(String strSelectList, String strTableName, String strWhereClause, String strSortClause) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		String SQL_QUERY = null;
		List lstReturn=null;
		if (null == strSelectList || strSelectList.equals("")) {
			SQL_QUERY = "";
		} else {
			SQL_QUERY = "select " + strSelectList;
		}
		SQL_QUERY = SQL_QUERY + " from " + strTableName;
		if (null != strWhereClause && !strWhereClause.equalsIgnoreCase("")) {
			SQL_QUERY = SQL_QUERY + "  where " + strWhereClause + " ";
		}
		if (null != strSortClause && !strSortClause.equalsIgnoreCase("")) {
			SQL_QUERY = SQL_QUERY + " order by " + strSortClause;
		}
		Query query = session.createQuery(SQL_QUERY);
		lstReturn=query.list();
		if (null != lstReturn && lstReturn.size() > 0) {
			return lstReturn.get(0);
		}
		return null;
	}

	
	public static boolean updateRecords(String strTableName, String strUpdateClause, String strWhereClause) throws UTHException {
		String SQL_QUERY = " update " + strTableName;
		Session session = null;
		MessageBean msg = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			if (null != strUpdateClause && !strUpdateClause.equalsIgnoreCase("")) {
				SQL_QUERY = SQL_QUERY + "  set " + strUpdateClause + " ";
			}
			if (null != strWhereClause && !strWhereClause.equalsIgnoreCase("")) {
				SQL_QUERY = SQL_QUERY + "  where " + strWhereClause + " ";
			}
			Transaction tx = session.beginTransaction();
			Query strqu = session.createQuery(SQL_QUERY);
			strqu.executeUpdate();
			tx.commit();
			session.flush();
			return true;
		} catch (HibernateException ex) {
			msg = new MessageBean();
			msg.setStrMsgType(IUMCConstants.ERROR);
			msg.setStrMsgText(ex.getMessage());
			return false;
			//throw new ULException(msg.getStrMsgText(), ex, msg);
		}
	}

	// Modify record generic
	public static boolean modifyRecord(Object objRecord) throws UTHException {
		Session session = null;
		UTHException exUAVS = null;
		MessageBean msg = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			Transaction tx = null;
			tx = session.beginTransaction();
			session.saveOrUpdate(objRecord);
			tx.commit();
			session.flush();
		} catch (HibernateException ex) {
			msg = new MessageBean();
			msg.setStrMsgText(ex.getMessage());
			exUAVS = new UTHException("Hibernate Exception", ex, msg);
			throw exUAVS;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		return true;
	}

	
	@SuppressWarnings( {"finally" })
	public static boolean modifyRecords(List<Object> objList) throws UTHException {
		Session session = null;
		UTHException exUAVS = null;
		MessageBean msg = null;
		try {
			Iterator<Object> it = objList.iterator();
			session = HibernateUtil.getSessionFactory().openSession();
			Transaction tx = null;
			tx = session.beginTransaction();
			for (int i = 1; it.hasNext();i++) {
				session.saveOrUpdate( it.next());
				
				/*tx = session.beginTransaction();//modified 01-04-2017
				if(i%IUMCConstants.COMMIT_SIZE == 0){
					tx.commit();
					tx = session.beginTransaction();
				}*/
			}
			tx.commit();
			session.flush();
		} catch (HibernateException ex) {
			msg = new MessageBean();
			msg.setStrMsgText(ex.getMessage());
			exUAVS = new UTHException("Hibernate Exception", ex, msg);
			throw exUAVS;
		} finally {
			if (null != session) {
				session.close();
			}
			return true;
		}
	}

	public static boolean deleteRecords(String strBean, String strID) throws UTHException {
		return deleteRecords(strBean, strID,false);
	}
	
	public static boolean deleteRecords(String strBean, String strID, boolean blnHardDelete) throws UTHException {
		Session session = null;
		UTHException exUAVS = null;
		MessageBean msg = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			strID = strID.replaceAll(";", ",");
			strID = strID.substring(1,strID.length()-1);
			if(blnHardDelete){
				Query objQuery  = session.createQuery("DELETE FROM " + strBean  + " bn where bn.lngID in ( " + strID + ")");
				objQuery.executeUpdate();
			}else{
				Query objQuery  = session.createQuery("update " + strBean  + " bn set bn.intStatus="+ IUMCConstants.STATUS_DELETED + " where bn.lngID in ( " + strID + ")");
				objQuery.executeUpdate();
			}
			tx.commit();
			session.flush();
		}catch (HibernateException ex) {
			msg = new MessageBean();
			msg.setStrMsgText(ex.getMessage());
			exUAVS =  new UTHException("Hibernate Exception", ex, msg);
			throw exUAVS;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		return true;
	}
	
	public static boolean deleteRecordsFromTable(String strBean,String strWhereClause) throws UMBException{
		Session session = null;
		UMBException exUAVS = null;
		MessageBean msg = null;
		
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			Query objQuery  = session.createQuery("DELETE FROM " + strBean  + "  where  " + strWhereClause) ;
			objQuery.executeUpdate();
			tx.commit();
			session.flush();
		}catch (HibernateException ex) {
			msg = new MessageBean();
			msg.setStrMsgText(ex.getMessage());
			exUAVS =  new UMBException("Hibernate Exception", ex, msg);
			throw exUAVS;
		} finally {
			if (null != session) {
				session.flush();
				session.close();
			}
		}
		return true;
	}
	
	public static boolean rejectRecords(String strBean, String strID, String strComments) throws UTHException {
		Session session = null;
		UTHException exUAVS = null;
		MessageBean msg = null;
		String strUpdateClause = "";
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			strID = strID.replaceAll(";", ",");
			strID = strID.substring(1,strID.length()-1);
			strUpdateClause = " bn set bn.intStatus="+ IUMCConstants.STATUS_REJECTED + " , strComments = '" + strComments + "'";
			Query objQuery  = session.createQuery("update " + strBean  + strUpdateClause + " where bn.lngID in ( " + strID + ")");
			objQuery.executeUpdate();
			tx.commit();
			session.flush();
		}catch (HibernateException ex) {
			msg = new MessageBean();
			msg.setStrMsgText(ex.getMessage());
			exUAVS =  new UTHException("Hibernate Exception", ex, msg);
			throw exUAVS;
		} finally {
			if (null != session) {
				session.close();
			}
		}
		return true;
	}
	@SuppressWarnings("rawtypes")
	public static boolean checkIsUnique(String strBeanName, String strAliasName, ArrayList<NameValueBean> arrNameValueList) {
		String strSQL = "";
		NameValueBean objNameValue = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		strSQL = " From " + strBeanName + " " + strAliasName + " WHERE ";

		for (int i = 0; i < arrNameValueList.size(); i++) {
			objNameValue = arrNameValueList.get(i);
			if (objNameValue.isBlnKey() && WebUtil.parseInt(objNameValue.getStrBeanFieldValue()) > 0) {
				strSQL += strAliasName + "." + objNameValue.getStrBeanFieldName() + " != " + objNameValue.getStrBeanFieldValue() + " AND ";
			} else if (!objNameValue.isBlnKey()) {
				strSQL += strAliasName + "." + objNameValue.getStrBeanFieldName() + " = '" + objNameValue.getStrBeanFieldValue() + "' AND ";
			}
		}
		if (strSQL.contains("AND")) {
			strSQL = strSQL.substring(0, strSQL.lastIndexOf("AND"));
		} else {
			strSQL = strSQL.substring(0, strSQL.lastIndexOf("WHERE"));
		}
		strSQL += " AND intStatus = " + IUMCConstants.STATUS_ACTIVE;
		Query query = session.createQuery(strSQL);
		List queryResult = query.list();
		if (queryResult != null && queryResult.size() == 0) {
			return true;
		}
		return false;
	}


}