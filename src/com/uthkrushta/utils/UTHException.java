package com.uthkrushta.utils;

import com.uthkrushta.basis.bean.MessageBean;





@SuppressWarnings("serial")
public class UTHException extends Exception{
	MessageBean msgBean = null;

	public UTHException(String strMsg, Throwable th, MessageBean msgBean) {
		super(strMsg, th);
		this.msgBean = msgBean;
	}

	public MessageBean getMsgBean() {
		return msgBean;
	}

	public void setMsg(MessageBean msgBean) {
		this.msgBean = msgBean;
	}

}
